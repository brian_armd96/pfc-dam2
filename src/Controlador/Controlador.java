package Controlador;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.naming.spi.DirStateFactory.Result;
import javax.print.attribute.standard.JobSheets;
import javax.sound.midi.MidiDevice.Info;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;

import org.apache.derby.iapi.services.cache.Cacheable;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.pdf.PdfWriter;


import Modelo.ConexionBBDD;
import Modelo.Modelo;
import Vista.Equipos;
import Vista.InfoGrupo;
import Vista.Jugadores;
import Vista.LoginSplash;
import Vista.Partidos;
import Vista.Principal;
import necesario.RSAnimation;

public class Controlador {
	private static Color Complemento=Controlador.ponerColores().get(0);
	private static Color ComplementoDark=Controlador.ponerColores().get(1);
	private static Color Primario=Controlador.ponerColores().get(2);
	private static Color PrimarioLight=Controlador.ponerColores().get(3);
	private static Color PrimarioDark=Controlador.ponerColores().get(4);
	
	private static String color="1";
	private static DefaultListModel<Object> listaEquipos=new DefaultListModel<>(), 
			listaJugadores=new DefaultListModel<>(), 
			listaJugadoresFutbol=new DefaultListModel<>(), 
			listaJugadoresTenis=new DefaultListModel<>(), 
			listaJugadoresAjedrez=new DefaultListModel<>();
	private static ArrayList<String> jugadores;
	private static ArrayList<Integer> id_jugadoresDeEquipoSeleccionado=new ArrayList<>();
	private static ArrayList<Object[]> listaDePartidasDeAjedrez;
	private static String id_jugador;
	private static int nFase=1, 
			nFaseTenis=1,
			puntosLocal=0,
			puntosVisitante=0;
	private static String contrasenna;
	private static int opcion=0;
	public static ActionListener metodoClick(){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//METODOS DEL MODELO
			}
		};
	}
	//AJUSTESS APLICACION
	public static ArrayList<Color> ponerColores() {
		ArrayList<Color>coloresVerde=new ArrayList<>();
		ArrayList<Color>coloresAzul=new ArrayList<>();
		ResultSet ajustes=Modelo.ajustes();
		try {
			while (ajustes.next()) {
				color=ajustes.getString("colores");	
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (color.equals("1")) {
			coloresVerde.add(new Color(255, 255, 255));
			coloresVerde.add(new Color(0, 0, 0));
			coloresVerde.add(new Color(55, 99, 53));
			coloresVerde.add(new Color(100, 145, 95));
			coloresVerde.add(new Color(9, 56, 14));
			return coloresVerde;
		}else {
			coloresAzul.add(new Color(255, 255, 255));
			coloresAzul.add(new Color(0, 0, 0));
			coloresAzul.add(new Color(52, 131, 171));
			coloresAzul.add(new Color(0, 86, 124));
			coloresAzul.add(new Color(107, 179, 221));
			return coloresAzul;
		}	
	}
	
	private static void ajustes(int opcion) {
		ResultSet ajustes=Modelo.ajustes();
		String usuario = null, 
				fav = null, 
				futbol, 
				ajedrez, 
				tenis,
				otros;
		try {
			while (ajustes.next()) {
				usuario=ajustes.getString("usuario");
				contrasenna=ajustes.getString("contrasenna").toString();
				color=ajustes.getString("colores");
				fav=ajustes.getString("fav");
				futbol=ajustes.getString("observaciones_futbol");
				ajedrez=ajustes.getString("observaciones_ajedrez");
				tenis=ajustes.getString("observaciones_tenis");
				otros=ajustes.getString("otro");
			}
			if (opcion!=0) {
				Principal.getUsuario().setText(usuario);
				Principal.getContrasenna().setText("");
				Principal.getPaginaFav().setText(fav);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static ActionListener cambiarColor(int i) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (i==1) {
					Modelo.cambiarColor(i, "admin");
					actualizarFramePrincipal();
				}else {
					Modelo.cambiarColor(i, "admin");
					actualizarFramePrincipal();
				}
				new rojerusan.RSNotifyFade("Ajustes", "Color modificado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);		
				JOptionPane.showMessageDialog(null, "Para que surtan efecto los cambios, debe de reiniciar la aplicaci�n", "Configuraci�n", 1);
			}
		};
	}
	
	//ABRIR VENTANAS NUEVAS
	public static ActionListener abrirVentanasFutbol(int i){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (i) {
				case 1:
					try {
			            Equipos frame=new Equipos();
			            frame.setVisible(true);
			            frame.setLocationRelativeTo(null);
			            RSAnimation.setBajar(-960, 100, 1, 5, frame);
			        } catch (Exception e1) {
			        	System.out.println("Erro al abrir la ventana equipos");
			        }
					break;
				case 2:
					try {
			            Partidos frame=new Partidos();
			            frame.setVisible(true);
			            frame.setLocationRelativeTo(null);
			            RSAnimation.setBajar(-960, 100, 1, 5, frame);
			        } catch (Exception e1) {
			        	System.out.println("Erro al abrir la ventana partidos");
			        }
					break;
				}
			}		
		};
	}
	private static boolean consultarSiTieneGrupo(){
		ResultSet rsListaEquiposSinGrupo=Modelo.listarEquiposPorFaseSinGrupo(1);
		ArrayList<String>listaEquiposSinEquipo=new ArrayList<>();
		try {
			while (rsListaEquiposSinGrupo.next()) {
				listaEquiposSinEquipo.add(rsListaEquiposSinGrupo.getString(1));	
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (listaEquiposSinEquipo.isEmpty()) {
			return true;
		}else {
			return false;
		}	
	}
	
	public static void abrirInfoGrupo(int i) {
		InfoGrupo frame;
		
		if (consultarSiTieneGrupo()) {
			switch (i) {
			case 1:
				frame = new InfoGrupo("A");
				break;
			case 2:
				frame= new InfoGrupo("B");
				break;
			case 3:
				frame= new InfoGrupo("C");
				break;
			case 4:
				frame= new InfoGrupo("D");
				break;
			case 5:
				frame= new InfoGrupo("E");
				break;
			default:
				frame= new InfoGrupo("F");
				break;
			}
			frame.setVisible(true);
			frame.setLocationRelativeTo(null);
            RSAnimation.setBajar(-960, 100, 1, 5, frame);
		}else {
			System.out.println("Primero debe de asignar a cada equipo un grupo");
		}		
	}
	//ACTUALIZAR VENTANAS 
	public static void actualizarFramePrincipal() {
		actualizarPanelFutbol();
		actualizarPanelAjedrez();
		actualizarPanelTenis();
		ajustes(1);
		ponerDiaDeLaSemana();
	}
	
	private static void actualizarPanelTenis() {
		int maximo=comprobarNumeroFases("tenis").get(comprobarNumeroFases("tenis").size()-1), minimo=1;
		Principal.getLblFaseX().setText("Fase "+nFaseTenis);
		Principal.getBarraFasesTenis().setMaximum(maximo);
		Principal.getBarraFasesTenis().setMinimum(minimo);
		Principal.getBarraFasesTenis().setValue(nFaseTenis);
		Principal.getJugadorTenisLocal().setText("");
		Principal.getJugadorTenisVisitante().setText("");
		Principal.getGanadorJ1().setSelected(false);
		Principal.getGanadorJ2().setSelected(false);
		actualizarTablaTenis();
		
		
	}
	private static void actualizarTablaTenis() {
		ResultSet rsListaPartidasTenis=Modelo.listaPartidaAjedrezFase(nFaseTenis);
		//PREPARAMOS LA TABLA TENIS
		String columnas[]= {"ID Partida", "Jugador 1", "Jugador 2", "Fecha", "Vencedor", "Ronda"};
		DefaultTableModel tablaTenis;
		tablaTenis=new DefaultTableModel(null, columnas);
		//RELLENAMOS CON DATOS
		try {
			while (rsListaPartidasTenis.next()) {
				String partidas[]={rsListaPartidasTenis.getString(1), 
						rsListaPartidasTenis.getString(2), 
						rsListaPartidasTenis.getString(3), 
						rsListaPartidasTenis.getString(6), 
						rsListaPartidasTenis.getString(7), 
						rsListaPartidasTenis.getString(8)};
				tablaTenis.addRow(partidas);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Principal.getTablaParatidasTenis().setModel(tablaTenis);
	}
	
	@SuppressWarnings({"unchecked"})
	public static void actualizarFrameEquipo() {
		ArrayList<Object>equiposLista=new ArrayList<>();
		equiposLista.clear();
		//Se a�aden los datos dentro de los combos
		ResultSet rsJugadoresSinEquipo = Modelo.listarJugadoresFutbolNoEquipo();
		ResultSet rsEquipos= Modelo.listarTodosLosEquipos();
		 //Inicializamos los combos
		reiniciarCombosEquipo();
		Vista.Equipos.getJ1().addItem("Jugador 1�");
     	Vista.Equipos.getJ2().addItem("Jugador 2�");
     	Vista.Equipos.getJ3().addItem("Jugador 3�");
     	Vista.Equipos.getJ4().addItem("Jugador 4�");
     	Vista.Equipos.getJ5().addItem("Jugador 5�");
     	Vista.Equipos.getJ6().addItem("Jugador 6�");
     	Vista.Equipos.getJ7().addItem("Jugador 7�");
     	//Rellenamos
	       try{
	    	   while(rsJugadoresSinEquipo.next()) {  	
	          	String nombre=rsJugadoresSinEquipo.getString("nombre");
	           	String curso=rsJugadoresSinEquipo.getString("curso");
	           	int id_jugador=rsJugadoresSinEquipo.getInt(1);
	           		Vista.Equipos.getJ1().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ2().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ3().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ4().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ5().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ6().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ7().addItem(id_jugador+"-"+nombre+"-"+curso);
	            }
	            while(rsEquipos.next()) {  	
	            	String nombre_equipos=rsEquipos.getString("nombre_equipo");
	            	String tag_curso=rsEquipos.getString("tag_curso");
	            	String ronda_Equipos=rsEquipos.getString("ronda");
	               	String datos_equipo=nombre_equipos+"-"+tag_curso+"-Ronda ("+ronda_Equipos+")";
	               	equiposLista.add(datos_equipo);
	            }
	            listaEquipos.removeAllElements();
	    		for (int i = 0; i < equiposLista.size(); i++) {
	    			listaEquipos.addElement(equiposLista.get(i));
	    		}
	    		Equipos.getListEquipo().setModel(listaEquipos);
	    		rsJugadoresSinEquipo.close();
	        }catch(Exception ex){
	        	System.out.println("error mostrando equipos");
	            System.out.println(ex);
	            }
	}
	
	private static void reiniciarCombosEquipo() {
		Vista.Equipos.getJ1().removeAllItems();
     	Vista.Equipos.getJ2().removeAllItems();
     	Vista.Equipos.getJ3().removeAllItems();
     	Vista.Equipos.getJ4().removeAllItems();
     	Vista.Equipos.getJ5().removeAllItems();
     	Vista.Equipos.getJ6().removeAllItems();
     	Vista.Equipos.getJ7().removeAllItems();		
	}
	public static void actualizarFrameJugadores(int i) {
		
		ResultSet rsListaJugadores=Modelo.listarJugadores();
		opcion=i;
	
		switch (i) {
		case 1:
			actualizarJugadorFutbol();
			//DESACTIVAMOS OPCIONES QUE NO SEAN DE FUTBOL
			Jugadores.getFutbolSelect().setSelected(true);
			Jugadores.getAjedrezSelect().setEnabled(false);
			Jugadores.getTenisSelect().setEnabled(false);
			Jugadores.getSpinner().setEnabled(false);
			Jugadores.getSpinner_1().setEnabled(false);
			break;
		case 2:
			actualizarJugadoresAjedrez();			
			Jugadores.getFutbolSelect().setEnabled(false);
			Jugadores.getAjedrezSelect().setSelected(true);
			Jugadores.getTenisSelect().setEnabled(false);
			Jugadores.getSpinner().setEnabled(false);
			break;
		case 3:
			actualizarJugadoresTenis();
			Jugadores.getFutbolSelect().setEnabled(false);
			Jugadores.getAjedrezSelect().setEnabled(false);
			Jugadores.getTenisSelect().setSelected(true);
			Jugadores.getSpinner_1().setEnabled(false);
			break;
		default:
			try {
				listaJugadores.removeAllElements();
				while (rsListaJugadores.next()) {
					String jugadores=rsListaJugadores.getString("id_jugador")+
							"-"+rsListaJugadores.getString("nombre")+
							"-"+rsListaJugadores.getString("curso")+
							" - "+rsListaJugadores.getString("competicion")+
							"-"+rsListaJugadores.getString("ronda_ajedrez")+
							"-"+rsListaJugadores.getString("ronda_tenis");
					listaJugadores.addElement(jugadores);
				}
				Jugadores.getListJugadores().setModel(listaJugadores);
			} catch (SQLException e) {
				//erro obteniendo los datos de JUGARORES
				e.printStackTrace();
			}
			break;
		}	
	}
	
	private static void actualizarJugadoresTenis() {
		ResultSet rsListaJugadoresTenis=Modelo.listarJugadoresTenis();
		try {
			
			listaJugadoresTenis.removeAllElements();
			while (rsListaJugadoresTenis.next()) {
				String jugadores=rsListaJugadoresTenis.getString("id_jugador")+
						"-"+rsListaJugadoresTenis.getString("nombre")+
						"-"+rsListaJugadoresTenis.getString("curso")+
						" - "+rsListaJugadoresTenis.getString("competicion")+
						"-"+rsListaJugadoresTenis.getString("ronda_tenis");
				
				listaJugadoresTenis.addElement(jugadores);	
			}
			Jugadores.getListJugadores().setModel(listaJugadoresTenis);
		} catch (SQLException e) {
			//erro obteniendo los datos de la parte de TNIS
			e.printStackTrace();
		}
	}
	private static void actualizarJugadoresAjedrez() {
		ResultSet rsListaJugadoresAjedrez=Modelo.listarJugadoresAjedrez();
		try {
			
			//PREPARAMOS EL ARRAY LIST PARA GUARDAR TODOS LOS JUGADORES Y LUEGO MOSTRARLO EN EL JLIST
			listaJugadoresAjedrez.removeAllElements();
			while (rsListaJugadoresAjedrez.next()) {
				String jugadores=rsListaJugadoresAjedrez.getString("id_jugador")+
						"-"+rsListaJugadoresAjedrez.getString("nombre")+
						"-"+rsListaJugadoresAjedrez.getString("curso")+
						" - "+rsListaJugadoresAjedrez.getString("competicion")+
						"-"+rsListaJugadoresAjedrez.getString("ronda_ajedrez");
				
				listaJugadoresAjedrez.addElement(jugadores);
			}
			Jugadores.getListJugadores().setModel(listaJugadoresAjedrez);
		} catch (SQLException e) {
			//erro obteniendo los datos de la parte de AJDREZ
			e.printStackTrace();
		}
	}
	private static void actualizarJugadorFutbol() {
		
		ResultSet rsListaJugadoresFutbol=Modelo.listarJugadoresFutbol();
		try {
			//PREPARAMOS EL ARRAY LIST PARA GUARDAR TODOS LOS JUGADORES Y LUEGO MOSTRARLO EN EL JLIST
			listaJugadoresFutbol.removeAllElements();
			while (rsListaJugadoresFutbol.next()) {
				String jugadores=rsListaJugadoresFutbol.getString("id_jugador")+
						"-"+rsListaJugadoresFutbol.getString("nombre")+
						"-"+rsListaJugadoresFutbol.getString("curso")+
						" - "+rsListaJugadoresFutbol.getString("competicion");
				//CREAMOS EL MODEL LIST PARA MOSTRARLO EN EL JLIST
				listaJugadoresFutbol.addElement(jugadores);		
			}
			Jugadores.getListJugadores().setModel(listaJugadoresFutbol);
		} catch (SQLException e) {
			//erro obteniendo los datos de la parte de futbol
			e.printStackTrace();
		}
		
	}
	public static void actualizarFrameInfoGrupo(String titulo) {
		ResultSet rslistarEquiposPorGruposOrdenadoPorPuntos = null;
		ResultSet rsPartidosGrupo= null;
		DefaultListModel<String>listaEquipos=new DefaultListModel<>();
		String idEquipo;
		Object[]FilaTitulo= {"ID Partido", "Equipo Local", "Equipo Visitante", "Goles local", "Goles visitante", "Fecha"};
		DefaultTableModel listaPartidosGrupo=new DefaultTableModel(null, FilaTitulo);
		
		switch (titulo) {
		case "A":
			rslistarEquiposPorGruposOrdenadoPorPuntos=Modelo.listaEquipoPorGrupoPuntos(1);
			rsPartidosGrupo=Modelo.listaPartidosPorGrupo(1);
			break;
		case "B":
			rslistarEquiposPorGruposOrdenadoPorPuntos=Modelo.listaEquipoPorGrupoPuntos(2);
			rsPartidosGrupo=Modelo.listaPartidosPorGrupo(2);
			break;
		case "C":
			rslistarEquiposPorGruposOrdenadoPorPuntos=Modelo.listaEquipoPorGrupoPuntos(3);
			rsPartidosGrupo=Modelo.listaPartidosPorGrupo(3);
			break;
		case "D":
			rslistarEquiposPorGruposOrdenadoPorPuntos=Modelo.listaEquipoPorGrupoPuntos(4);
			rsPartidosGrupo=Modelo.listaPartidosPorGrupo(4);
			break;
		case "E":
			rslistarEquiposPorGruposOrdenadoPorPuntos=Modelo.listaEquipoPorGrupoPuntos(5);
			rsPartidosGrupo=Modelo.listaPartidosPorGrupo(5);
			break;
		case "F":
			rslistarEquiposPorGruposOrdenadoPorPuntos=Modelo.listaEquipoPorGrupoPuntos(6);
			rsPartidosGrupo=Modelo.listaPartidosPorGrupo(6);
			break;	
		}
		try {
			while (rslistarEquiposPorGruposOrdenadoPorPuntos.next()) {
				listaEquipos.addElement(rslistarEquiposPorGruposOrdenadoPorPuntos.getString(2).toString());
			}
			while (rsPartidosGrupo.next()) {
				Object[] fila= {rsPartidosGrupo.getString(1), rsPartidosGrupo.getString(2), rsPartidosGrupo.getString(3), rsPartidosGrupo.getString(4), rsPartidosGrupo.getString(5), rsPartidosGrupo.getString(6),};
				listaPartidosGrupo.addRow(fila);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		InfoGrupo.getListaEquipos().setModel(listaEquipos);
		InfoGrupo.getTableMetro().setModel(listaPartidosGrupo);
		InfoGrupo.getGolesLocal().setText("0");
		InfoGrupo.getGolesVisitante().setText("0");

	}
	//ACTUALIZAR PANELES DE LA VENTANA PRINCIPAL
	public static void actualizarPanelFutbol() {
		ActualizarGrupos();
		ActualizarCuartos();
		ActualizarSemmins();
		ActualizarFinal();
		
	}
	
	private static void ActualizarFinal() {
		
	}
	private static void ActualizarSemmins() {
		
		
	}
	private static void ActualizarCuartos() {
		//DEJAMOS LISTOS PARA QUE SE ANNADAN LOS EQUIPOS EN LOS COMBOS
		Principal.getP1Local().removeAllItems();
		Principal.getP1Visitante().removeAllItems();
		Principal.getP2Local().removeAllItems();
		Principal.getP2Visitante().removeAllItems();
		Principal.getP3Local().removeAllItems();
		Principal.getP3Visitante().removeAllItems();
		Principal.getP4Local().removeAllItems();
		Principal.getP4Visitante().removeAllItems();
		
		ResultSet rs=Modelo.listarEquiposPorFase(2);
		try {
			while (rs.next()) {
				Principal.getP1Local().addItem(rs.getString(2));
				Principal.getP1Visitante().addItem(rs.getString(2));
				
				Principal.getP2Local().addItem(rs.getString(2));
				Principal.getP2Visitante().addItem(rs.getString(2));
				
				Principal.getP3Local().addItem(rs.getString(2));
				Principal.getP3Visitante().addItem(rs.getString(2));
				
				Principal.getP4Local().addItem(rs.getString(2));
				Principal.getP4Visitante().addItem(rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	//CREAR NUEVO REGISTRO
	public static void crearEquipo() {
		String nombre_equipo=Equipos.getNombreEquipo().getText().toString();
		String tag_curso=Equipos.getTagCurso().getSelectedItem().toString();
		String observaciones=Equipos.getObservacionesEquipo().getText().toString();
		int ronda=1;
		int puntos=0;
		if (Equipos.getObservacionesEquipo().getText().equals("Observaciones")) {
			observaciones="";
		}
		Modelo.annadirEquipo(nombre_equipo, tag_curso, ronda, observaciones, puntos);
	}
	

	//BOTONES
	public static ActionListener eliminarJugador(int i){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Modelo.eliminarJugador(id_jugador);
				limpiarCamposJugadores();
				switch (i) {
				case 1:
					actualizarFrameJugadores(i);
					break;
				case 2:
					actualizarFrameJugadores(i);
					break;
				case 3:
					actualizarFrameJugadores(i);
					break;
				default:
					actualizarFrameJugadores(i);
					break;
				}
			}
		};
	}
	
	public static ActionListener eliminarEquipo(){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nombre_equipo=Equipos.getNombreEquipo().getText().toString();
				int id_equipo=Modelo.obtenerIdEquipo(nombre_equipo);
				try {
					if (!id_jugadoresDeEquipoSeleccionado.isEmpty()) {
						for (int i = 0; i < id_jugadoresDeEquipoSeleccionado.size(); i++) {
							Modelo.eliminarJugadordeEquipo(id_jugadoresDeEquipoSeleccionado.get(i));
						}
						Modelo.eliminarEquipo(id_equipo);
						actualizarFrameEquipo();
						limpiarFrameEquipo();
						Equipos.getBtnAgregar().setText("Insertar");
					} else {
						System.out.println("ERROR \n -ESTE EQUIPO NO CONTIENE EL NUMERO ADECUADO DE JUGADORES");
					}
				} catch (Exception e2) {
					System.out.println("Ha ocurrido un error, este equipo no cuenta con el numero correcto de jugadores");
				}
			}
		};
	}
	
	public static ActionListener annadirJugadorAlEquipo(){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (Equipos.getBtnAgregar().getText().toString()) {
				case "Insertar":
					//OBTENEMOS LOS DATOS PARA LUEGO ACTUALIZAR LOS JUGADORES CON EL EQUIPO
					String nombre_equipo=Equipos.getNombreEquipo().getText().toString();
					if (cogerIdJugadorDeEquipo()!=null && !Equipos.getTagCurso().getSelectedItem().toString().equals("Seleccione tag de curso")) {
						ArrayList<Integer> id_jugador=cogerIdJugadorDeEquipo();
						crearEquipo();
						int id_equipo=Modelo.obtenerIdEquipo(nombre_equipo);
						for (int i = 0; i < id_jugador.size(); i++) {
							Modelo.actualizarJugador(id_equipo, id_jugador.get(i));
						}
						actualizarFrameEquipo();
						limpiarFrameEquipo();
						Equipos.getBtnAgregar().setText("Insertar");
					}else {
						//MENSAJE RELLENAR TODOS LOS CAMPOS
						System.out.println("rellene los campos");
						JOptionPane.showMessageDialog(null, "Todos los campos deben de estar rellenos", "Fallo al guardar", 0);
					}
					break;
					
				case "Crear equipo":
					actualizarFrameEquipo();
					limpiarFrameEquipo();
					Equipos.getBtnAgregar().setText("Insertar");
					break;
				}	
			}
		};
	}
	
	public static ActionListener actualizarEquipoJugadores() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Equipos.getNombreEquipo().getText().isEmpty()) {
					//MANDAR MENSAJE SELECCIONAR PRIMERO UN EQUIPO
					System.out.println("escoje un equipo");
				} else {
					//COGEMOS LOS CAMPOS DE EQUIPO PARA LUEGO MANDAR PETICION DE ACTUALIZAR
					String nombre_equipo=Equipos.getNombreEquipo().getText().toString();
					int id_equipo=Modelo.obtenerIdEquipo(nombre_equipo);
					String tagCurso=Equipos.getTagCurso().getSelectedItem().toString();
					String observaciones=Equipos.getObservacionesEquipo().getText().toString();
					//COMPROBAMOS CAMBIOS EN TODOS LOS COMBOS DE LOS JUGADORES Y ACTUALIZAMOS
					if (!id_jugadoresDeEquipoSeleccionado.isEmpty()) {
						int contadorDeCambios=0;
						ArrayList<Integer> id_jugadoresNuevos=cogerIdJugadorDeEquipo();
						for (int i = 0; i < id_jugadoresDeEquipoSeleccionado.size(); i++) {
							if (!id_jugadoresDeEquipoSeleccionado.contains(id_jugadoresNuevos.get(i)) && !id_jugadoresNuevos.contains(id_jugadoresDeEquipoSeleccionado.get(i))) {
								Modelo.eliminarJugadordeEquipo(id_jugadoresDeEquipoSeleccionado.get(i));
								Modelo.actualizarJugador(id_equipo, id_jugadoresNuevos.get(i));
							} else {
								System.out.println("NO HAY CAMBIOS QUE GUARDAR");
								contadorDeCambios++;
							}
						}
						if (contadorDeCambios==7) {
							//DIALOG MENSAJE NO HAY CAMBIOS QUE REALIZAR<<<<<<<<<<<<<<<<<<<<<<<<<<<<
							System.out.println("no hay cambios en el equipo");
							
						}
						Modelo.actualizarEquipo(id_equipo, nombre_equipo, tagCurso, observaciones);
						actualizarFrameEquipo();
						limpiarFrameEquipo();
						Equipos.getBtnAgregar().setText("Insertar");
					}else {
						//MENSAJE ERRO EN COGER LOS DATOS DEL EQUIPO ERRO BASE DE DATOS
						System.out.println("eerror coger jugadores del equipo");
					}
				}
			}
		};
	}
				
	
	public static MouseAdapter mostrarEquipo() {
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (Equipos.getListEquipo().getSelectedIndex()==-1) {
					//mensaje de erro LISTA VACIA
					System.out.println("lista vacia");
				} else {
					String [] equipoSeleccionado=Equipos.getListEquipo().getSelectedValue().toString().split("-");
					String nombre_equipo=equipoSeleccionado[0];
					Equipos.getNombreEquipo().setText(nombre_equipo);
					reiniciarCombosEquipo();
					actualizarCombosEquipos();
					mostramosCamposdeEquipo(nombre_equipo);
					Equipos.getTagCurso().setSelectedItem(equipoSeleccionado[1]);
					Equipos.getBtnAgregar().setText("Crear equipo");
					//jugadores.clear();
					//RELLENAMOS EL ARRAY LIST CON LOS JUGADORES DE ESE EQUIPO
					id_jugadoresDeEquipoSeleccionado=cogerIdJugadorDeEquipo();
				}
			}
		};
	}
	protected static void mostramosCamposdeEquipo(String nombreEquipo) {
		jugadores=new ArrayList<>();
		int id_equipo=Modelo.obtenerIdEquipo(nombreEquipo);
		ResultSet jugadoresEquipo=Modelo.listarJugadoresEquipo(id_equipo);
		try {
			
			while (jugadoresEquipo.next()) {
				String nombre=jugadoresEquipo.getString("nombre");
	           	String curso=jugadoresEquipo.getString("curso");
	           	int id_jugador=jugadoresEquipo.getInt(1);
	           		jugadores.add(id_jugador+"-"+nombre+"-"+curso);
			}
		} catch (SQLException e) {
			//ERROR BBDD 
			e.printStackTrace();
		}
		//seleccionamos
		if (jugadores.size()==7) {
			System.out.println(jugadores.get(0)+jugadores.get(1)+jugadores.get(2)+jugadores.get(3)+jugadores.get(4)
			+jugadores.get(5)+jugadores.get(6));
			Equipos.getJ1().setSelectedItem(jugadores.get(0).toString());		
			Equipos.getJ2().setSelectedItem(jugadores.get(1).toString());
			Equipos.getJ3().setSelectedItem(jugadores.get(2).toString());
			Equipos.getJ4().setSelectedItem(jugadores.get(3).toString());
			Equipos.getJ5().setSelectedItem(jugadores.get(4).toString());
			Equipos.getJ6().setSelectedItem(jugadores.get(5).toString());
			Equipos.getJ7().setSelectedItem(jugadores.get(6).toString());
		}else {
			System.out.println("jugadores incompletos");
		}
		try {
			jugadoresEquipo.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	protected static void actualizarCombosEquipos() {
		ArrayList<Object>equiposLista=new ArrayList<>();
		equiposLista.clear();
		//Se a�aden los datos dentro de los combos
		ResultSet rsJugadoresFutbol = Modelo.listarJugadoresFutbol();
		 //Inicializamos los combos
		Vista.Equipos.getJ1().addItem("Jugador 1�");
     	Vista.Equipos.getJ2().addItem("Jugador 2�");
     	Vista.Equipos.getJ3().addItem("Jugador 3�");
     	Vista.Equipos.getJ4().addItem("Jugador 4�");
     	Vista.Equipos.getJ5().addItem("Jugador 5�");
     	Vista.Equipos.getJ6().addItem("Jugador 6�");
     	Vista.Equipos.getJ7().addItem("Jugador 7�");
     	//Rellenamos
	       try{
	    	   while(rsJugadoresFutbol.next()) {  	
	          	String nombre=rsJugadoresFutbol.getString("nombre");
	           	String curso=rsJugadoresFutbol.getString("curso");
	           	int id_jugador=rsJugadoresFutbol.getInt(1);
	           		Vista.Equipos.getJ1().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ2().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ3().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ4().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ5().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ6().addItem(id_jugador+"-"+nombre+"-"+curso);
		        	Vista.Equipos.getJ7().addItem(id_jugador+"-"+nombre+"-"+curso);
	            }
	    		rsJugadoresFutbol.close();
	        }catch(Exception ex){
	            System.out.println(ex);
	            }
	}
	protected static void limpiarFrameEquipo() {
		Equipos.getNombreEquipo().setText("");
		Equipos.getObservacionesEquipo().setText("Observaciones");
		Equipos.getJ1().setSelectedItem("Jugador 1�");
		Equipos.getJ2().setSelectedItem("Jugador 2�");
		Equipos.getJ3().setSelectedItem("Jugador 3�");
		Equipos.getJ4().setSelectedItem("Jugador 4�");
		Equipos.getJ5().setSelectedItem("Jugador 5�");
		Equipos.getJ6().setSelectedItem("Jugador 6�");
		Equipos.getJ7().setSelectedItem("Jugador 7�");

	}
	public static ActionListener annadirJugador(int valor){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nombre, curso, seleccion = "", observacion;
				int ronda_ajedrez=0;
				int ronda_tenis=0;
				String id_equipo="Seleccione equipo";
						switch (valor) {
						default:
							nombre=Principal.getNombreAlumno().getText().toString();
							curso=Principal.getCursoAlumno().getSelectedItem().toString();
							observacion=Principal.getObservaciones().getText().toString();
							//Rellenamos la seleccion de torneo
							if (Vista.Principal.getFutbolSelect().isSelected() && !Vista.Principal.getAjedrezSelect().isSelected() && !Vista.Principal.getTenisSelect().isSelected()) {
								seleccion="F";
							}if (Vista.Principal.getFutbolSelect().isSelected() && Vista.Principal.getAjedrezSelect().isSelected() && !Vista.Principal.getTenisSelect().isSelected()) {
								seleccion="FA";
								ronda_ajedrez=1;
							}if (Vista.Principal.getFutbolSelect().isSelected() && !Vista.Principal.getAjedrezSelect().isSelected() && Vista.Principal.getTenisSelect().isSelected()) {
								seleccion="FT";
								ronda_tenis=1;
							}if (Vista.Principal.getFutbolSelect().isSelected() && Vista.Principal.getAjedrezSelect().isSelected() && Vista.Principal.getTenisSelect().isSelected()) {
								seleccion="FTA";
								ronda_ajedrez=1;
								ronda_tenis=1;
							}if (!Vista.Principal.getFutbolSelect().isSelected() && Vista.Principal.getAjedrezSelect().isSelected() && !Vista.Principal.getTenisSelect().isSelected()) {
								seleccion="A";
								ronda_ajedrez=1;
							}if (!Vista.Principal.getFutbolSelect().isSelected() && Vista.Principal.getAjedrezSelect().isSelected() && Vista.Principal.getTenisSelect().isSelected()) {
								seleccion="AT";
								ronda_ajedrez=1;
								ronda_tenis=1;
							}if (!Vista.Principal.getFutbolSelect().isSelected() && !Vista.Principal.getAjedrezSelect().isSelected() && Vista.Principal.getTenisSelect().isSelected()) {
								seleccion="T";
								ronda_tenis=1;
							}if (observacion.equals("Observaciones")) {
								observacion="";
							}if (nombre.isEmpty() || curso.equals("Seleccione curso") || seleccion.equals("")) {
								//INTRODUCIR VENTANA DE ALERTA
						        JOptionPane.showMessageDialog(null, "Rellene todos los campos: \n -NOMBRE \n -CURSO \n -TORNEO", "Datos incorrectos", 0, null);
							}else {
								try {
								Modelo.annadirJugador(nombre, curso, seleccion, observacion, ronda_ajedrez, ronda_tenis, id_equipo, 2);
							} catch (Exception e2) {
							       JOptionPane.showMessageDialog(null, "Error al insertar alumno, posibles causas: \n -Alumno ya existente. \n -No existe la base de datos. \n -Ha introducido datos incorrectos.", "Datos incorrectos", 0, null);
							}
							limpiarCamposPrincipal();
							new rojerusan.RSNotifyFade("Jugador", "Alumno insertado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);
							}
							break;

						case 1:
							if (Jugadores.getBtnInsertar().getText().toString().equals("Insertar")) {
							id_equipo="";
							nombre=Jugadores.getNombreJugador().getText().toString();
							curso=Jugadores.getComboCurso().getSelectedItem().toString();
							observacion=Jugadores.getObservaciones().getText().toString();
							seleccion = "F";
							if (observacion.equals("Observaciones")) {
								observacion="";
							}if (nombre.isEmpty() || curso.equals("Seleccione curso") || seleccion.equals("")) {
								//INTRODUCIR VENTANA DE ALERTA
						        JOptionPane.showMessageDialog(null, "Rellene todos los campos: \n -NOMBRE \n -CURSO \n -TORNEO", "Datos incorrectos", 0, null);
							}else {
								try {
							
								Modelo.annadirJugador(nombre, curso, seleccion, observacion, ronda_ajedrez, ronda_tenis, id_equipo, 2);
							} catch (Exception e2) {
							       JOptionPane.showMessageDialog(null, "Error al insertar alumno, posibles causas: \n -Alumno ya existente. \n -No existe la base de datos. \n -Ha introducido datos incorrectos.", "Datos incorrectos", 0, null);
							}
							new rojerusan.RSNotifyFade("Jugador", "Alumno insertado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);
							actualizarFrameJugadores(valor);
							limpiarCamposJugadores();
							}
							}else{
								limpiarCamposJugadores();
								Jugadores.getBtnInsertar().setText("Insertar");
							}
							break;
							
						case 2:
							if (Jugadores.getBtnInsertar().getText().toString().equals("Insertar")) {
								System.out.println("VA A INSERTAR");
								nombre=Jugadores.getNombreJugador().getText().toString();
								curso=Jugadores.getComboCurso().getSelectedItem().toString();
								observacion=Jugadores.getObservaciones().getText().toString();
								seleccion = "A";
								ronda_ajedrez=1;						
								if (observacion.equals("Observaciones")) {
									observacion="";
								}if (nombre.isEmpty() || curso.equals("Seleccione curso") || seleccion.equals("")) {
									//INTRODUCIR VENTANA DE ALERTA
							        JOptionPane.showMessageDialog(null, "Rellene todos los campos: \n -NOMBRE \n -CURSO \n -TORNEO", "Datos incorrectos", 0, null);
								}else {
									try {
									Modelo.annadirJugador(nombre, curso, seleccion, observacion, ronda_ajedrez, ronda_tenis, id_equipo, 2);
									} catch (Exception e2) {
								       JOptionPane.showMessageDialog(null, "Error al insertar alumno, posibles causas: \n -Alumno ya existente. \n -No existe la base de datos. \n -Ha introducido datos incorrectos.", "Datos incorrectos", 0, null);
								}
								new rojerusan.RSNotifyFade("Jugador", "Alumno insertado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);
								actualizarFrameJugadores(valor);
								limpiarCamposJugadores();
								}
							}else{
								limpiarCamposJugadores();
								Jugadores.getBtnInsertar().setText("Insertar");
							}
							break;
							
						case 3:
							if (Jugadores.getBtnInsertar().getText().toString().equals("Insertar")) {
							
							nombre=Jugadores.getNombreJugador().getText().toString();
							curso=Jugadores.getComboCurso().getSelectedItem().toString();
							observacion=Jugadores.getObservaciones().getText().toString();
							nombre=Jugadores.getNombreJugador().getText().toString();
							curso=Jugadores.getComboCurso().getSelectedItem().toString();
							seleccion = "T";
							observacion=Jugadores.getObservaciones().getText().toString();
							ronda_ajedrez=0;
							ronda_tenis=1;
							if (observacion.equals("Observaciones")) {
								observacion="";
							}if (nombre.isEmpty() || curso.equals("Seleccione curso") || seleccion.equals("")) {
								//INTRODUCIR VENTANA DE ALERTA
						        JOptionPane.showMessageDialog(null, "Rellene todos los campos: \n -NOMBRE \n -CURSO \n -TORNEO", "Datos incorrectos", 0, null);
							}else {
								try {
								Modelo.annadirJugador(nombre, curso, seleccion, observacion, ronda_ajedrez, ronda_tenis, id_equipo, 2);
							} catch (Exception e2) {
							       JOptionPane.showMessageDialog(null, "Error al insertar alumno, posibles causas: \n -Alumno ya existente. \n -No existe la base de datos. \n -Ha introducido datos incorrectos.", "Datos incorrectos", 0, null);
							}
							new rojerusan.RSNotifyFade("Jugador", "Alumno insertado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);
							actualizarFrameJugadores(valor);
							limpiarCamposJugadores();
							}
							}else{
								limpiarCamposJugadores();
								Jugadores.getBtnInsertar().setText("Insertar");
							}
							break;
						case 4:
							if (Jugadores.getBtnInsertar().getText().toString().equals("Insertar")) {
							
							nombre=Jugadores.getNombreJugador().getText().toString();
							curso=Jugadores.getComboCurso().getSelectedItem().toString();
							observacion=Jugadores.getObservaciones().getText().toString();
							//Rellenamos la seleccion de torneo
							if (Jugadores.getFutbolSelect().isSelected() && !Jugadores.getAjedrezSelect().isSelected() && !Jugadores.getTenisSelect().isSelected()) {
								seleccion="F";
							}if (Jugadores.getFutbolSelect().isSelected() && Jugadores.getAjedrezSelect().isSelected() && !Jugadores.getTenisSelect().isSelected()) {
								seleccion="FA";
								ronda_ajedrez=1;
							}if (Jugadores.getFutbolSelect().isSelected() && !Jugadores.getAjedrezSelect().isSelected() && Jugadores.getTenisSelect().isSelected()) {
								seleccion="FT";
								ronda_tenis=1;
							}if (Jugadores.getFutbolSelect().isSelected() && Jugadores.getAjedrezSelect().isSelected() && Jugadores.getTenisSelect().isSelected()) {
								seleccion="FTA";
								ronda_ajedrez=1;
								ronda_tenis=1;
							}if (!Jugadores.getFutbolSelect().isSelected() && Jugadores.getAjedrezSelect().isSelected() && !Jugadores.getTenisSelect().isSelected()) {
								seleccion="A";
								ronda_ajedrez=1;
							}if (!Jugadores.getFutbolSelect().isSelected() && Jugadores.getAjedrezSelect().isSelected() && Jugadores.getTenisSelect().isSelected()) {
								seleccion="AT";
								ronda_ajedrez=1;
								ronda_tenis=1;
							}if (!Jugadores.getFutbolSelect().isSelected() && !Jugadores.getAjedrezSelect().isSelected() && Jugadores.getTenisSelect().isSelected()) {
								seleccion="T";
								ronda_tenis=1;
							}if (observacion.equals("Observaciones")) {
								observacion="";
							}if (nombre.isEmpty() || curso.equals("Seleccione curso") || seleccion.equals("")) {
								//INTRODUCIR VENTANA DE ALERTA
						        JOptionPane.showMessageDialog(null, "Rellene todos los campos: \n -NOMBRE \n -CURSO \n -TORNEO", "Datos incorrectos", 0, null);
							}else {
							if (id_equipo.equals("Seleccione equipo")) {
								try {
									Modelo.annadirJugador(nombre, curso, seleccion, observacion, ronda_ajedrez, ronda_tenis, id_equipo, 2);
								} catch (Exception e2) {
							        JOptionPane.showMessageDialog(null, "Error al insertar alumno, posibles causas: \n -Alumno ya existente. \n -No existe la base de datos. \n -Ha introducido datos incorrectos.", "Datos incorrectos", 0, null);
								}
							}else {
								try {
									Modelo.annadirJugador(nombre, curso, seleccion, observacion, ronda_ajedrez, ronda_tenis, id_equipo, 3);
								} catch (Exception e2) {
							        JOptionPane.showMessageDialog(null, "Error al insertar alumno, posibles causas: \n -Alumno ya existente. \n -No existe la base de datos. \n -Ha introducido datos incorrectos.", "Datos incorrectos", 0, null);
								}
							}
								actualizarFrameJugadores(valor);
								limpiarCamposJugadores();
								new rojerusan.RSNotifyFade("Jugador", "Alumno insertado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);		
							}
							}else{
								limpiarCamposJugadores();
								Jugadores.getBtnInsertar().setText("Insertar");
							}
							break;	
				}			
			}				
			
		};
	}

	protected static void limpiarCamposJugadores() {
		Jugadores.getNombreJugador().setText("");
		Jugadores.getComboCurso().setSelectedItem("Seleccione curso");
		
		Jugadores.getFutbolSelect().setSelected(false);
		Jugadores.getAjedrezSelect().setSelected(false);
		Jugadores.getTenisSelect().setSelected(false);
		Jugadores.getSpinner().setValue(1);
		Jugadores.getSpinner().setValue(1);;
		
	}
	protected static ArrayList<Integer> cogerIdJugadorDeEquipo() {
		if (Equipos.getJ1().getSelectedItem().toString().equals("Jugador 1�") || Equipos.getJ2().getSelectedItem().toString().equals("Jugador 2�") 
				|| Equipos.getJ3().getSelectedItem().toString().equals("Jugador 3�") || Equipos.getJ4().getSelectedItem().toString().equals("Jugador 4�") 
				|| Equipos.getJ5().getSelectedItem().toString().equals("Jugador 5�") || Equipos.getJ6().getSelectedItem().toString().equals("Jugador 6�") 
				|| Equipos.getJ7().getSelectedItem().toString().equals("Jugador 7�") || Equipos.getTagCurso().getSelectedItem().equals("Seleccione tag curso")) {
			//MENSAJE DE ALERTA ESCoGER 7 JUGADORES PARA EL CREAR EL QUIPO
			System.out.println("EL EQUIPO DEBE DE TENER UN NUMERO CORRECTO DE JUGADORES");
			JOptionPane.showMessageDialog(null, "El equipo debe de tener 7 jugadores", "Error", 0);
			return null;
		}else {
			String[] IDJ1=Equipos.getJ1().getSelectedItem().toString().split("-");
			String[] IDJ2=Equipos.getJ2().getSelectedItem().toString().split("-");
			String[] IDJ3=Equipos.getJ3().getSelectedItem().toString().split("-");
			String[] IDJ4=Equipos.getJ4().getSelectedItem().toString().split("-");
			String[] IDJ5=Equipos.getJ5().getSelectedItem().toString().split("-");
			String[] IDJ6=Equipos.getJ6().getSelectedItem().toString().split("-");
			String[] IDJ7=Equipos.getJ7().getSelectedItem().toString().split("-");

			int id1=Integer.parseInt(IDJ1[0]);
			int id2=Integer.parseInt(IDJ2[0]);
			int id3=Integer.parseInt(IDJ3[0]);
			int id4=Integer.parseInt(IDJ4[0]);
			int id5=Integer.parseInt(IDJ5[0]);
			int id6=Integer.parseInt(IDJ6[0]);
			int id7=Integer.parseInt(IDJ7[0]);

			ArrayList<Integer> id_jugador=new ArrayList<>();
			id_jugador.clear();
			id_jugador.add(id1);
			id_jugador.add(id2);
			id_jugador.add(id3);
			id_jugador.add(id4);
			id_jugador.add(id5);
			id_jugador.add(id6);
			id_jugador.add(id7);

			return id_jugador;
		}
	}

	private static void ponerDiaDeLaSemana() {
		Calendar diaSemana = Calendar.getInstance();
		int dia=diaSemana.get(Calendar.DAY_OF_WEEK)-1;
		iniciarFechaSemana(dia);
		switch (dia) {
			case 1:
				Principal.getSelectLunes().setSelected(true);
				Principal.getSelectMartes().setSelected(false);
				Principal.getSelectMiercoles().setSelected(false);
				Principal.getSelectJueves().setSelected(false);
				Principal.getSelectViernes().setSelected(false);
				break;
			case 2:
				Principal.getSelectLunes().setSelected(false);
				Principal.getSelectMartes().setSelected(true);
				Principal.getSelectMiercoles().setSelected(false);
				Principal.getSelectJueves().setSelected(false);
				Principal.getSelectViernes().setSelected(false);
				break;
			case 3:
				
				Principal.getSelectLunes().setSelected(false);
				Principal.getSelectMartes().setSelected(false);
				Principal.getSelectMiercoles().setSelected(true);
				Principal.getSelectJueves().setSelected(false);
				Principal.getSelectViernes().setSelected(false);
				break;
			case 4:
				Principal.getSelectLunes().setSelected(false);
				Principal.getSelectMartes().setSelected(false);
				Principal.getSelectMiercoles().setSelected(false);
				Principal.getSelectJueves().setSelected(true);
				Principal.getSelectViernes().setSelected(false);
				break;
			case 5:
				Principal.getSelectLunes().setSelected(false);
				Principal.getSelectMartes().setSelected(false);
				Principal.getSelectMiercoles().setSelected(false);
				Principal.getSelectJueves().setSelected(false);
				Principal.getSelectViernes().setSelected(true);
				break;
			default:
				Principal.getSelectLunes().setEnabled(false);
				Principal.getSelectMartes().setEnabled(false);
				Principal.getSelectMiercoles().setEnabled(false);
				Principal.getSelectJueves().setEnabled(false);
				Principal.getSelectViernes().setEnabled(false);
				break;
		}
	}

	private static void iniciarFechaSemana(int dia) {
		
		Calendar diaSemana = Calendar.getInstance();
		int fecha=diaSemana.get(Calendar.DATE);
		int Lunes=fecha-dia+1;
		ArrayList<Integer>semana = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			semana.add(Lunes+i);
		}
		Principal.getLunes().setText(semana.get(0).toString());
		Principal.getMartes().setText(semana.get(1).toString());
		Principal.getMiercoles().setText(semana.get(2).toString());
		Principal.getJueves().setText(semana.get(3).toString());
		Principal.getViernes().setText(semana.get(4).toString());
	}

	@SuppressWarnings("unchecked" )
	private static void ActualizarGrupos() {
		Principal.getComboGrupoA().removeAllItems();
		Principal.getComboGrupoB().removeAllItems();
		Principal.getComboGrupoC().removeAllItems();
		Principal.getComboGrupoD().removeAllItems();
		Principal.getComboGrupoE().removeAllItems();
		Principal.getComboGrupoF().removeAllItems();

		Principal.getComboGrupoA().addItem("Seleccione nuevo equipo");
		Principal.getComboGrupoB().addItem("Seleccione nuevo equipo");
		Principal.getComboGrupoC().addItem("Seleccione nuevo equipo");
		Principal.getComboGrupoD().addItem("Seleccione nuevo equipo");
		Principal.getComboGrupoE().addItem("Seleccione nuevo equipo");
		Principal.getComboGrupoF().addItem("Seleccione nuevo equipo");


		ResultSet rsListaEquipos=Modelo.listarEquiposPorFaseSinGrupo(1);
		try {
			while (rsListaEquipos.next()) {
				String equipos=rsListaEquipos.getString("nombre_equipo");
				Principal.getComboGrupoA().addItem(equipos);
				Principal.getComboGrupoB().addItem(equipos);
				Principal.getComboGrupoC().addItem(equipos);
				Principal.getComboGrupoD().addItem(equipos);
				Principal.getComboGrupoE().addItem(equipos);
				Principal.getComboGrupoF().addItem(equipos);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		actualizarListaGrupos();
	}

	private static void actualizarListaGrupos() {
		DefaultListModel<String> listaEquiposGrupoA=new DefaultListModel<>();
		DefaultListModel<String> listaEquiposGrupoB=new DefaultListModel<>();
		DefaultListModel<String> listaEquiposGrupoC=new DefaultListModel<>();
		//OBTENEMOS LA INFORMACION PARA MOSTRARLO EN LA VISTA
		//RELLENAMOS LAS LISTAS DE TODOS LOS GRUPOS
		ResultSet rsListaGrupoA=Modelo.listaEquipoPorGrupo(1), 
				rsListaGrupoB=Modelo.listaEquipoPorGrupo(2), 
				rsListaGrupoC=Modelo.listaEquipoPorGrupo(3);
		try {
			while (rsListaGrupoA.next()) {
				listaEquiposGrupoA.addElement(rsListaGrupoA.getString(2).toString());
			}
			while (rsListaGrupoB.next()) {
				listaEquiposGrupoB.addElement(rsListaGrupoB.getString(2));
			}
			while (rsListaGrupoC.next()) {
				listaEquiposGrupoC.addElement(rsListaGrupoC.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Principal.getListA().setModel(listaEquiposGrupoA);
		Principal.getListB().setModel(listaEquiposGrupoB);
		Principal.getListC().setModel(listaEquiposGrupoC);
	}
	//LIMPIAR CAMPOS
	public static void limpiarCamposPrincipal() {
		Principal.getNombreAlumno().setText("");
		Principal.getCursoAlumno().setSelectedItem("Seleccione curso");
		Principal.getAjedrezSelect().setSelected(false);
		Principal.getFutbolSelect().setSelected(false);
		Principal.getTenisSelect().setSelected(false);
		Principal.getObservaciones().setText("Observaciones");
	}
	
	public static ActionListener abrirFrameJugador(int i) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Jugadores frame = null;
				switch (i) {
				case 1:
					try {
						frame=new Jugadores(i);
			           
			        } catch (Exception e1) {
			        	System.out.println("Erro al abrir la ventana jugadores");
			        }
					break;
				case 2:
					try {
			            frame=new Jugadores(i);
			           
			        } catch (Exception e1) {
			        	System.out.println("Erro al abrir la ventana jugadores");
			        }
					break;	
				case 3:
					try {
			            frame=new Jugadores(i);
			           
			        } catch (Exception e1) {
			        	System.out.println("Erro al abrir la ventana jugadores");
			        }
					break;	
				case 4:
					try {
			            frame=new Jugadores(i);
			            
			        } catch (Exception e1) {
			        	System.out.println("Erro al abrir la ventana jugadores");
			        }
					break;	
				default:
					try {
			            frame=new Jugadores(10);
			           
			        } catch (Exception e1) {
			        	System.out.println("Erro al abrir la ventana jugadores");
			        }
					break;
				}
				 frame.setVisible(true);
				 frame.setLocationRelativeTo(null);
		         RSAnimation.setBajar(-960, 100, 1, 5, frame);
			}
		};
	}
	public static MouseListener mostrarJugadorSeleccionado(int i) {
		return new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				limpiarCamposJugadores();
				String[] JugadorListado = Jugadores.getListJugadores().getSelectedValue().toString().split("-");
				String[] datosJugador=new String[8];
				id_jugador=JugadorListado[0];
				datosJugador=Modelo.obtenerJugador(id_jugador).toArray(datosJugador);
				
				String nombre=datosJugador[1], 
						curso=datosJugador[2], 
						competicion=datosJugador[3], 
						rondaAjedrez=datosJugador[4], 
						rondaTenis=datosJugador[5], 
						observaciones=datosJugador[6], 
						nombreEquipo="none";
				if (i==1 || i>5) {
					nombreEquipo=Modelo.obtenerNombreEquipoPorID(datosJugador[7]);
				}
				//RELLENAMOS LOS CAMPOS CON LOS DATOS DEL JUGADOR
				Jugadores.getNombreJugador().setText(nombre);
				Jugadores.getComboCurso().setSelectedItem(curso);
				if (competicion.contains("F")) {
					Jugadores.getFutbolSelect().setSelected(true);
				}if (competicion.contains("T")) {
					Jugadores.getTenisSelect().setSelected(true);
				}if (competicion.contains("A")) {
					Jugadores.getAjedrezSelect().setSelected(true);
				}
				Jugadores.getSpinner_1().setValue(Integer.parseInt(rondaAjedrez));
				Jugadores.getSpinner().setValue(Integer.parseInt(rondaTenis));
				Jugadores.getObservaciones().setText(observaciones);
				Jugadores.getBtnInsertar().setText("Alumno nuevo");
				
			}
		};
	}
	
	public static void actualizarPanelAjedrez() {
		int maximo=comprobarNumeroFases("ajedrez").get(comprobarNumeroFases("ajedrez").size()-1), minimo=1;
		Principal.getFaseX().setText("Fase "+nFase);
		Principal.getBarraAjedrez().setMaximum(maximo);
		Principal.getBarraAjedrez().setMinimum(minimo);
		Principal.getBarraAjedrez().setValue(nFase);
		Principal.getPartida1Ajedrez().removeAllItems();
		Principal.getPartida2Ajedrez().removeAllItems();
		Principal.getPartida3Ajedrez().removeAllItems();
		actualizarTablaAjedrez(nFase);
	}
	
	private static void actualizarTablaAjedrez(int nFase) {
		ResultSet rsListaPartidasAjedrez=Modelo.listaPartidasFase(nFase);
		//PREPARAMOS LA TABLA AJEDREZ
		String columnas[]= {"ID Partida", "Jugador 1", "Jugador 2", "Veces ganado J1", "Veces ganado J2", "Fase"};
		DefaultTableModel tablaAjedrez;
		tablaAjedrez=new DefaultTableModel(null, columnas);
		//RELLENAMOS CON DATOS
		try {
			while (rsListaPartidasAjedrez.next()) {
				String partidas[]={rsListaPartidasAjedrez.getString(1), 
						rsListaPartidasAjedrez.getString(2), 
						rsListaPartidasAjedrez.getString(3), 
						rsListaPartidasAjedrez.getString(4), 
						rsListaPartidasAjedrez.getString(5), 
						rsListaPartidasAjedrez.getString(6)};
				tablaAjedrez.addRow(partidas);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		Principal.getTablaAjedrez().setModel(tablaAjedrez);
		
	}
	public static ActionListener actualizarJugador(int i) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String nombre, curso, seleccion = null, ronda_ajedrez, ronda_tenis, observaciones = "", id_equipo;
				nombre=Jugadores.getNombreJugador().getText().toString();
				curso=Jugadores.getComboCurso().getSelectedItem().toString();
				ronda_ajedrez=Jugadores.getSpinner_1().getValue().toString();
				ronda_tenis=Jugadores.getSpinner().getValue().toString();
				id_equipo="Seleccione un equipo";
				observaciones=Jugadores.getObservaciones().getText().toString();
				if (observaciones.equals("Observaciones")) {
					observaciones="";
				}
				if (nombre.isEmpty() || curso.equals("Seleccione curso")) {
					//INTRODUCIR VENTANA DE ALERTA
			        JOptionPane.showMessageDialog(null, "Aseg�rese de que todos los campos est�n rellenados: \n -NOMBRE \n -CURSO \n -TORNEO", "Datos incorrectos", 0, null);
				}else {
					switch (i) {
					case 1:
						seleccion = "F";
						if (id_equipo.equals("Seleccione un equipo")) {
							Modelo.actualizarJugador(id_jugador, nombre, curso, seleccion, observaciones, ronda_ajedrez, ronda_tenis, id_equipo, 0);
						}else {
							Modelo.actualizarJugador(id_jugador, nombre, curso, seleccion, observaciones, ronda_ajedrez, ronda_tenis, id_equipo, 1);
						}
						new rojerusan.RSNotifyFade("Jugador", "Alumno insertado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);
						actualizarFrameJugadores(i);
						limpiarCamposJugadores();
						break;
						
					case 2:
						seleccion = "A";
						if (nombre.isEmpty() || curso.equals("Seleccione curso")) {
							//INTRODUCIR VENTANA DE ALERTA
					        JOptionPane.showMessageDialog(null, "Rellene todos los campos: \n -NOMBRE \n -CURSO \n -TORNEO", "Datos incorrectos", 0, null);
							
						}else {
							if (observaciones.equals("Observaciones")) {
								observaciones="";
							}
							Modelo.actualizarJugador(id_jugador, nombre, curso, seleccion, observaciones, ronda_ajedrez, ronda_tenis, id_equipo, 0);
							new rojerusan.RSNotifyFade("Jugador", "Alumno insertado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);
							actualizarFrameJugadores(i);
							limpiarCamposJugadores();
						}
						break;
						
					case 3:
						seleccion = "T";					
						if (observaciones.equals("Observaciones")) {
							observaciones="";
						}
						Modelo.actualizarJugador(id_jugador, nombre, curso, seleccion, observaciones, ronda_ajedrez, ronda_tenis, id_equipo, 0);
						new rojerusan.RSNotifyFade("Jugador", "Alumno insertado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);
						actualizarFrameJugadores(i);
						limpiarCamposJugadores();
						break;
					case 4:
						seleccion = "";
						//Rellenamos la seleccion de torneo
						if (Vista.Jugadores.getFutbolSelect().isSelected() && !Vista.Jugadores.getAjedrezSelect().isSelected() && !Vista.Jugadores.getTenisSelect().isSelected()) {
							seleccion="F";
						}if (Vista.Jugadores.getFutbolSelect().isSelected() && Vista.Jugadores.getAjedrezSelect().isSelected() && !Vista.Jugadores.getTenisSelect().isSelected()) {
							seleccion="FA";
							if (ronda_ajedrez.equals("0")) {
								ronda_ajedrez="1";
							}
						}if (Vista.Jugadores.getFutbolSelect().isSelected() && !Vista.Jugadores.getAjedrezSelect().isSelected() && Vista.Jugadores.getTenisSelect().isSelected()) {
							seleccion="FT";
							if (ronda_tenis.equals("0")) {
								ronda_tenis="1";
							}
						}if (Vista.Jugadores.getFutbolSelect().isSelected() && Vista.Jugadores.getAjedrezSelect().isSelected() && Vista.Jugadores.getTenisSelect().isSelected()) {
							seleccion="FTA";
							if (ronda_tenis.equals("0")) {
								ronda_tenis="1";
							}if (ronda_ajedrez.equals("0")) {
								ronda_ajedrez="1";
							}
						}if (!Vista.Jugadores.getFutbolSelect().isSelected() && Vista.Jugadores.getAjedrezSelect().isSelected() && !Vista.Jugadores.getTenisSelect().isSelected()) {
							seleccion="A";
							if (ronda_ajedrez.equals("0")) {
								ronda_ajedrez="1";
							}
						}if (!Vista.Jugadores.getFutbolSelect().isSelected() && Vista.Jugadores.getAjedrezSelect().isSelected() && Vista.Jugadores.getTenisSelect().isSelected()) {
							seleccion="AT";
							if (ronda_tenis.equals("0")) {
								ronda_tenis="1";
							}if (ronda_ajedrez.equals("0")) {
								ronda_ajedrez="1";
							}
						}if (!Vista.Jugadores.getFutbolSelect().isSelected() && !Vista.Jugadores.getAjedrezSelect().isSelected() && Vista.Jugadores.getTenisSelect().isSelected()) {
							seleccion="T";
							if (ronda_tenis.equals("0")) {
								ronda_tenis="1";
							}
						}if (observaciones.equals("Obervaciones")) {
							observaciones="";
						}if (nombre.isEmpty() || curso.equals("Seleccione curso") || seleccion.equals("")) {
							//INTRODUCIR VENTANA DE ALERTA
					        JOptionPane.showMessageDialog(null, "Rellene todos los campos: \n -NOMBRE \n -CURSO \n -TORNEO", "Datos incorrectos", 0, null);
						}else {
							if (id_equipo.equals("Seleccione un equipo")) {
								Modelo.actualizarJugador(id_jugador, nombre, curso, seleccion, observaciones, ronda_ajedrez, ronda_tenis, id_equipo, 2);
							} else {
								Modelo.actualizarJugador(id_jugador, nombre, curso, seleccion, observaciones, ronda_ajedrez, ronda_tenis, id_equipo, 1);
							}
							actualizarFrameJugadores(i);
							limpiarCamposJugadores();
							new rojerusan.RSNotifyFade("Jugador", "Alumno modificado",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);
						}
						break;
					}
				}
			}
		};
	}
	public static ActionListener siguienteTablaAjedrez() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int faseMaxima=comprobarNumeroFases("ajedrez").get(comprobarNumeroFases("ajedrez").size()-1);
				if (nFase<faseMaxima) {
					nFase++;
					actualizarPanelAjedrez();
					if (nFase==faseMaxima) {
						mostrarVencedorAjedrez();
					}
				}else {
					System.out.println("NO HAY MAS FASES");
				}
			}
			private void mostrarVencedorAjedrez() {
				String ganador=Modelo.obtenerGanadorAjedrez(nFase);
				if (!ganador.equals("")) {
					JOptionPane.showMessageDialog(null, ganador, "CAMPE�N DEL TORNEO DE AJEDREZ", 0);
				}else {
					JOptionPane.showMessageDialog(null, "A�n no hay campe�n", "CAMPE�N DEL TORNEO DE AJEDREZ", 0);
				}
			}
		};
	}
	public static ActionListener anteriorTablaAjedrez() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int faseMinima=1;
				if (nFase>faseMinima) {
					nFase--;
					actualizarPanelAjedrez();
				} else {
					System.out.println("no se puede ir mas atras");
				}
			}
		};
	}
	
	public static ArrayList<Integer> comprobarNumeroFases(String tenis) {
		ArrayList<Integer>NJugadoresDeTorneo= new ArrayList<>();
		int numerosTotalJugadores, 
			minimoFases=1, 
			maximoFases=1;
		if (tenis.equals("tenis")) {
			numerosTotalJugadores=Modelo.countJugadoresTenis();
			System.out.println(numerosTotalJugadores + " jugadores de tenis");
			NJugadoresDeTorneo.add(numerosTotalJugadores);
			NJugadoresDeTorneo.add(maximoFases);
			while (numerosTotalJugadores>1) {
				if (numerosTotalJugadores%2==0) {
					maximoFases++;
					numerosTotalJugadores=numerosTotalJugadores/2;
				}else {
					maximoFases++;
					numerosTotalJugadores=(Integer)numerosTotalJugadores/2+1;
				}
				NJugadoresDeTorneo.add(numerosTotalJugadores);
				NJugadoresDeTorneo.add(maximoFases);
			}
		}else {
			numerosTotalJugadores=Modelo.countJugadoresAjedrez();
			System.out.println(numerosTotalJugadores+" jugadores de ajedrez");
			NJugadoresDeTorneo.add(numerosTotalJugadores);
			NJugadoresDeTorneo.add(maximoFases);
			while (numerosTotalJugadores>1) {
				if (numerosTotalJugadores%2==0) {
					maximoFases++;
					numerosTotalJugadores=numerosTotalJugadores/2;
				}else {
					maximoFases++;
					numerosTotalJugadores=(Integer)numerosTotalJugadores/2+1;
					//System.out.println("es impar"+numerosTotalJugadores+"----"+maximoFases);
				}
				NJugadoresDeTorneo.add(numerosTotalJugadores);
				NJugadoresDeTorneo.add(maximoFases);
			}
		}	
		
		return NJugadoresDeTorneo;
	}
	
	public static ArrayList<Integer> idJugadoresAjedrezDeFase(){
		ResultSet rsJugadoresAjedrez=Modelo.listarJugadoresAjedrezPorFase(nFase);
		ArrayList<Integer>idJugadores=new ArrayList<>();
		try {
			while(rsJugadoresAjedrez.next()) {
				idJugadores.add(rsJugadoresAjedrez.getInt(1));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return idJugadores;
	}
	public static ActionListener generarPartidasAjedrez() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Coprobamos si ya se han generado partidas de ajedrez...");
				if (comprobarPartidas(nFase)) {
					System.out.println("Ya se han generado las partidas de ajedrez");
				}else {
					if (nFase==1) {
						ArrayList<Integer>listaJugadoresDeFase=idJugadoresAjedrezDeFase();
						int nTodalesJugadores=listaJugadoresDeFase.size();
						for (int i = 1; i < nTodalesJugadores; i++) {
							if (i%2!=0) {
								Modelo.crearPartidas(listaJugadoresDeFase.get(i), listaJugadoresDeFase.get(i-1), nFase);
								String idPartida=Modelo.obtenerIDPartidaPorJugador(listaJugadoresDeFase.get(i));
								String fechaPartida="01/08/2018";
								Modelo.crearPartidasDeAjedrez(listaJugadoresDeFase.get(i), listaJugadoresDeFase.get(i-1), nFase, idPartida, fechaPartida);
							}if (i%2==0 && nTodalesJugadores%2!=0 && i==(nTodalesJugadores-1)) {
								Modelo.actualizarFaseJugador(2,nFase+1, listaJugadoresDeFase.get(i));
							}
						}
						System.out.println("Partidas de ajedrez generadas para la Fase "+nFase);
					}else {
						ArrayList<Integer>listaJugadoresDeFase=idJugadoresAjedrezDeFase();
						//COMPROBAMOS QUE EL NUMERO DE PARTIDAS ES EL CORRECTO PARA CREAR LA SIGUIENTE FASE
						if (listaJugadoresDeFase.size()==comprobarNumeroFases("ajedrez").get(nFase*2-2)) {
							//CREAMOS PARTIDAS DE LA FASE QUE SE QUIERE MOSTRAR EN LA TABLA
							for (int i = 1; i < listaJugadoresDeFase.size(); i++) {
								if (i%2!=0) {
									Modelo.crearPartidas(listaJugadoresDeFase.get(i), listaJugadoresDeFase.get(i-1), nFase);
									String idPartida=Modelo.obtenerIDPartidaPorJugador(listaJugadoresDeFase.get(i));
									String fechaPartida="01/08/2018";
									Modelo.crearPartidasDeAjedrez(listaJugadoresDeFase.get(i), listaJugadoresDeFase.get(i-1), nFase, idPartida, fechaPartida);
								}if (i%2==0 && i==(listaJugadoresDeFase.size()-1)) {
									Modelo.actualizarFaseJugador(2,nFase+1, listaJugadoresDeFase.get(i));
								}
							}
							System.out.println("Partidas de ajedrez generadas para la fase: "+nFase);
							new rojerusan.RSNotifyFade("Partidas de ajedrez", "Partidas de ajedrez insertadas correctamente",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);		
						}else {
							System.out.println("No exiten el numero necesario para crear fase, compruebe que las partidas han finalizado todas");
							JOptionPane.showMessageDialog(null, "No exiten el numero necesario para crear fase, compruebe que las partidas han finalizado todas", "ERROR", 0);
						}			
					}
				}
				actualizarTablaAjedrez(nFase);
			}
		};
	}
	protected static boolean comprobarPartidas(int nFase) {
		ArrayList<int[]>listaPartidaN=new ArrayList<>();
		ResultSet rsListaPartidas=Modelo.listaPartidasFase(nFase);
		try {
			while (rsListaPartidas.next()) {
				int[] partidaX= { 
						rsListaPartidas.getInt(2), 
						rsListaPartidas.getInt(3), 
						rsListaPartidas.getInt(4), 
						rsListaPartidas.getInt(5), 
						};	
				listaPartidaN.add(partidaX);
			}
		} catch (SQLException e1) {
			
			e1.printStackTrace();
		}
		if (!listaPartidaN.isEmpty()) {
			return true;
		}else {
			return false;
		}
	}
	
	public static MouseListener mostrarPartidaSeleccionadaAjedrez() {
		return new MouseAdapter() {
			@SuppressWarnings("unchecked")
			public void mouseClicked(MouseEvent arg0) {
				//PREPARAMOS
				Principal.getPartida1Ajedrez().removeAllItems();
				Principal.getPartida2Ajedrez().removeAllItems();
				Principal.getPartida3Ajedrez().removeAllItems();
				listaDePartidasDeAjedrez=new ArrayList<>();
				int idPartida=Integer.parseInt(Principal.getTablaAjedrez().getValueAt(Principal.getTablaAjedrez().getSelectedRow(), 0).toString());
				Date fechaPartidaAjedrez = null;
				String tituloComboAjedrez="Seleccione el ganador";
				String nombreJ1=Modelo.obtenerNombreJugadorPorID(Principal.getTablaAjedrez().getValueAt(Principal.getTablaAjedrez().getSelectedRow(), 1).toString());
				String nombreJ2=Modelo.obtenerNombreJugadorPorID(Principal.getTablaAjedrez().getValueAt(Principal.getTablaAjedrez().getSelectedRow(), 2).toString());
				
				Principal.getPartida1Ajedrez().addItem(tituloComboAjedrez);
				Principal.getPartida2Ajedrez().addItem(tituloComboAjedrez);
				Principal.getPartida3Ajedrez().addItem(tituloComboAjedrez);
				//OBTENEMOS LA PARTIDA DE AJEDREZ DE ELEMENTO SELECCIONADO DEL AJEDREZ
				ResultSet rsPartidasAjedrezDeID=Modelo.obtenerPartidaAjedrezPorID(idPartida);
				try {
					while (rsPartidasAjedrezDeID.next()) {
						Object[] partidas= {
						rsPartidasAjedrezDeID.getString(6),
						rsPartidasAjedrezDeID.getString(7),
						rsPartidasAjedrezDeID.getString(1)
						};
						listaDePartidasDeAjedrez.add(partidas);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				//RELLENAMOS LOS COMBOS CON LOS JUGADORES DE LA PARTIDA
				Principal.getPartida1Ajedrez().addItem(nombreJ1);
				Principal.getPartida2Ajedrez().addItem(nombreJ1);
				Principal.getPartida3Ajedrez().addItem(nombreJ1);
				Principal.getPartida1Ajedrez().addItem(nombreJ2);
				Principal.getPartida2Ajedrez().addItem(nombreJ2);
				Principal.getPartida3Ajedrez().addItem(nombreJ2);
				if (!listaDePartidasDeAjedrez.isEmpty() && listaDePartidasDeAjedrez.size()==3) {
					for (int i = 0; i < listaDePartidasDeAjedrez.size(); i++) {
						fechaPartidaAjedrez=ParseFecha(listaDePartidasDeAjedrez.get(i)[0].toString());
						if (i==0) {
							//COGEMOS LOS DATOS OBTENIDOS Y LOS PASAMOS A LA VISTA
							Principal.getFechaPartida1().setDatoFecha(fechaPartidaAjedrez);
							//OBTENEMOS EL GANADOR Y LO SOLOCAMOS EN EL COMBO
							Principal.getPartida1Ajedrez().setSelectedItem(listaDePartidasDeAjedrez.get(i)[1].toString());
						}if (i==1) {
							//COGEMOS LOS DATOS OBTENIDOS Y LOS PASAMOS A LA VISTA
							Principal.getFechaPartida2().setDatoFecha(fechaPartidaAjedrez);
							Principal.getPartida2Ajedrez().setSelectedItem(listaDePartidasDeAjedrez.get(i)[1].toString());
						}else {
							//COGEMOS LOS DATOS OBTENIDOS Y LOS PASAMOS A LA VISTA
							Principal.getFechaPartida3().setDatoFecha(fechaPartidaAjedrez);
							Principal.getPartida3Ajedrez().setSelectedItem(listaDePartidasDeAjedrez.get(i)[1].toString());
						}
					}
				}else {
					System.out.println("No hay partidas de ajedrez creadas");
				}	
				System.out.println("Mostrando informacion del elemento seleccionado de la tabla");
			}
		};
	}
	
	public static Date ParseFecha(String fecha){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaDate = null;
        try {
           fechaDate = sdf.parse(fecha);
        } 
        catch (ParseException ex) 
        {
            System.out.println(ex);
        }
        
        return fechaDate;
    }
	public static ActionListener actualizarPartidaDeAjedrez() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Principal.getPartida1Ajedrez().getItemCount()>0) {
					int idLocal=Integer.parseInt(Principal.getTablaAjedrez().getValueAt(Principal.getTablaAjedrez().getSelectedRow(), 1).toString()), 
							idVisitante=Integer.parseInt(Principal.getTablaAjedrez().getValueAt(Principal.getTablaAjedrez().getSelectedRow(), 2).toString()),
							idPartida=Integer.parseInt(Principal.getTablaAjedrez().getValueAt(Principal.getTablaAjedrez().getSelectedRow(), 0).toString());
					int indexGanadorP1=Principal.getPartida1Ajedrez().getSelectedIndex(), 
							indexGanadorP2=Principal.getPartida2Ajedrez().getSelectedIndex(), 
							indexGanadorP3=Principal.getPartida3Ajedrez().getSelectedIndex();
					int partidasGanadasLocal=Integer.parseInt(Principal.getTablaAjedrez().getValueAt(Principal.getTablaAjedrez().getSelectedRow(), 4).toString()), 
							partidasGanadasVisitante=Integer.parseInt(Principal.getTablaAjedrez().getValueAt(Principal.getTablaAjedrez().getSelectedRow(), 5).toString());
					String idP1=listaDePartidasDeAjedrez.get(0)[2].toString(), 
							idP2=listaDePartidasDeAjedrez.get(1)[2].toString(), 
							idP3=listaDePartidasDeAjedrez.get(2)[2].toString() ;
					SimpleDateFormat sdf= new SimpleDateFormat("MM/dd/yyyy");
					
					String fechaP1=sdf.format(Principal.getFechaPartida1().getDatoFecha()), 
							fechaP2=sdf.format(Principal.getFechaPartida2().getDatoFecha()), 
							fechaP3=sdf.format(Principal.getFechaPartida3().getDatoFecha());
					System.out.println(fechaP1);
					
					String vencedorP1=Principal.getPartida1Ajedrez().getSelectedItem().toString(), 
							vencedorP2=Principal.getPartida2Ajedrez().getSelectedItem().toString(), 
							vencedorP3=Principal.getPartida3Ajedrez().getSelectedItem().toString();
					
					Modelo.actualizarPartidaDeAjedrez(idP1, fechaP1, vencedorP1);
					Modelo.actualizarPartidaDeAjedrez(idP2, fechaP2, vencedorP2);
					Modelo.actualizarPartidaDeAjedrez(idP3, fechaP3, vencedorP3);
					System.out.println("Fechas actualizadas correctamente");

					//COMPROBAMOS LOS GANADORES PARA LUEGO PASAR A LA SIGUIENTE RONDA
					if (indexGanadorP1!=0) {
						System.out.println(indexGanadorP1+"\n"+indexGanadorP2+"\n"+indexGanadorP3);
						if (indexGanadorP2==0 && indexGanadorP3==0) {
							if (indexGanadorP1==1) {
								Modelo.actualizarPartida(1, "local", idPartida);
								Modelo.actualizarPartida(0, "visitante", idPartida);
							}else {
								Modelo.actualizarPartida(1, "visitante", idPartida);
								Modelo.actualizarPartida(0, "local", idPartida);
							}
						}else{
							if ((indexGanadorP2==indexGanadorP1)) {
								if (indexGanadorP1==1) {
									Modelo.actualizarPartida(2, "local", idPartida);
									Modelo.actualizarPartida(0, "visitante", idPartida);
									Modelo.actualizarFaseJugador(2, nFase+1, idLocal);
									System.out.println("Pasa de ronda el jugador local");
								}else {
									Modelo.actualizarPartida(2, "visitante", idPartida);
									Modelo.actualizarPartida(0, "local", idPartida);
									Modelo.actualizarFaseJugador(2, nFase+1, idVisitante);
									System.out.println("Pasa de ronda el jugador visitante");
								}
							}else {
								if (indexGanadorP2==2) {
									Modelo.actualizarPartida(1, "visitante", idPartida);
								}else {
									Modelo.actualizarPartida(1, "local", idPartida);
								}
							}if (indexGanadorP3==1) {
								Modelo.actualizarPartida(2, "local", idPartida);
								Modelo.actualizarFaseJugador(2, nFase+1, idLocal);
								System.out.println("Pasa de ronda el jugador local");
							}if(indexGanadorP3==2) {
								Modelo.actualizarPartida(2, "visitante", idPartida);
								Modelo.actualizarFaseJugador(2, nFase+1, idVisitante);
								System.out.println("Pasa de ronda el jugador visitante");
							}
						}
						actualizarTablaAjedrez(nFase);
						//LIMPIAMOS CAMPOS
						Principal.getPartida1Ajedrez().removeAllItems();
						Principal.getPartida2Ajedrez().removeAllItems();
						Principal.getPartida3Ajedrez().removeAllItems();
						new rojerusan.RSNotifyFade("Partida de ajedrez", "Partida guardada",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);		
					}else {
						System.out.println("Primero debe de poner un ganador almenos en una partida");
						JOptionPane.showMessageDialog(null, "Primero debe de poner un ganador almenos en una partida", "ERROR", 0);
					}
				}else {
					System.out.println("Debe de seleccionar un elemento de la tabla.");
					JOptionPane.showMessageDialog(null, "Debe de seleccionar un elemento de la tabla.", "ERROR", 0);
				}	
			}
		};
	}
	public static ActionListener siguienteTablaTenis() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int faseMaxima=comprobarNumeroFases("tenis").get(comprobarNumeroFases("tenis").size()-1);
				
				if (nFaseTenis<faseMaxima) {
					nFaseTenis++;
					if (nFaseTenis==faseMaxima) {
						mostrarVencedorTenis();
					}
					actualizarPanelTenis();
				}else {
					System.out.println("No hay mas fases");
				}
			}

			private void mostrarVencedorTenis() {
				String ganador=Modelo.obtenerGanadorTenis(nFaseTenis);
				if (!ganador.equals("")) {
					JOptionPane.showMessageDialog(null, ganador, "CAMPE�N DEL TORNEO DE TENIS DE MESA", 0);
				}else {
					JOptionPane.showMessageDialog(null, "A�n no hay campe�n", "CAMPE�N DEL TORNEO DE TENIS DE MESA", 0);
				}
			}
		};
	}
	public static ActionListener anteriorTablaTenis() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int faseMinima=1;
				if (nFaseTenis>faseMinima) {
					nFaseTenis--;
					actualizarPanelTenis();
				} else {
					System.out.println("No se puede ir mas atras");
				}
			}
		};
	}
	public static ActionListener generarPartidasTenis() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Coprobamos si ya se han generado partidas de tenis...");
				if (comprobarPartidasTenis()) {
					System.out.println("Ya se han generado las partidas de tenis");
					JOptionPane.showMessageDialog(null, "Ya han sido generada las partidas anteriormete", "Atenci�n", 0);
				}else {
					ArrayList<Integer>listaJugadoresDeFase=idJugadoresTenisDeFase();
					int numeroDeJugadoresTenis=listaJugadoresDeFase.size();
					//COMPROBAMOS QUE EL NUMERO DE PARTIDAS ES EL CORRECTO PARA CREAR LA SIGUIENTE FASE
					if (numeroDeJugadoresTenis==comprobarNumeroFases("tenis").get(nFaseTenis*2-2)) {
						//CREAMOS PARTIDAS DE LA FASE QUE SE QUIERE MOSTRAR EN LA TABLA
						for (int i = 1; i < listaJugadoresDeFase.size(); i++) {
							if (i%2!=0) {
								String fechaPartida="01/08/2018";
								Modelo.crearPartidasDeTenis(listaJugadoresDeFase.get(i), listaJugadoresDeFase.get(i-1), nFaseTenis, fechaPartida);
							}if (i%2==0 && i==(listaJugadoresDeFase.size()-1)) {
								Modelo.actualizarFaseJugador(1,nFaseTenis+1, listaJugadoresDeFase.get(i));
							}
						}
						System.out.println("Partidas de tenis generadas para la fase: "+nFaseTenis);
						new rojerusan.RSNotifyFade("Tenis de mesa", "Partidas generadas exitosamente",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.SUCCESS).setVisible(true);		
					}else {
						System.out.println("No exiten el numero necesario para crear fase, compruebe que las partidas han finalizado todas");
					}			
					
				}
				actualizarTablaTenis();
			}
		};
	}
	
	protected static ArrayList<Integer> idJugadoresTenisDeFase() {
		ResultSet rsJugadoresAjedrez=Modelo.listarJugadoresTenisPorFase(nFaseTenis);
		ArrayList<Integer>idJugadores=new ArrayList<>();
		try {
			while(rsJugadoresAjedrez.next()) {
				idJugadores.add(rsJugadoresAjedrez.getInt(1));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return idJugadores;
	}
	
	protected static boolean comprobarPartidasTenis() {
		ArrayList<Integer>listaPartidasAjedrez=new ArrayList<>();
		ResultSet rsListaPartidas=Modelo.listaPartidasTenisFase(nFaseTenis);
		try {
			while (rsListaPartidas.next()) {
				System.out.println("...");
				listaPartidasAjedrez.add(rsListaPartidas.getInt(1));
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		if (!listaPartidasAjedrez.isEmpty()) {
			return true;
		}else {
			return false;
		}
	}
	public static MouseListener mostrarPartidaTenis() {
		return new MouseAdapter() {
			@SuppressWarnings("unchecked")
			public void mouseClicked(MouseEvent arg0) {
				System.out.println("Obtenemos los valores del elemento seleccionado de la tabla");
				String Vencedor=Principal.getTablaParatidasTenis().getValueAt(Principal.getTablaParatidasTenis().getSelectedRow(), 4).toString(), 
				idJugador1=Principal.getTablaParatidasTenis().getValueAt(Principal.getTablaParatidasTenis().getSelectedRow(), 1).toString(), 
				idJugador2=Principal.getTablaParatidasTenis().getValueAt(Principal.getTablaParatidasTenis().getSelectedRow(), 2).toString(), 
				fechaTenis=Principal.getTablaParatidasTenis().getValueAt(Principal.getTablaParatidasTenis().getSelectedRow(), 3).toString();
				Date fechaPartidaTenis=null;
				fechaPartidaTenis=ParseFecha(fechaTenis);
				String JugadorLocal=Modelo.obtenerNombreJugadorPorID(idJugador1),
						JugadorVisitante=Modelo.obtenerNombreJugadorPorID(idJugador2);
				//MOSTRAMOS LOS DATOS EN LA VISTA
				Principal.getFechaTenis().setDatoFecha(fechaPartidaTenis);
				Principal.getJugadorTenisLocal().setText(JugadorLocal);
				Principal.getJugadorTenisVisitante().setText(JugadorVisitante);
				if (Vencedor.equals(JugadorLocal)) {
					Principal.getGanadorJ1().setSelected(true);
					Principal.getGanadorJ2().setSelected(false);
				}if (Vencedor.equals(JugadorVisitante)) {
					Principal.getGanadorJ2().setSelected(true);
					Principal.getGanadorJ1().setSelected(false);
				}
			}
		};
	}
	public static MouseListener seleccionarGanadorTenis(String valor) {
		return new MouseAdapter() {
			@SuppressWarnings("unchecked")
			public void mouseClicked(MouseEvent arg0) {
				switch (valor) {
				case "local":
					Principal.getGanadorJ1().setSelected(true);
					Principal.getGanadorJ2().setSelected(false);
					break;

				case "visitante":
					Principal.getGanadorJ1().setSelected(false);
					Principal.getGanadorJ2().setSelected(true);
					break;
				}
			}
		};
	}
	
	public static ActionListener actualizarPartidaTenis() {		
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//OBTENEMOS LOS VALORES DE LA VISTA
				if (!Principal.getJugadorTenisLocal().getText().isEmpty()) {
					String idPartidaTenis=Principal.getTablaParatidasTenis().getValueAt(Principal.getTablaParatidasTenis().getSelectedRow(), 0).toString(),
							Vencedor = null,
							Fecha;
					Date fechaTenis=Principal.getFechaTenis().getDatoFecha();
					int idJugadorLocal=Integer.parseInt(Principal.getTablaParatidasTenis().getValueAt(Principal.getTablaParatidasTenis().getSelectedRow(), 1).toString());
					int idJugadorVisitante=Integer.parseInt(Principal.getTablaParatidasTenis().getValueAt(Principal.getTablaParatidasTenis().getSelectedRow(), 2).toString());
					if (Principal.getGanadorJ1().isSelected()) {
						Vencedor=Principal.getJugadorTenisLocal().getText().toString();
						Modelo.actualizarFaseJugador(1, nFaseTenis+1, idJugadorLocal);
						Modelo.actualizarFaseJugador(1, nFaseTenis, idJugadorVisitante);
					}if (Principal.getGanadorJ2().isSelected()) {
						Vencedor=Principal.getJugadorTenisVisitante().getText().toString();
						Modelo.actualizarFaseJugador(1, nFaseTenis+1, idJugadorVisitante);
						Modelo.actualizarFaseJugador(1, nFaseTenis, idJugadorLocal);
					}
					SimpleDateFormat sdf= new SimpleDateFormat("MM/dd/yyyy");
					Fecha=sdf.format(fechaTenis);
					Modelo.actalizarPartidaDeTenis(idPartidaTenis, Vencedor, Fecha);
					actualizarPanelTenis();
					new rojerusan.RSNotifyFade("Tenis de mesa", "Se han guaardado los resultados",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);		
				}else {
					JOptionPane.showMessageDialog(null, "Primeramente seleccione una partida de la tabla", "Error", 1);
					System.out.println("Primero debe de seleccionar una partida de la tabla");
				}			
			}
		};
	}
	public static ActionListener guardarConfiguracion() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//OBTENEMOS LA INFORMACION DE LA VISTA Y LA GUARDAMOS PARA LUEGO PASARLO AL MODELO
				String usuario=Principal.getUsuario().getText().toString(),  
						contrasennaSeguridad=Principal.getContrasenna().getText().toString(),
						contrasennaNueva=Principal.getContrasennaNueva().getText().toString(), 
						paginaFav=Principal.getPaginaFav().getText().toString();
				if (!contrasennaNueva.isEmpty()) {
					if (comprobarContrasenna(contrasennaSeguridad) && contrasennaNueva.length()<=8) {
						Modelo.actualizarConfiguracion(usuario, contrasennaNueva, paginaFav);
						ajustes(1);
					}else {
						System.out.println("Fallo al actualizar la contrase�a, posibles causas: \n "
								+ "-Contrase�a actual no v�lida \n "
								+ "-Comprueba que la longitud de la contrase�a nueva no supera los 8 caracteres");
					}
				}else {
					Modelo.actualizarConfiguracion(usuario, contrasenna, paginaFav);
				}
				Principal.getContrasenna().setText("");
				Principal.getContrasennaNueva().setText("");
				new rojerusan.RSNotifyFade("Configuraci�n", "Configuraci�n guardada",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.SUCCESS).setVisible(true);		
			}
		};
	}
	protected static boolean comprobarContrasenna(String contrasennaSeguridad) {
		ajustes(0);
		if (contrasenna.equals(contrasennaSeguridad)) {
			System.out.println("Contrase�a aceptada");
			return true;
		}else {
			System.out.println("Vuelva a introducir la contrase�a original");
			return false;
		}
	}
	public static ActionListener guardarEquipoEnGrupo() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//RECOGEMOS LOS EQUIPOS DE LOS COMBOS Y COMPARAMOS, PARA LUEGO A�ADIRLES UN GRUPO
				String equipoA=Principal.getComboGrupoA().getSelectedItem().toString(),
						equipoB=Principal.getComboGrupoB().getSelectedItem().toString(), 
						equipoC=Principal.getComboGrupoC().getSelectedItem().toString(), 
						equipoD=Principal.getComboGrupoD().getSelectedItem().toString(), 
						equipoE=Principal.getComboGrupoE().getSelectedItem().toString(), 
						equipoF=Principal.getComboGrupoF().getSelectedItem().toString();
				String mensajeSinEquio="Seleccione nuevo equipo";
				if (equipoA.equals(mensajeSinEquio) && 
						equipoB.equals(mensajeSinEquio) && 
						equipoC.equals(mensajeSinEquio) && 
						equipoD.equals(mensajeSinEquio) && 
						equipoE.equals(mensajeSinEquio) && 
						equipoF.equals(mensajeSinEquio)) {
					System.out.println("Para guardar los cambios, primero debe de haber cambios, selecciona un equipo.");	
				}else {
					int idEquipoA=Modelo.obtenerIdEquipo(equipoA),
							idEquipoB=Modelo.obtenerIdEquipo(equipoB),
							idEquipoC=Modelo.obtenerIdEquipo(equipoC),
							idEquipoD=Modelo.obtenerIdEquipo(equipoD),
							idEquipoE=Modelo.obtenerIdEquipo(equipoE),
							idEquipoF=Modelo.obtenerIdEquipo(equipoF);
					System.out.println(idEquipoA+"-"+idEquipoB);
					//UNA VEZ OBTENIDOS LOS IDS, AHORA ACTUALIZAMOS EL GRUPO DE LOS EQUIPOS
					if (!equipoA.equals(mensajeSinEquio)) {
						Modelo.actualizarGrupoDeEquipo(idEquipoA, 1, "");
					}if (!equipoB.equals(mensajeSinEquio)) {
						Modelo.actualizarGrupoDeEquipo(idEquipoB, 2, "");
					}if (!equipoC.equals(mensajeSinEquio)) {
						Modelo.actualizarGrupoDeEquipo(idEquipoC, 3, "");
					}if (!equipoD.equals(mensajeSinEquio)) {
						Modelo.actualizarGrupoDeEquipo(idEquipoD, 4, "");
					}if (!equipoE.equals(mensajeSinEquio)) {
						Modelo.actualizarGrupoDeEquipo(idEquipoE, 5, "");
					}if (!equipoF.equals(mensajeSinEquio)) {
						Modelo.actualizarGrupoDeEquipo(idEquipoF, 6, "");
					}
					ActualizarGrupos();
					new rojerusan.RSNotifyFade("Fase de grupos", "Equipo insertado correctamente",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.SUCCESS).setVisible(true);		
				}
			}
		};
	}
	public static ActionListener eliminarEquipoSeleccionado(String grupo) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				//RECOGEMOS LA INFORMACION DE LA VISTA PARA LUEGO ELIMINAR
				switch (grupo) {
				case "A":
					if (!Principal.getListA().isSelectionEmpty()) {
						String nombreEquipo=Principal.getListA().getSelectedValue().toString();
						int idEquipo=Modelo.obtenerIdEquipo(nombreEquipo);
						Modelo.actualizarGrupoDeEquipo(idEquipo, 1, "null");
						System.out.println("Equipo eliminado");
					}else {
						System.out.println("Seleccione primero un equipo de la lista para eliminar ");
					}
					break;
				case "B":
					if (!Principal.getListB().isSelectionEmpty()) {
						String nombreEquipo=Principal.getListB().getSelectedValue().toString();
						int idEquipo=Modelo.obtenerIdEquipo(nombreEquipo);
						Modelo.actualizarGrupoDeEquipo(idEquipo, 2, "null");
						System.out.println("Equipo eliminado");
					}else {
						System.out.println("Seleccione primero un equipo de la lista para eliminar ");
					}
					break;
				case "C":
					if (!Principal.getListC().isSelectionEmpty()) {
						String nombreEquipo=Principal.getListC().getSelectedValue().toString();
						int idEquipo=Modelo.obtenerIdEquipo(nombreEquipo);
						Modelo.actualizarGrupoDeEquipo(idEquipo, 3, "null");
						System.out.println("Equipo eliminado");
					}else {
						System.out.println("Seleccione primero un equipo de la lista para eliminar ");
					}
					break;
				case "D":
					if (!Principal.getListD().isSelectionEmpty()) {
						String nombreEquipo=Principal.getListD().getSelectedValue().toString();
						int idEquipo=Modelo.obtenerIdEquipo(nombreEquipo);
						Modelo.actualizarGrupoDeEquipo(idEquipo, 4, "null");
						System.out.println("Equipo eliminado");
					}else {
						System.out.println("Seleccione primero un equipo de la lista para eliminar ");
					}
					break;
				case "E":
					if (!Principal.getListE().isSelectionEmpty()) {
						String nombreEquipo=Principal.getListE().getSelectedValue().toString();
						int idEquipo=Modelo.obtenerIdEquipo(nombreEquipo);
						Modelo.actualizarGrupoDeEquipo(idEquipo, 5, "null");
						System.out.println("Equipo eliminado");
					}else {
						System.out.println("Seleccione primero un equipo de la lista para eliminar ");
					}
					break;
				case "F":
					if (!Principal.getListF().isSelectionEmpty()) {
						String nombreEquipo=Principal.getListF().getSelectedValue().toString();
						int idEquipo=Modelo.obtenerIdEquipo(nombreEquipo);
						Modelo.actualizarGrupoDeEquipo(idEquipo, 6, "null");
						System.out.println("Equipo eliminado");
					}else {
						System.out.println("Seleccione primero un equipo de la lista para eliminar ");
					}
					break;
				}
				ActualizarGrupos();
			}
		};
	}
	public static ActionListener generarPDF(int opcion) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				JFileChooser explorardor=new JFileChooser();
				String ruta;
				int i=explorardor.showSaveDialog(explorardor);
				if (i==JFileChooser.APPROVE_OPTION) {
					File f=explorardor.getSelectedFile();
					ruta=f.toString()+".pdf";
					System.out.println("LA RUTA ES: "+ruta);
					FileOutputStream pdf;
					try {
						pdf = new FileOutputStream(ruta);
						Document documentoPDF= new Document();
						PdfWriter.getInstance(documentoPDF, pdf);
						Paragraph titulo=new Paragraph();
						//titulo.add(new Chunk("TORNEO DE "+deporte+" FASE "+fase+" DE ELIMINATORIAS"));
						titulo.setAlignment(Element.ALIGN_CENTER);
						
						documentoPDF.setMargins(20, 20, 10, 10);
						documentoPDF.open();
						documentoPDF.add(ponerCabeceraIMG());
						documentoPDF.add(titulo);
						//ESCOGEMOS QUE TIPO DE CONTENIDO IR� DENTRO
						
						switch (opcion) {
						case 1:
							
							break;
						case 2:
							break;
						case 3:
							
							break;
						case 4:
							break;
						case 5:
							
							break;
						case 6:
							break;
						case 7:
							break;
						case 8:
							break;
						}
						//CERRAMOS EL DOCUMENTO Y SE CREAR� EL PDF
						documentoPDF.close();
					} catch (FileNotFoundException | DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else {
					System.out.println("No ha seleccionado niguna ruta para guardar el PDF");
				}
			}

			private Image ponerCabeceraIMG() {
				Image cabecera=null;
				try {
					cabecera=Image.getInstance("resources/Banner/splash.png");
					cabecera.scaleAbsolute(500, 100);
					cabecera.setAlignment(Element.ALIGN_CENTER);
				} catch (BadElementException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return cabecera;
			}

			private Paragraph obtenerLineaJugador(String jugador) {
				Font fuente=new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);
				Paragraph Jugador=new Paragraph();
				Chunk c=new Chunk();
				c.append(jugador);
				c.setFont(fuente);
				Jugador.setTabSettings(new TabSettings(10F));
				Jugador.add(Chunk.TABBING);
				Jugador.add(c);
				return Jugador;
			}
			private Paragraph obtenerLineaFecha(String fecha) {
				Font fuente=new Font(Font.FontFamily.SYMBOL, 10, Font.BOLD);
				Paragraph Jugador=new Paragraph();
				//Jugador.setAlignment(Element.ALIGN_CENTER);
				Chunk c=new Chunk();
				c.append(fecha);
				c.setFont(fuente);
				c.setBackground(BaseColor.YELLOW);
				Jugador.setTabSettings(new TabSettings(200F));
				Jugador.add(new Chunk("-"));
				Jugador.add(Chunk.TABBING);
				Jugador.add(c);
				return Jugador;
			}
		};
	}
	public static ActionListener resetBBDD() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int confirmado = JOptionPane.showConfirmDialog(null, "Se va a borrar toda la base de datos, �est�s seguro?", "CUIDADO!", 2);
					if (JOptionPane.OK_OPTION == confirmado) {
						/*
					String nombreBBDD="GestorDeTorneosBBDD";
					File bbdd=new File(nombreBBDD);
					System.out.println(bbdd.getAbsolutePath().toString());
					deleteFolder(bbdd);
					ConexionBBDD.main(null);
					*/
					ConexionBBDD.borrarTablas();
					ConexionBBDD.insertarTablas();
				}else {
					System.out.println("HA SELECIIONADO NO");
				}
			}
		};
	}
	
	private static void deleteFolder(File fileDel) {
        if(fileDel.isDirectory()){   
        	//COMPROBAMOS SI TIENE MAS DIRECTORIOS PARA BORRARLOS
            if(fileDel.list().length == 0)
                fileDel.delete();
            else{
            	//BORRAMOS LOS DIRECTORIO DENTRO DE LA CARPETA
               for (String temp : fileDel.list()) {
                   File fileDelete = new File(fileDel, temp);
                   deleteFolder(fileDelete);
               }
               if(fileDel.list().length==0)
                   fileDel.delete();
            }
        }else{
            fileDel.delete();           
        }
    }
	public static MouseListener mostrarPartidoDeGrupo() {
		return new MouseAdapter() {
			@SuppressWarnings("unchecked")
			public void mouseClicked(MouseEvent arg0) {
				//GUARDAMOS INFORMACION DE LA VISTA
					String idPartido=InfoGrupo.getTableMetro().getValueAt(InfoGrupo.getTableMetro().getSelectedRow(), 0).toString(),
							nombreEquipoLocal=InfoGrupo.getTableMetro().getValueAt(InfoGrupo.getTableMetro().getSelectedRow(), 1).toString(),
							nombreEquipoVisitante=InfoGrupo.getTableMetro().getValueAt(InfoGrupo.getTableMetro().getSelectedRow(), 2).toString(),
							golesLocal=InfoGrupo.getTableMetro().getValueAt(InfoGrupo.getTableMetro().getSelectedRow(), 3).toString(),
							golesVisitante=InfoGrupo.getTableMetro().getValueAt(InfoGrupo.getTableMetro().getSelectedRow(), 4).toString(), 
							fecha=InfoGrupo.getTableMetro().getValueAt(InfoGrupo.getTableMetro().getSelectedRow(), 5).toString();	
					Date fechaPARTIDO=ParseFecha(fecha);
					
					//SACAMOS LOS PUNTOS DE LOS EQUIPOS
					puntosLocal=Modelo.obtenerPuntosDeEquipo(nombreEquipoLocal);
					puntosVisitante=Modelo.obtenerPuntosDeEquipo(nombreEquipoVisitante);
					
					InfoGrupo.getEquipoLocal().setText(nombreEquipoLocal);
					InfoGrupo.getEquipoVisitante().setText(nombreEquipoVisitante);
					InfoGrupo.getGolesLocal().setText(golesLocal);
					InfoGrupo.getGolesVisitante().setText(golesVisitante);
					InfoGrupo.getFechaPartido().setDatoFecha(fechaPARTIDO);
			}
		};
	}
	public static ActionListener guardarInfoPartidoGrupo(String grupo) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int golesLocal=Integer.parseInt(InfoGrupo.getGolesLocal().getText().toString()), 
						golesVisitante=Integer.parseInt(InfoGrupo.getGolesVisitante().getText().toString());
				String idPartido=InfoGrupo.getTableMetro().getValueAt(InfoGrupo.getTableMetro().getSelectedRow(), 0).toString(),
						nombreEquipoLocal=InfoGrupo.getTableMetro().getValueAt(InfoGrupo.getTableMetro().getSelectedRow(), 1).toString(),
						nombreEquipoVisitante=InfoGrupo.getTableMetro().getValueAt(InfoGrupo.getTableMetro().getSelectedRow(), 2).toString();
				int idEquipoLocal=Modelo.obtenerIdEquipo(nombreEquipoLocal), idEquipoVisitante=Modelo.obtenerIdEquipo(nombreEquipoVisitante);
				String fechaPartido = null, observaciones="";
				Date fechaCalendario=InfoGrupo.getFechaPartido().getDatoFecha(), fechaHoy=new Date();
				SimpleDateFormat sdf= new SimpleDateFormat("MM/dd/yyyy");
				fechaPartido=sdf.format(fechaCalendario);
				//GUARDAMOS LA INFORMACION EN LA TABLA DE PARTIDOS
				Modelo.actualizarPartidoFutbol(idPartido, idEquipoLocal, idEquipoVisitante, golesLocal, golesVisitante, fechaPartido, observaciones);
				//COMPROBAMOS LOS GOLES Y DAMOS LOS PUNTOS
				int comparacionFechas=fechaHoy.compareTo(fechaCalendario);
				if (comparacionFechas>=0) {
					if (golesLocal-golesVisitante>0) {
						//PUNTOS PARA EL LOCAL
						Modelo.actualizarPuntosEquipo(3+puntosLocal, idEquipoLocal);
						Modelo.actualizarVencedorPartido(idPartido, idEquipoLocal);
					}if (golesLocal-golesVisitante<0) {
						Modelo.actualizarPuntosEquipo(3+puntosVisitante, idEquipoVisitante);
						Modelo.actualizarVencedorPartido(idPartido, idEquipoVisitante);
					}else {
						Modelo.actualizarPuntosEquipo(1+puntosLocal, Modelo.obtenerIdEquipo(nombreEquipoLocal));
						Modelo.actualizarPuntosEquipo(1+puntosVisitante, Modelo.obtenerIdEquipo(nombreEquipoVisitante));
						Modelo.actualizarVencedorPartido(idPartido, 99999);
					}
				}
				actualizarFrameInfoGrupo(grupo);
			}			
		};
	}

	public static ActionListener pasarDeRonda() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (InfoGrupo.getListaEquipos().getSelectedIndex()!=-1) {
					if (Modelo.listarPartidosSinJugar(1)) {
						JOptionPane.showMessageDialog(null, "Hay partidos aun por jugarse, rev�salos y cuando finalicen todos \n podr� pasar de ronda los equipos que desee", "Atenci�n", 0);
					}else {
						//PASAMOS DE RONDA A LOS 2 PRIMEROS DE EQUIPO
						String nombreEquipo=InfoGrupo.getListaEquipos().getSelectedValue().toString();
						int idEquipo=Modelo.obtenerIdEquipo(nombreEquipo);
						Modelo.pasarDeRondaEquipo(idEquipo, 2);
						new rojerusan.RSNotifyFade("Equipos", "Ha pasado de ronda",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);		
						actualizarCuartosFutbol();
						
					}
				}else {
					JOptionPane.showMessageDialog(null, "Seleccione primero un equipo", "Error", 0);
				}
			}
		};
	}
	@SuppressWarnings("unchecked")
	public static void actualizarCuartosFutbol() {
		ResultSet rs=Modelo.listarEquiposPorFase(2);
		
		Principal.getP1Local().removeAllItems();
		Principal.getP1Visitante().removeAllItems();
		Principal.getP2Local().removeAllItems();
		Principal.getP2Visitante().removeAllItems();
		Principal.getP3Local().removeAllItems();
		Principal.getP3Visitante().removeAllItems();
		Principal.getP4Local().removeAllItems();
		Principal.getP4Visitante().removeAllItems();
		
		Principal.getP1Local().addItem("Seleccione equipo");
		Principal.getP1Visitante().addItem("Seleccione equipo");
		Principal.getP2Local().addItem("Seleccione equipo");
		Principal.getP2Visitante().addItem("Seleccione equipo");
		Principal.getP3Local().addItem("Seleccione equipo");
		Principal.getP3Visitante().addItem("Seleccione equipo");
		Principal.getP4Local().addItem("Seleccione equipo");
		Principal.getP4Visitante().addItem("Seleccione equipo");
		
		String nombreEquipo=null;
		try {
			while (rs.next()) {
				nombreEquipo=rs.getString(2);
				Principal.getP1Local().addItem(nombreEquipo);
				Principal.getP1Visitante().addItem(nombreEquipo);
				Principal.getP2Local().addItem(nombreEquipo);
				Principal.getP2Visitante().addItem(nombreEquipo);
				Principal.getP3Local().addItem(nombreEquipo);
				Principal.getP3Visitante().addItem(nombreEquipo);
				Principal.getP4Local().addItem(nombreEquipo);
				Principal.getP4Visitante().addItem(nombreEquipo);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mostrarCuartos();
		
	}
	private static void mostrarCuartos() {
		ResultSet rs=Modelo.partidosPorFase(2);
		ArrayList<Object[]>listaPartidas=new ArrayList<>();
		try {
			while (rs.next()) {
				Object[] partidos= {rs.getString(1), rs.getString(2), rs.getString(3)};
				listaPartidas.add(partidos);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (!listaPartidas.isEmpty()) {
			Principal.getP1Local().setSelectedItem(listaPartidas.get(0)[2]);
			Principal.getP1Visitante().setSelectedItem(listaPartidas.get(0)[3]);
			Principal.getP2Local().setSelectedItem(listaPartidas.get(1)[2]);
			Principal.getP2Visitante().setSelectedItem(listaPartidas.get(1)[3]);
			Principal.getP3Local().setSelectedItem(listaPartidas.get(2)[2]);
			Principal.getP3Visitante().setSelectedItem(listaPartidas.get(2)[3]);
			Principal.getP4Local().setSelectedItem(listaPartidas.get(3)[2]);
			Principal.getP4Visitante().setSelectedItem(listaPartidas.get(3)[3]);
		}
	}
	public static ActionListener generarPartidosGrupo() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (consultarSiTieneGrupo()) {
					int numeroDeEquiposA=Principal.getListA().getModel().getSize(),
							numeroDeEquiposB=Principal.getListB().getModel().getSize(),
							numeroDeEquiposC=Principal.getListC().getModel().getSize(),
							numeroDeEquiposD=Principal.getListD().getModel().getSize(),
							numeroDeEquiposE=Principal.getListE().getModel().getSize(),
							numeroDeEquiposF=Principal.getListF().getModel().getSize();
					String nombreLocal, nombreVisitante;
					if (numeroDeEquiposA>0) {
						//USAMOS EL ALGORITMO PARA SACAR EL NUMERO DE PARTIDO QUE HABR�
						int n=obtenerNPartidos(numeroDeEquiposA), x=numeroDeEquiposA-1, y=0, m=numeroDeEquiposA-1;
						//CON ESTOS FOR REALIZAMOS TODOS LOS ENCUENTROS DE LA LIGUILLA
						for (int i = 0; i < x; i++) {
							y=i;
							for (int j = 0; j < m ; j++) {
								nombreLocal=Principal.getListA().getModel().getElementAt(i);
								nombreVisitante=Principal.getListA().getModel().getElementAt(y+1);
								int local=Modelo.obtenerIdEquipo(nombreLocal), 
										visitante=Modelo.obtenerIdEquipo(nombreVisitante);
								Modelo.crearPartidoDeFutbol(local, visitante);
								if (numeroDeEquiposA==3) {
									Modelo.crearPartidoDeFutbol(visitante, local);
								}
								y++;
							}			
							m--;
						}
					}if (numeroDeEquiposB>0) {
						//USAMOS EL ALGORITMO PARA SACAR EL NUMERO DE PARTIDO QUE HABR�
						int n=obtenerNPartidos(numeroDeEquiposB), x=numeroDeEquiposB-1, y=0, m=numeroDeEquiposB-1;
						//CON ESTOS FOR REALIZAMOS TODOS LOS ENCUENTROS DE LA LIGUILLA
						for (int i = 0; i < x; i++) {
							y=i;
							for (int j = 0; j < m ; j++) {
								nombreLocal=Principal.getListB().getModel().getElementAt(i);
								nombreVisitante=Principal.getListB().getModel().getElementAt(y+1);
								int local=Modelo.obtenerIdEquipo(nombreLocal), 
										visitante=Modelo.obtenerIdEquipo(nombreVisitante);
								Modelo.crearPartidoDeFutbol(local, visitante);
								if (numeroDeEquiposB==3) {
									Modelo.crearPartidoDeFutbol(visitante, local);
								}
								y++;
							}			
							m--;
						}
					}if (numeroDeEquiposC>0) {
						//USAMOS EL ALGORITMO PARA SACAR EL NUMERO DE PARTIDO QUE HABR�
						int n=obtenerNPartidos(numeroDeEquiposC), x=numeroDeEquiposC-1, y=0, m=numeroDeEquiposC-1;
						//CON ESTOS FOR REALIZAMOS TODOS LOS ENCUENTROS DE LA LIGUILLA
						for (int i = 0; i < x; i++) {
							y=i;
							for (int j = 0; j < m ; j++) {
								nombreLocal=Principal.getListC().getModel().getElementAt(i);
								nombreVisitante=Principal.getListC().getModel().getElementAt(y+1);
								int local=Modelo.obtenerIdEquipo(nombreLocal), 
										visitante=Modelo.obtenerIdEquipo(nombreVisitante);
								Modelo.crearPartidoDeFutbol(local, visitante);
								if (numeroDeEquiposC==3) {
									Modelo.crearPartidoDeFutbol(visitante, local);
								}
								y++;
							}			
							m--;
						}
					}if (numeroDeEquiposD>0) {
						//USAMOS EL ALGORITMO PARA SACAR EL NUMERO DE PARTIDO QUE HABR�
						int n=obtenerNPartidos(numeroDeEquiposD), x=numeroDeEquiposD-1, y=0, m=numeroDeEquiposD-1;
						//CON ESTOS FOR REALIZAMOS TODOS LOS ENCUENTROS DE LA LIGUILLA
						for (int i = 0; i < x; i++) {
							y=i;
							for (int j = 0; j < m ; j++) {
								nombreLocal=Principal.getListD().getModel().getElementAt(i);
								nombreVisitante=Principal.getListD().getModel().getElementAt(y+1);
								int local=Modelo.obtenerIdEquipo(nombreLocal), 
										visitante=Modelo.obtenerIdEquipo(nombreVisitante);
								Modelo.crearPartidoDeFutbol(local, visitante);
								if (numeroDeEquiposD==3) {
									Modelo.crearPartidoDeFutbol(visitante, local);
								}
								y++;
							}			
							m--;
						}
					}if (numeroDeEquiposE>0) {
						//USAMOS EL ALGORITMO PARA SACAR EL NUMERO DE PARTIDO QUE HABR�
						int n=obtenerNPartidos(numeroDeEquiposE), x=numeroDeEquiposE-1, y=0, m=numeroDeEquiposE-1;
						//CON ESTOS FOR REALIZAMOS TODOS LOS ENCUENTROS DE LA LIGUILLA
						for (int i = 0; i < x; i++) {
							y=i;
							for (int j = 0; j < m ; j++) {
								nombreLocal=Principal.getListE().getModel().getElementAt(i);
								nombreVisitante=Principal.getListE().getModel().getElementAt(y+1);
								int local=Modelo.obtenerIdEquipo(nombreLocal), 
										visitante=Modelo.obtenerIdEquipo(nombreVisitante);
								Modelo.crearPartidoDeFutbol(local, visitante);
								if (numeroDeEquiposE==3) {
									Modelo.crearPartidoDeFutbol(visitante, local);
								}
								y++;
							}			
							m--;
						}
					}if (numeroDeEquiposF>0) {
						//USAMOS EL ALGORITMO PARA SACAR EL NUMERO DE PARTIDO QUE HABR�
						int n=obtenerNPartidos(numeroDeEquiposF), x=numeroDeEquiposF-1, y=0, m=numeroDeEquiposF-1;
						//CON ESTOS FOR REALIZAMOS TODOS LOS ENCUENTROS DE LA LIGUILLA
						for (int i = 0; i < x; i++) {
							y=i;
							for (int j = 0; j < m ; j++) {
								nombreLocal=Principal.getListF().getModel().getElementAt(i);
								nombreVisitante=Principal.getListF().getModel().getElementAt(y+1);
								int local=Modelo.obtenerIdEquipo(nombreLocal), 
										visitante=Modelo.obtenerIdEquipo(nombreVisitante);
								Modelo.crearPartidoDeFutbol(local, visitante);
								if (numeroDeEquiposF==3) {
									Modelo.crearPartidoDeFutbol(visitante, local);
								}
								y++;
							}			
							m--;
						}
					}
					new rojerusan.RSNotifyFade("Partidos", "Se han generado todos los partidos",Primario,PrimarioDark,Complemento, 7,rojerusan.RSNotifyFade.PositionNotify.BottomRight, rojerusan.RSNotifyFade.TypeNotify.INFORMATION).setVisible(true);		
				}else {
					JOptionPane.showMessageDialog(null, "Hay equipos sin grupo, asigne primero un grupo, recuerda, un grupo debe de contener almenos 3 equipos", "ERROR", 0);
					System.out.println("Hay equipos sin grupo, asigne primero un grupo, recuerda, un grupo debe de contener almenos 3 equipos");
				}
			}

			private int obtenerNPartidos(int i) {
				int n=(i*(i-1))/2;
				return n;
			}
		};
	}
	public static KeyListener buscadorJugadores() {
		return new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				String buscar=Jugadores.getBuscadorJugadores().getText().toString();
				System.out.println(buscar);
				if (buscar.isEmpty()) {
					switch (opcion) {
					case 1:
						actualizarJugadorFutbol();
						break;
					case 2:
						actualizarJugadoresAjedrez();
						break;
					case 3:
						actualizarJugadoresTenis();
						break;
					default:
						actualizarFrameJugadores(10);
						break;
					}
				}else {
					actualizarListaJugadores(buscar, opcion);
				}
			}
		};
	}
	@SuppressWarnings("unchecked")
	protected static void actualizarListaJugadores(String buscar, int i) {
		DefaultListModel<String> listaNombreBusqueda=new DefaultListModel<>();
		ResultSet rs=Modelo.listaJugadoresBusqueda(buscar, i);
		try {
			while (rs.next()) {
				String jugadores=rs.getString("id_jugador")+
						"-"+rs.getString("nombre")+
						"-"+rs.getString("curso")+
						" - "+rs.getString("competicion");
				listaNombreBusqueda.addElement(jugadores);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Jugadores.getListJugadores().setModel(listaNombreBusqueda);
	}
	
	public static ActionListener iniciarSesion(LoginSplash loginSplash) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String contrasenna=LoginSplash.getContrasenna().getText().toString(), 
						usuario=LoginSplash.getUsuario().getText().toString();
				if (usuario.equals("admin")) {
					if (comprobarContrasenna(contrasenna)) {
						Principal frame=new Principal();
						frame.setVisible(true);
						frame.setLocationRelativeTo(null);
						RSAnimation.setBajar(-960, 100, 1, 5, frame);
						loginSplash.dispose();
					}else {
						JOptionPane.showMessageDialog(null, "Contrase�a incorrecta", "Contrase�a no v�lida", 0);
						System.out.println("Datos incorrectos para iniciar sesion");
					}
				}else {
					JOptionPane.showMessageDialog(null, "Usuario incorrecto", "Error de inicio de sesi�n", 0);
				}		
			}
		};
	}
	public static void capturarPantalla(/*String Nombre*/) throws
    AWTException, IOException {
		BufferedImage captura = new Robot().createScreenCapture(
		new Rectangle(new Dimension(123, 131)) );

		// Guardar Como JPEG
		File file = new File("MI CAMPTURA JEJEJ" + ".jpg");
		ImageIO.write(captura, "jpg", file);
	}
	public static MouseListener mostrarPuntos() {		
		return new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				if (InfoGrupo.getListaEquipos().getSelectedIndex()!=-1) {
					String nombre_equipo=InfoGrupo.getListaEquipos().getSelectedValue().toString();
					int puntos=Modelo.obtenerPuntosDeEquipo(nombre_equipo);
					InfoGrupo.getPuntosEquipo().setText(puntos+"");
				}else {
					JOptionPane.showMessageDialog(null, "Lista vac�a", "Error", 0);
				}
			}
		};
	}
	public static ActionListener crearPartidosFase2() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//GUARDAMOS LA INFORMACION EN VARIABLES
				String nombreEquipoLocal1=Principal.getP1Local().getSelectedItem().toString(), nombreEquipoVisitante1=Principal.getP1Visitante().getSelectedItem().toString(), 
				nombreEquipoLocal2=Principal.getP2Local().getSelectedItem().toString(), nombreEquipoVisitante2=Principal.getP2Visitante().getSelectedItem().toString(), 
				nombreEquipoLocal3=Principal.getP3Local().getSelectedItem().toString(), nombreEquipoVisitante3=Principal.getP3Visitante().getSelectedItem().toString(), 
				nombreEquipoLocal4=Principal.getP4Local().getSelectedItem().toString(), nombreEquipoVisitante4=Principal.getP4Visitante().getSelectedItem().toString();
				int idEquipoLocal1=Modelo.obtenerIdEquipo(nombreEquipoLocal1), idEquipoVisitante1=Modelo.obtenerIdEquipo(nombreEquipoVisitante1), 
						idEquipoLocal2=Modelo.obtenerIdEquipo(nombreEquipoLocal2), idEquipoVisitante2=Modelo.obtenerIdEquipo(nombreEquipoVisitante2), 
						idEquipoLocal3=Modelo.obtenerIdEquipo(nombreEquipoLocal3), idEquipoVisitante3=Modelo.obtenerIdEquipo(nombreEquipoVisitante3), 
						idEquipoLocal4=Modelo.obtenerIdEquipo(nombreEquipoLocal4), idEquipoVisitante4=Modelo.obtenerIdEquipo(nombreEquipoVisitante4);
				
				//CREAMOS UN PARTIDO
				Modelo.crearPartidoDeFutbol(idEquipoLocal1, idEquipoVisitante1);
				Modelo.crearPartidoDeFutbol(idEquipoLocal2, idEquipoVisitante2);
				Modelo.crearPartidoDeFutbol(idEquipoLocal3, idEquipoVisitante3);
				Modelo.crearPartidoDeFutbol(idEquipoLocal4, idEquipoVisitante4);
				
			}
		};
	}
}

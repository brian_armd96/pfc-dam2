package Vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import Controlador.Controlador;
import Modelo.AjustesApp;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import rojerusan.RSPasswordTextPlaceHolder;
import rojerusan.RSMetroTextPlaceHolder;
import rojeru_san.RSMTextFull;
import rojeru_san.RSPanelShadow;
import rojerusan.RSButtonPane;
import rojerusan.RSMaterialButtonRectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class LoginSplash extends JFrame {
	
	int x, y;
	private JPanel contentPane;
	Color PrimarioLight=new Color(100, 145, 95);
	Color Primario=new Color(55, 99, 53);
	Color PrimarioDark=new Color(9, 56, 14);
	Color Complemento=new Color(255, 255, 255);
	Color ComplementoOscuro=new Color(255, 255, 255);
	private static RSMTextFull usuario;
	private static RSPasswordTextPlaceHolder contrasenna;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginSplash frame = new LoginSplash();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginSplash() {
		//COMPROBAMOS LA CREACION DE LA BASE DE DATOS
		AjustesApp.iniciarTodo();
		
		ImageIcon splash=new ImageIcon("resources/Logos/splash.png");
		setUndecorated(true);
		setBackground(new Color(0, 0, 0, 0));
		setIconImage(Toolkit.getDefaultToolkit().getImage("resources/iconsApp/footballApp.png"));
		setTitle("Gestor torneos");
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 701, 498);
		contentPane = new JPanel();
		contentPane.setOpaque(false);
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("");
		label.setBounds(0, 0, 666, 406);
		Icon splashIcon=new ImageIcon(splash.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(splashIcon);
		contentPane.add(label);
		
		RSPanelShadow panelShadow = new RSPanelShadow();
		panelShadow.setBounds(277, 248, 422, 248);
		contentPane.add(panelShadow);
		panelShadow.setLayout(null);
		
		usuario = new RSMTextFull();
		usuario.setForeground(Primario);
		usuario.setBounds(92, 65, 250, 42);
		panelShadow.add(usuario);
		usuario.setFont(new Font("Dialog", Font.BOLD, 18));
		usuario.setPlaceholder("Usuario");
		
		contrasenna = new RSPasswordTextPlaceHolder();
		contrasenna.setBounds(92, 105, 250, 40);
		panelShadow.add(contrasenna);
		contrasenna.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		RSMaterialButtonRectangle entrarbtn = new RSMaterialButtonRectangle();
		entrarbtn.addActionListener(Controlador.iniciarSesion(this));
		entrarbtn.setBackground(PrimarioLight);
		entrarbtn.setFont(new Font("Dialog", Font.PLAIN, 18));
		entrarbtn.setText("Entrar");
		entrarbtn.setBounds(90, 145, 250, 60);
		panelShadow.add(entrarbtn);
		//Movimiento del frame a partir de un JLabel
		JLabel frameFake = new JLabel("");
		frameFake.setOpaque(true);
		frameFake.setBounds(0, 0, 425, 252);
		panelShadow.add(frameFake);
		frameFake.setForeground(Color.WHITE);
		frameFake.setBackground(Primario);
		frameFake.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				setLocation(getLocation().x + arg0.getX() - x, getLocation().y + arg0.getY() - y );
			}
		});
		frameFake.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				x=arg0.getX();
				y=arg0.getY();
			}
		});
		
		frameFake.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		//Boton X salir
		JButton btnX = new JButton("X");
		btnX.setBounds(650, 199, 50, 50);
		contentPane.add(btnX);
		btnX.setBackground(PrimarioDark);
		btnX.setBorderPainted(false);
		btnX.setBorder(null);
		btnX.setForeground(Complemento);
		btnX.setFont(new Font("Tahoma", Font.BOLD, 50));
		btnX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();			
			}
		});
		
		
	}

	public static RSPasswordTextPlaceHolder getContrasenna() {
		return contrasenna;
	}

	public static RSMTextFull getUsuario() {
		return usuario;
	}
	
}

package Vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import Controlador.Controlador;

import java.awt.ComponentOrientation;
import rojerusan.RSComboMetro;
import rojerusan.RSTableMetro;
import javax.swing.JList;
import rojerusan.RSMetroTextPlaceHolder;
import rojerusan.RSMaterialButtonRectangle;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import rscomponentshade.RSTextFieldShade;
import javax.swing.SwingConstants;
import rojeru_san.RSMTextFull;
import rojeru_san.componentes.RSDateChooser;

public class InfoGrupo extends JFrame {

	int x, y;
	private JPanel contentPane;
	/*
private static Color PrimarioLight=Principal.getPrimarioLight();
	private static Color Primario=Principal.getPrimario();
	private static Color PrimarioDark=Principal.getPrimarioDark();
	private static Color Complemento=Principal.getComplemento();
	private static Color ComplementoOscuro=Principal.getComplementoOscuro();
	
*/
	Color PrimarioLight=new Color(100, 145, 95);
	Color Primario=new Color(55, 99, 53);
	Color PrimarioDark=new Color(9, 56, 14);
	Color Complemento=new Color(255, 255, 255);
	Color ComplementoOscuro=new Color(255, 255, 255);
	
	private JLabel lblGrupo;
	String tituloGrupo;
	String nombreGrupo;
	private static JList listaEquipos;
	private static RSTableMetro tableMetro;
	private static RSDateChooser fechaPartido;
	private static RSMTextFull equipoLocal;
	private static RSMTextFull equipoVisitante;
	private static RSTextFieldShade golesLocal;
	private static RSTextFieldShade golesVisitante;
	private static RSTextFieldShade puntosEquipo;
	
	
	public InfoGrupo() {}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InfoGrupo frame = new InfoGrupo();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InfoGrupo(String titulo) {
		String tituloFrame="Información del grupo "+titulo;
		setIconImage(Toolkit.getDefaultToolkit().getImage("resources/iconsApp/footballApp.png"));
		setTitle(tituloFrame);
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 900, 600);
		contentPane = new JPanel();
		contentPane.setBackground(PrimarioDark);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//Boton X salir
		JButton btnX = new JButton("X");
		btnX.setBackground(Primario);
		btnX.setBorderPainted(false);
		btnX.setBorder(null);
		btnX.setForeground(PrimarioDark);
		btnX.setFont(new Font("Tahoma", Font.BOLD, 50));
		btnX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();			}
		});
		btnX.setBounds(850, 0, 50, 50);
		contentPane.add(btnX);
		
		lblGrupo = new JLabel("");
		lblGrupo.setOpaque(true);
		lblGrupo.setBackground(PrimarioLight);
		lblGrupo.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		lblGrupo.setForeground(Primario);
		lblGrupo.setFont(new Font("Corbel", Font.BOLD, 40));
		lblGrupo.setBounds(15, 16, 271, 50);
		
		lblGrupo.setText("GRUPO: "+titulo);
		contentPane.add(lblGrupo);
		
		listaEquipos = new JList();
		listaEquipos.setBackground(Primario);
		listaEquipos.addMouseListener(Controlador.mostrarPuntos());
		listaEquipos.setBounds(15, 82, 271, 271);
		contentPane.add(listaEquipos);
		//Movimiento del frame a partir de un JLabel
		JLabel frameFake = new JLabel("");
		frameFake.setForeground(Color.WHITE);
		frameFake.setBackground(Color.WHITE);
		frameFake.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				setLocation(getLocation().x + arg0.getX() - x, getLocation().y + arg0.getY() - y );
			}
		});
		frameFake.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				x=arg0.getX();
				y=arg0.getY();
			}
		});
		
		frameFake.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		frameFake.setBounds(0, 0, 800, 50);
		contentPane.add(frameFake);
		
		RSMaterialButtonRectangle btnGuardar = new RSMaterialButtonRectangle();
		btnGuardar.addActionListener(Controlador.guardarInfoPartidoGrupo(titulo));
		btnGuardar.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnGuardar.setText("Guardar");
		btnGuardar.setBounds(671, 177, 200, 60);
		btnGuardar.setBackground(PrimarioLight);
		contentPane.add(btnGuardar);
		
		puntosEquipo = new RSTextFieldShade();
		puntosEquipo.setForeground(new Color(255, 255, 255));
		puntosEquipo.setBackground(Primario);
		puntosEquipo.setFont(new Font("Tahoma", Font.BOLD, 18));
		puntosEquipo.setHorizontalAlignment(SwingConstants.CENTER);
		puntosEquipo.setText("0");
		puntosEquipo.setBounds(301, 82, 50, 50);
		contentPane.add(puntosEquipo);
		
		JLabel lblPuntos = new JLabel("Puntos");
		lblPuntos.setForeground(Color.WHITE);
		lblPuntos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPuntos.setBounds(354, 105, 69, 20);
		contentPane.add(lblPuntos);
		
		equipoLocal = new RSMTextFull();
		equipoLocal.setColorTransparente(true);
		equipoLocal.setModoMaterial(true);
		equipoLocal.setBordeColorFocus(Color.WHITE);
		equipoLocal.setEditable(false);
		equipoLocal.setForeground(Color.WHITE);
		equipoLocal.setOpaque(false);
		equipoLocal.setSoloLetras(true);
		equipoLocal.setBotonColor(Primario);
		equipoLocal.setPlaceholder("Equipo local");
		equipoLocal.setFont(new Font("Dialog", Font.BOLD, 18));
		equipoLocal.setBounds(301, 228, 250, 42);
		contentPane.add(equipoLocal);
		
		golesLocal = new RSTextFieldShade();
		golesLocal.setForeground(new Color(255, 255, 255));
		golesLocal.setBackground(Primario);
		golesLocal.setFont(new Font("Tahoma", Font.BOLD, 18));
		golesLocal.setText("0");
		golesLocal.setHorizontalAlignment(SwingConstants.CENTER);
		golesLocal.setBounds(566, 220, 50, 50);
		contentPane.add(golesLocal);
		
		equipoVisitante = new RSMTextFull();
		equipoVisitante.setColorTransparente(true);
		equipoVisitante.setModoMaterial(true);
		equipoVisitante.setBordeColorFocus(Color.WHITE);
		equipoVisitante.setEditable(false);
		equipoVisitante.setForeground(Color.WHITE);
		equipoVisitante.setOpaque(false);
		equipoVisitante.setSoloLetras(true);
		equipoVisitante.setBotonColor(Primario);
		equipoVisitante.setPlaceholder("Equipo visitante");
		equipoVisitante.setFont(new Font("Dialog", Font.BOLD, 18));
		equipoVisitante.setBounds(301, 299, 250, 42);
		contentPane.add(equipoVisitante);
		
		golesVisitante = new RSTextFieldShade();
		golesVisitante.setForeground(new Color(255, 255, 255));
		golesVisitante.setBackground(Primario);
		golesVisitante.setFont(new Font("Tahoma", Font.BOLD, 18));
		golesVisitante.setText("0");
		golesVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		golesVisitante.setBounds(566, 291, 50, 50);
		contentPane.add(golesVisitante);
		
		fechaPartido = new RSDateChooser();
		fechaPartido.setFormatoFecha("dd/MM/yyyy");
		fechaPartido.setColorForeground(Primario);
		fechaPartido.setColorBackground(Primario);
		
		fechaPartido.setFuente(new Font("Tahoma", Font.BOLD, 18));
		fechaPartido.setBounds(631, 266, 240, 40);
		contentPane.add(fechaPartido);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 369, 870, 215);
		contentPane.add(scrollPane);
		
		tableMetro = new RSTableMetro();
		tableMetro.addMouseListener(Controlador.mostrarPartidoDeGrupo());
		tableMetro.setAltoHead(30);
		tableMetro.setColorFilasForeground1(Color.DARK_GRAY);
		tableMetro.setColorFilasBackgound2(Color.WHITE);
		tableMetro.setRowMargin(2);
		tableMetro.setRowHeight(25);
		tableMetro.setEditingRow(0);
		tableMetro.setEditingColumn(0);
		tableMetro.setFont(new Font("Tahoma", Font.PLAIN, 30));
		tableMetro.setColorSelForeground(Color.WHITE);
		tableMetro.setColorSelBackgound(Color.DARK_GRAY);
		tableMetro.setColorFilasForeground2(Color.DARK_GRAY);
		tableMetro.setColorFilasBackgound1(Color.WHITE);
		tableMetro.setColorBackgoundHead(Primario);
		tableMetro.setSelectionForeground(new Color(0, 0, 0));
		tableMetro.setSelectionBackground(new Color(0, 255, 0));
		tableMetro.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableMetro.setMultipleSeleccion(false);
		tableMetro.setFuenteFilas(new Font("Tahoma", Font.PLAIN, 24));
		tableMetro.setFuenteFilasSelect(new Font("Tahoma", Font.PLAIN, 24));
		tableMetro.setFuenteHead(new Font("Tahoma", Font.PLAIN, 16));
		scrollPane.setViewportView(tableMetro);
		
		RSMaterialButtonRectangle siguiente = new RSMaterialButtonRectangle();
		siguiente.addActionListener(Controlador.pasarDeRonda());
		siguiente.setText("Siguiente fase >");
		siguiente.setFont(new Font("Dialog", Font.PLAIN, 18));
		siguiente.setBackground(new Color(100, 145, 95));
		siguiente.setBounds(301, 141, 200, 42);
		contentPane.add(siguiente);
		
		Controlador.actualizarFrameInfoGrupo(titulo);
	}

	public static JList getListaEquipos() {
		return listaEquipos;
	}

	public static RSTableMetro getTableMetro() {
		return tableMetro;
	}

	public static RSDateChooser getFechaPartido() {
		return fechaPartido;
	}

	public static RSMTextFull getEquipoLocal() {
		return equipoLocal;
	}

	public static RSMTextFull getEquipoVisitante() {
		return equipoVisitante;
	}

	public static RSTextFieldShade getGolesLocal() {
		return golesLocal;
	}

	public static RSTextFieldShade getGolesVisitante() {
		return golesVisitante;
	}

	public static RSTextFieldShade getPuntosEquipo() {
		return puntosEquipo;
	}
}

package Vista;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.AWTException;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import rojerusan.RSPanelsSlider;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import rscomponentshade.RSButtonShade;
import rscomponentshade.RSFormatFieldShade;
import rscomponentshade.RSToggleButtonShade;
import rscomponentshade.RSPassFieldShade;

import rojeru_san.RSPanelShadow;
import rojeru_san.RSLabelFecha;
import rojeru_san.RSLabelHora;
import rojerusan.RSTableMetro;
import javax.swing.border.LineBorder;
import javax.swing.table.TableColumnModel;

import Controlador.Controlador;
import Modelo.AjustesApp;
import rojerusan.RSButtonMetro;
import rojerusan.RSMaterialButtonCircle;
import rojerusan.RSButtonIconD;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import rscomponentshade.RSTextFieldShade;
import java.awt.Toolkit;

import rojeru_san.RSButtonRiple;
import rojerusan.RSMaterialButtonRectangle;
import rojeru_san.RSMTextFull;
import rojerusan.RSComboMetro;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;

import javax.swing.JEditorPane;

import rspanelgradiente.RSPanelGradiente;
import javax.swing.JList;
import rojeru_san.componentes.RSDateChooser;
import javax.swing.JTabbedPane;

import javax.swing.JProgressBar;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JPopupMenu;
import java.awt.Component;
import javax.swing.JMenuItem;
import javax.swing.AbstractAction;
import javax.swing.Action;
import java.awt.Dimension;

public class Principal extends JFrame {
	
	private int x, y;
	private JPanel contentPane;
	
	
	
	private static Color Complemento=Controlador.ponerColores().get(0);
	private static Color ComplementoDark=Controlador.ponerColores().get(1);
	private static Color Primario=Controlador.ponerColores().get(2);
	private static Color PrimarioLight=Controlador.ponerColores().get(3);
	private static Color PrimarioDark=Controlador.ponerColores().get(4);
	
/*
Color PrimarioLight=new Color(100, 145, 95);
	Color Primario=new Color(55, 99, 53);
	Color PrimarioDark=new Color(9, 56, 14);
	Color Complemento=new Color(255, 255, 255);
	Color ComplementoOscuro=new Color(255, 255, 255);
*/

	
	boolean abierto = false, 
			abiertoConfig = false;
	private static JPanel mainPanel, 
		futbolPanel, 
		tenisPanel, 
		ajedrezPanel;
	private static RSPanelsSlider panelSliderPrincipal;
	private static RSToggleButtonShade futbolSelect, ajedrezSelect, tenisSelect, selectLunes, selectMartes, selectMiercoles, selectJueves, selectViernes;
	private static RSTextFieldShade nombreAlumno, p1GolesLocal, p1GolesVisitante, p2GolesLocal, p2GolesVisitante, p3GolesLocal, p3GolesVisitante, p4GolesLocal, p4GolesVisitante;
	private static RSPanelShadow panelInformacionPrincipal;
	private static JList<String> listA, 
		listB, 
		listC, 
		listD, 
		listE, 
		listF;
	private static JEditorPane observaciones;
	private static RSComboMetro comboGrupoA,
		comboGrupoB,
		comboGrupoC,
		comboGrupoD,
		comboGrupoE,
		comboGrupoF,
		cursoAlumno,
		partida1Ajedrez,
		partida2Ajedrez,
		partida3Ajedrez, 
		p1Local,
		p2Local,
		p1Visitante,
		p2Visitante,
		p3Local,
		p3Visitante,
		p4Local,
		p4Visitante;
	private static RSTableMetro tablaParatidasTenis, 
		tablaAjedrez;
	private static JLabel faseXTenis, faseX, deY, lblNombreGrupo;
	private static RSButtonIconD lunes,
		martes,
		miercoles,
		jueves,
		viernes;
	private static RSPassFieldShade contrasenna, contrasennaNueva;
	private static RSFormatFieldShade usuario, paginaFav;
	private static JTextField equipo1, equipo2;
	private static RSDateChooser fechaPartida1, fechaPartida2, fechaPartida3,
		p2Fecha, p1Fecha, p3Fecha, p4Fecha,
		fechaTenis;
	private static JProgressBar barraAjedrez, barraFasesTenis;
	private static JRadioButton ganadorJ1, ganadorJ2;
	private static RSMTextFull jugadorTenisVisitante, jugadorTenisLocal;
	private final Action action = new SwingAction();
	private JLabel consola;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Principal frame = new Principal();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("unchecked")
	public Principal() {
		
		ImageIcon menu=new ImageIcon("resources/iconsApp/menu.png");
		ImageIcon menuB=new ImageIcon("resources/iconsApp/menu_b.png");
		ImageIcon home=new ImageIcon("resources/iconsApp/home.png");
		ImageIcon homeB=new ImageIcon("resources/iconsApp/home_press.png");
		ImageIcon calendario=new ImageIcon("resources/iconsBotones/calendar.png");
		ImageIcon iconoBanderaEquipo=new ImageIcon("resources/iconsApp/footballApp.png");
		ImageIcon iconoPanelAjedrez=new ImageIcon("resources/iconsApp/strategy.png");
		ImageIcon iconoTenis=new ImageIcon("resources/iconsApp/ping-pongApp.png");
		
		setIconImage(Toolkit.getDefaultToolkit().getImage("resources/iconsApp/cup.png"));
		setTitle("Gestor torneos");
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 920);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelMenu = new JPanel();
		
		panelMenu.setBackground(Primario);
		panelMenu.setBounds(-326, 115, 400, 785);
		contentPane.add(panelMenu);
		panelMenu.setLayout(null);
		
		RSButtonIconD btncndEfaMoratalaz = new RSButtonIconD();
		btncndEfaMoratalaz.addActionListener(AjustesApp.abrirPaginaEfa());
		btncndEfaMoratalaz.setColorTextHover(PrimarioLight);
		btncndEfaMoratalaz.setColorText(Primario);
		btncndEfaMoratalaz.setColorHover(Color.LIGHT_GRAY);
		btncndEfaMoratalaz.setBackground(Color.WHITE);
		btncndEfaMoratalaz.setIcon(new ImageIcon("resources/Logos/efa.png"));
		btncndEfaMoratalaz.setBounds(205, 210, 195, 70);
		panelMenu.add(btncndEfaMoratalaz);
		btncndEfaMoratalaz.setText("EFA Moratalaz");
		
		RSButtonIconD configuracionbtn = new RSButtonIconD();
		configuracionbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int posicion = panelMenu.getX();
		        if(!abiertoConfig){
		            Animacion.Animacion.mover_derecha(posicion, 0, 2, 4, panelMenu);
		            abierto=true;
		            abiertoConfig=true;
		        }else{
		            Animacion.Animacion.mover_izquierda(posicion, -326, 2, 4, panelMenu);
		            abiertoConfig=false;
		            abierto=false;
		        }
			}
		});
		configuracionbtn.setFont(new Font("Tahoma", Font.BOLD, 17));
		configuracionbtn.setBorder(null);
		configuracionbtn.setIcon(new ImageIcon("resources/iconsApp/gear_b.png"));
		configuracionbtn.setText("Configuración");
		configuracionbtn.setColorTextHover(Color.WHITE);
		configuracionbtn.setColorText(new Color(255, 255, 255));
		configuracionbtn.setColorHover(PrimarioLight);
		configuracionbtn.setBackground(Primario);
		configuracionbtn.setBounds(205, 70, 195, 70);
		panelMenu.add(configuracionbtn);
		
		RSButtonIconD favoritosbtn = new RSButtonIconD();
		favoritosbtn.setFont(new Font("Tahoma", Font.BOLD, 18));
		favoritosbtn.setBorder(null);
		favoritosbtn.addActionListener(AjustesApp.abrirPaginaFav());
		favoritosbtn.setIcon(new ImageIcon("resources/iconsApp/grid-world.png"));
		favoritosbtn.setText("Favoritos");
		favoritosbtn.setColorTextHover(Color.WHITE);
		favoritosbtn.setColorText(new Color(255, 255, 255));
		favoritosbtn.setColorHover(PrimarioLight);
		favoritosbtn.setBackground(Primario);
		favoritosbtn.setBounds(205, 140, 195, 70);
		panelMenu.add(favoritosbtn);
		
		RSButtonShade guardarbtn = new RSButtonShade();
		guardarbtn.setText("Guardar configuraci\u00F3n");
		guardarbtn.setFgText(new Color(55, 99, 53));
		guardarbtn.setFgHover(new Color(95, 132, 75));
		guardarbtn.setBgShadeHover(Color.WHITE);
		guardarbtn.setBgShade(Color.BLACK);
		guardarbtn.bgHover = Color.LIGHT_GRAY;
		guardarbtn.setBgHover(Color.LIGHT_GRAY);
		guardarbtn.setBackground(Color.WHITE);
		guardarbtn.addActionListener(Controlador.guardarConfiguracion());
		guardarbtn.setBounds(0, 191, 200, 40);
		panelMenu.add(guardarbtn);
		
		RSButtonIconD perfilbtn = new RSButtonIconD();
		perfilbtn.setHorizontalAlignment(SwingConstants.LEADING);
		perfilbtn.setIcon(new ImageIcon("resources/iconsApp/profile.png"));
		perfilbtn.setText("Perfil");
		perfilbtn.setFont(new Font("Tahoma", Font.BOLD, 17));
		perfilbtn.setColorTextHover(Color.WHITE);
		perfilbtn.setColorText(Color.WHITE);
		perfilbtn.setColorHover(PrimarioLight);
		perfilbtn.setBorder(null);
		perfilbtn.setBackground(Primario);
		perfilbtn.setBounds(205, 0, 195, 70);
		panelMenu.add(perfilbtn);
		
		usuario = new RSFormatFieldShade();
		usuario.setEditable(false);
		usuario.setPlaceholder("Usuario");
		usuario.setBounds(0, 0, 200, 45);
		panelMenu.add(usuario);
		
		contrasenna = new RSPassFieldShade();
		contrasenna.setPlaceholder("Contrase\u00F1a");
		contrasenna.setBounds(0, 45, 200, 45);
		panelMenu.add(contrasenna);
		
		JLabel lblTemaAplicacin = new JLabel("Tema de la aplicaci\u00F3n");
		lblTemaAplicacin.setHorizontalAlignment(SwingConstants.CENTER);
		lblTemaAplicacin.setForeground(Color.WHITE);
		lblTemaAplicacin.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblTemaAplicacin.setBounds(0, 276, 200, 32);
		panelMenu.add(lblTemaAplicacin);
		
		RSMaterialButtonCircle verde = new RSMaterialButtonCircle();
		verde.addActionListener(Controlador.cambiarColor(1));
		verde.setBackground(new Color(0, 128, 0));
		verde.setBounds(0, 321, 75, 75);
		panelMenu.add(verde);

		verde.setText("Verde");
		
		RSMaterialButtonCircle azul = new RSMaterialButtonCircle();
		azul.addActionListener(Controlador.cambiarColor(2));
		azul.setText("Azul");
		azul.setBackground(new Color(30, 144, 255));
		azul.setBounds(125, 321, 75, 75);
		panelMenu.add(azul);
		
		paginaFav = new RSFormatFieldShade();
		paginaFav.setPlaceholder("P\u00E1gina web favorita");
		paginaFav.setBounds(0, 140, 200, 45);
		panelMenu.add(paginaFav);
		
		JEditorPane Creditos = new JEditorPane();
		Creditos.setEditable(false);
		Creditos.setForeground(Color.WHITE);
		Creditos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		Creditos.setSelectionColor(Color.LIGHT_GRAY);
		Creditos.setOpaque(false);
		Creditos.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		Creditos.setText("Aplicaci\u00F3n desarrollado \r\ny dise\u00F1ado por \r\nBrian Terrazas como \r\nproyecto de fin de \r\nciclo FPGS Desarrollo \r\nde aplicaciones de \r\nmultiplataforma.\r\nDAM\r\nCopyright \u00A9\u00AE\u2122\r\nAll right reserved 2018.");
		Creditos.setBounds(10, 530, 195, 239);
		panelMenu.add(Creditos);
		
		contrasennaNueva = new RSPassFieldShade();
		contrasennaNueva.setPlaceholder("Nueva contrase\u00F1a");
		contrasennaNueva.setBounds(0, 92, 200, 45);
		panelMenu.add(contrasennaNueva);
		
		RSButtonShade btnReset = new RSButtonShade();
		btnReset.addActionListener(Controlador.resetBBDD());
		btnReset.setText("Borrar todo");
		btnReset.setFgText(new Color(55, 99, 53));
		btnReset.setFgHover(new Color(95, 132, 75));
		btnReset.setBgShadeHover(Color.WHITE);
		btnReset.setBgShade(Color.BLACK);
		btnReset.bgHover = Color.LIGHT_GRAY;
		btnReset.setBgHover(Color.LIGHT_GRAY);
		btnReset.setBackground(Color.WHITE);
		btnReset.setBounds(0, 236, 200, 40);
		panelMenu.add(btnReset);
		
		//Boton X salir
		JButton btnX = new JButton("X");
		btnX.setBackground(Primario);
		btnX.setBorderPainted(false);
		btnX.setBorder(null);
		btnX.setForeground(PrimarioDark);
		btnX.setFont(new Font("Tahoma", Font.BOLD, 50));
		btnX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		btnX.setBounds(1150, 0, 50, 50);
		contentPane.add(btnX);
		
		JButton menubtn = new JButton("");
		menubtn.setToolTipText("Abrir menu lateral");
		menubtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int position=panelMenu.getX();
				if(!abierto){
			        Animacion.Animacion.mover_derecha(position, -204, 2, 4, panelMenu);
		            abierto=true;
		            
		        }else{
		        	Animacion.Animacion.mover_izquierda(position, -326, 2, 4, panelMenu);
		        	abierto=false;
		        	abiertoConfig=false;
		        }
			}
		});
		menubtn.setContentAreaFilled(false);
		menubtn.setBorderPainted(false);
		menubtn.setBounds(10, 35, 50, 50);
		
		Icon imenu=new ImageIcon(menu.getImage().getScaledInstance(menubtn.getWidth(), menubtn.getHeight(), Image.SCALE_DEFAULT));
		Icon imenuB=new ImageIcon(menuB.getImage().getScaledInstance(menubtn.getWidth(), menubtn.getHeight(), Image.SCALE_DEFAULT));
		menubtn.setPressedIcon(imenuB);
		menubtn.setIcon(imenu);
		contentPane.add(menubtn);
		
		
		
		
		RSButtonMetro btnTenis = new RSButtonMetro();
		btnTenis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//PONER PANEL PINPONG
				panelSliderPrincipal.setPanelSlider(5, tenisPanel, RSPanelsSlider.DIRECT.LEFT);
			}
		});
		
		RSButtonIconD btnHome = new RSButtonIconD();
		btnHome.setIcon(new ImageIcon("resources/iconsApp/home_b.png"));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnHome.setBackground(Primario);
		btnHome.setColorHover(PrimarioDark);
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelSliderPrincipal.setPanelSlider(5, mainPanel, RSPanelsSlider.DIRECT.LEFT);
			}
		});
		btnHome.setText("PRINCIPAL");
		btnHome.setBounds(112, 35, 250, 68);
		contentPane.add(btnHome);
		
		RSButtonMetro btnFutbol = new RSButtonMetro();
		btnFutbol.setToolTipText("Torneo de f\u00FAtbol");
		btnFutbol.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelSliderPrincipal.setPanelSlider(5, futbolPanel, RSPanelsSlider.DIRECT.LEFT);
				Controlador.actualizarPanelFutbol();
			}
		});
		btnFutbol.setGrosorLinea(15);
		btnFutbol.setBackground(Primario);
		btnFutbol.setBounds(112, 119, 250, 250);
		contentPane.add(btnFutbol);
		
		
		btnFutbol.setIcon(new ImageIcon("resources/iconsApp/soccer-ball-variant_b.png"));
		btnTenis.setToolTipText("Torneo tenis de mesa");
		btnTenis.setIcon(new ImageIcon("resources/iconsApp/pin-pong_pala_b.png"));
		btnTenis.setGrosorLinea(15);
		btnTenis.setBackground(Primario);
		btnTenis.setBounds(112, 379, 250, 250);
		contentPane.add(btnTenis);
		
		RSButtonMetro btnAjedrez = new RSButtonMetro();
		btnAjedrez.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelSliderPrincipal.setPanelSlider(5, ajedrezPanel, RSPanelsSlider.DIRECT.LEFT);
			}
		});
		btnAjedrez.setToolTipText("Torneo ajedrez");
		btnAjedrez.setGrosorLinea(15);
		btnAjedrez.setIcon(new ImageIcon("resources/iconsApp/crown_b.png"));
		btnAjedrez.setBackground(Primario);
		btnAjedrez.setBounds(112, 638, 250, 250);
		contentPane.add(btnAjedrez);
		
		
		ImageIcon selectFut=new ImageIcon("resources/iconsBotones/soccer-player-motion.png");
		ImageIcon selectAje=new ImageIcon("resources/iconsBotones/strategy.png");
		ImageIcon selectTen=new ImageIcon("resources/iconsBotones/man-playing-ping-pong.png");
		DefaultListModel<String> modellist=new DefaultListModel<>();
		modellist.addElement("EJEMPLO");
		
		
		
		panelSliderPrincipal = new RSPanelsSlider();
		panelSliderPrincipal.setBounds(400, 119, 780, 769);
		contentPane.add(panelSliderPrincipal);
		
		mainPanel = new JPanel();
		mainPanel.setName("mainPanel");
		mainPanel.setBackground(PrimarioDark);
		panelSliderPrincipal.add(mainPanel, "name_393940445883532");
		mainPanel.setLayout(null);
		
		JLabel lblBienvenidoAlGestor = new JLabel("Bienvenido al gestor de torneos");
		lblBienvenidoAlGestor.setForeground(new Color(255, 255, 255));
		lblBienvenidoAlGestor.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblBienvenidoAlGestor.setBounds(15, 16, 740, 76);
		mainPanel.add(lblBienvenidoAlGestor);
		
		lunes = new RSButtonIconD();
		lunes.setIcon(calendario);
		lunes.setFont(new Font("Tahoma", Font.BOLD, 40));
		lunes.setText("00");
		lunes.setColorTextHover(Color.WHITE);
		lunes.setColorText(new Color(55, 99, 53));
		lunes.setColorHover(new Color(204, 204, 204));
		lunes.setBackground(Color.WHITE);
		lunes.setBounds(0, 681, 156, 88);
		mainPanel.add(lunes);
		
		martes = new RSButtonIconD();
		martes.setIcon(calendario);
		martes.setText("00");
		martes.setFont(new Font("Tahoma", Font.BOLD, 40));
		martes.setColorTextHover(Color.WHITE);
		martes.setColorText(new Color(55, 99, 53));
		martes.setColorHover(new Color(204, 204, 204));
		martes.setBackground(Color.WHITE);
		martes.setBounds(156, 681, 156, 88);
		mainPanel.add(martes);
		
		miercoles = new RSButtonIconD();
		miercoles.setIcon(calendario);
		miercoles.setText("00");
		miercoles.setFont(new Font("Tahoma", Font.BOLD, 40));
		miercoles.setColorTextHover(Color.WHITE);
		miercoles.setColorText(new Color(55, 99, 53));
		miercoles.setColorHover(new Color(204, 204, 204));
		miercoles.setBackground(Color.WHITE);
		miercoles.setBounds(312, 681, 156, 88);
		mainPanel.add(miercoles);
		
		jueves = new RSButtonIconD();
		jueves.setIcon(calendario);
		jueves.setText("00");
		jueves.setFont(new Font("Tahoma", Font.BOLD, 40));
		jueves.setColorTextHover(Color.WHITE);
		jueves.setColorText(new Color(55, 99, 53));
		jueves.setColorHover(new Color(204, 204, 204));
		jueves.setBackground(Color.WHITE);
		jueves.setBounds(468, 681, 156, 88);
		mainPanel.add(jueves);
		
		viernes = new RSButtonIconD();
		viernes.setIcon(calendario);
		viernes.setText("00");
		viernes.setFont(new Font("Tahoma", Font.BOLD, 40));
		viernes.setColorTextHover(Color.WHITE);
		viernes.setColorText(new Color(55, 99, 53));
		viernes.setColorHover(new Color(204, 204, 204));
		viernes.setBackground(Color.WHITE);
		viernes.setBounds(624, 681, 156, 88);
		mainPanel.add(viernes);
		
		selectLunes = new RSToggleButtonShade();
		selectLunes.setBgShadeHover(new Color(255, 255, 255));
		selectLunes.setBgHover(PrimarioLight);
		selectLunes.setForeground(new Color(46, 139, 87));
		selectLunes.setBackground(Primario);
		selectLunes.setFont(new Font("Tahoma", Font.BOLD, 20));
		selectLunes.setText("Lunes");
		selectLunes.setBounds(0, 636, 156, 45);
		mainPanel.add(selectLunes);
		
		selectMartes = new RSToggleButtonShade();
		selectMartes.setBgShadeHover(new Color(255, 255, 255));
		selectMartes.setBgHover(PrimarioLight);
		selectMartes.setForeground(new Color(46, 139, 87));
		selectMartes.setBackground(Primario);
		selectMartes.setFont(new Font("Tahoma", Font.BOLD, 20));
		selectMartes.setText("Martes");
		selectMartes.setBounds(156, 636, 156, 45);
		mainPanel.add(selectMartes);
		
		selectMiercoles = new RSToggleButtonShade();
		selectMiercoles.setBgShadeHover(new Color(255, 255, 255));
		selectMiercoles.setBgHover(PrimarioLight);
		selectMiercoles.setForeground(new Color(46, 139, 87));
		selectMiercoles.setBackground(Primario);
		selectMiercoles.setFont(new Font("Tahoma", Font.BOLD, 20));
		selectMiercoles.setText("Mi\u00E9rcoles");
		selectMiercoles.setBounds(312, 636, 156, 45);
		mainPanel.add(selectMiercoles);
		
		selectJueves = new RSToggleButtonShade();
		selectJueves.setBgShadeHover(new Color(255, 255, 255));
		selectJueves.setBgHover(PrimarioLight);
		selectJueves.setForeground(new Color(46, 139, 87));
		selectJueves.setBackground(Primario);
		selectJueves.setFont(new Font("Tahoma", Font.BOLD, 20));
		selectJueves.setText("Jueves");
		selectJueves.setBounds(468, 636, 156, 45);
		mainPanel.add(selectJueves);
		
		selectViernes = new RSToggleButtonShade();
		selectViernes.setBgShadeHover(new Color(255, 255, 255));
		selectViernes.setBgHover(PrimarioLight);
		selectViernes.setForeground(new Color(46, 139, 87));
		selectViernes.setBackground(Primario);
		selectViernes.setFont(new Font("Tahoma", Font.BOLD, 20));
		selectViernes.setText("Viernes");
		selectViernes.setBounds(624, 636, 156, 45);
		mainPanel.add(selectViernes);
		
		JLabel altaRapida = new JLabel("Alta r\u00E1pida de alumnos");
		altaRapida.setForeground(Color.WHITE);
		altaRapida.setFont(new Font("Tahoma", Font.BOLD, 20));
		altaRapida.setBounds(15, 94, 250, 62);
		mainPanel.add(altaRapida);
		
		cursoAlumno = new RSComboMetro();
		cursoAlumno.setFont(new Font("Tahoma", Font.BOLD, 18));
		cursoAlumno.setModel(new DefaultComboBoxModel<String>(new String[] {"Seleccione curso", "ESO 1\u00BA", "ESO 2\u00BA", "ESO 3\u00BA", "ESO 4\u00BA", "FPB\u00E1sico 1\u00BA", "FPB\u00E1sico 2\u00BA", "GMedio 1\u00BA", "GMedio 2\u00BA", "GSuperior 1\u00BA", "GSuperior 2\u00BA"}));
		cursoAlumno.setColorBorde(new Color(255, 255, 255));
		cursoAlumno.setColorFondo(Primario);
		cursoAlumno.setColorArrow(Primario);
		cursoAlumno.setBounds(15, 224, 250, 42);
		mainPanel.add(cursoAlumno);
		
		nombreAlumno = new RSTextFieldShade();
		nombreAlumno.setPlaceholder("Nombre alumno");
		nombreAlumno.setFont(new Font("Tahoma", Font.PLAIN, 18));
		nombreAlumno.setBgShadeHover(Primario);
		nombreAlumno.setBgShade(new Color(255, 255, 255));
		nombreAlumno.setPixels(0);
		nombreAlumno.setBounds(15, 163, 250, 45);
		mainPanel.add(nombreAlumno);
		
		futbolSelect = new RSToggleButtonShade();
		futbolSelect.setBgShadeHover(new Color(255, 255, 255));
		futbolSelect.setBgHover(PrimarioLight);
		futbolSelect.setBackground(Primario);
		futbolSelect.setBounds(15, 282, 83, 83);
		Icon iselectFut=new ImageIcon(selectFut.getImage().getScaledInstance(futbolSelect.getWidth(), futbolSelect.getHeight(), Image.SCALE_DEFAULT));
		futbolSelect.setIcon(iselectFut);
		mainPanel.add(futbolSelect);
		
		ajedrezSelect = new RSToggleButtonShade();
		ajedrezSelect.setBgShadeHover(new Color(255, 255, 255));
		ajedrezSelect.setBgHover(PrimarioLight);
		ajedrezSelect.setBackground(Primario);
		ajedrezSelect.setBounds(99, 282, 83, 83);
		Icon iselectAje=new ImageIcon(selectAje.getImage().getScaledInstance(ajedrezSelect.getWidth(), ajedrezSelect.getHeight(), Image.SCALE_DEFAULT));
		ajedrezSelect.setIcon(iselectAje);
		mainPanel.add(ajedrezSelect);
		
		tenisSelect = new RSToggleButtonShade();
		tenisSelect.setBgShadeHover(new Color(255, 255, 255));
		tenisSelect.setBgHover(PrimarioLight);
		tenisSelect.setBackground(Primario);
		tenisSelect.setBounds(183, 282, 83, 83);
		Icon iselectTen=new ImageIcon(selectTen.getImage().getScaledInstance(tenisSelect.getWidth(), tenisSelect.getHeight(), Image.SCALE_DEFAULT));
		tenisSelect.setIcon(iselectTen);
		mainPanel.add(tenisSelect);
		
		observaciones = new JEditorPane();
		observaciones.setBounds(15, 370, 250, 162);
		mainPanel.add(observaciones);
		observaciones.setText("Observaciones");
		observaciones.setSelectedTextColor(new Color(255, 255, 255));
		observaciones.setSelectionColor(Primario);
		observaciones.setFont(new Font("Tahoma", Font.PLAIN, 18));
		observaciones.setForeground(Primario);
		observaciones.setBackground(new Color(255, 255, 255));
		observaciones.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		
		JLabel label = new JLabel("0");
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(new Color(255, 255, 255));
		label.setBounds(249, 532, 20, 20);
		mainPanel.add(label);
		
		RSMaterialButtonRectangle guardarJugador = new RSMaterialButtonRectangle();
		guardarJugador.setFont(new Font("Dialog", Font.PLAIN, 18));
		guardarJugador.addActionListener(Controlador.annadirJugador(10));
		guardarJugador.setText("Guardar");
		guardarJugador.setBounds(47, 559, 200, 60);
		guardarJugador.setBackground(PrimarioLight);
		mainPanel.add(guardarJugador);
		
		
		
		panelInformacionPrincipal = new RSPanelShadow();
		panelInformacionPrincipal.setBounds(280, 108, 500, 515);
		mainPanel.add(panelInformacionPrincipal);

		observaciones.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (observaciones.getText().toString().equals("Observaciones")) {
					observaciones.setText("");
				}
			}
		});
		observaciones.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				int numeroCaracteres=observaciones.getText().toString().length()+1;
				label.setText(""+numeroCaracteres);
			}
		});
		
		futbolPanel = new JPanel();
		futbolPanel.setName("futbolPanel");
		futbolPanel.setBackground(PrimarioDark);
		panelSliderPrincipal.add(futbolPanel, "name_393956218395309");
		futbolPanel.setLayout(null);
		
		RSButtonMetro btnVerPartidos = new RSButtonMetro();
		btnVerPartidos.setGrosorLinea(10);
		btnVerPartidos.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnVerPartidos.setBackground(Primario);
		btnVerPartidos.addActionListener(Controlador.abrirVentanasFutbol(2));
		
		JTabbedPane FasesFutbol = new JTabbedPane(JTabbedPane.TOP);
		FasesFutbol.setFont(new Font("Tahoma", Font.PLAIN, 20));
		FasesFutbol.setForeground(new Color(255, 255, 255));
		FasesFutbol.setBorder(null);
		FasesFutbol.setBackground(Primario);
		FasesFutbol.setBounds(0, 99, 780, 598);
		
		futbolPanel.add(FasesFutbol);
		
		JPanel Fase_1 = new JPanel();
		Fase_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		Fase_1.setName("Fase 1");
		Fase_1.setToolTipText("Fase clasificaci\u00F3n");
		Fase_1.setBackground(new Color(255, 255, 255));
		FasesFutbol.addTab("Fase 1", new ImageIcon("resources/iconsBotones/network.png"), Fase_1, "Fase clasificaci\u00F3n");
		FasesFutbol.setEnabledAt(0, true);
		FasesFutbol.setBackgroundAt(0, Primario);
		FasesFutbol.setForegroundAt(0, new Color(255, 255, 224));
		Fase_1.setLayout(null);
		
		RSMaterialButtonRectangle guardarGrupos = new RSMaterialButtonRectangle();
		guardarGrupos.addActionListener(Controlador.guardarEquipoEnGrupo());
		guardarGrupos.setFont(new Font("Dialog", Font.PLAIN, 18));
		guardarGrupos.setText("Guardar");
		guardarGrupos.setBounds(565, 498, 200, 60);
		guardarGrupos.setBackground(PrimarioLight);
		Fase_1.add(guardarGrupos);
		
		RSPanelGradiente panelGrupoA = new RSPanelGradiente();
		panelGrupoA.setBounds(10, 10, 240, 235);
		Fase_1.add(panelGrupoA);
		panelGrupoA.setColorSecundario(PrimarioLight);
		panelGrupoA.setColorPrimario(PrimarioDark);
		panelGrupoA.setLayout(null);
		
		lblNombreGrupo = new JLabel("GRUPO A");
		lblNombreGrupo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNombreGrupo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNombreGrupo.setForeground(new Color(255, 255, 255));
		lblNombreGrupo.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNombreGrupo.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreGrupo.setBounds(0, 0, 240, 31);
		panelGrupoA.add(lblNombreGrupo);
		lblNombreGrupo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {				
				Controlador.abrirInfoGrupo(1);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNombreGrupo.setForeground(PrimarioLight);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNombreGrupo.setForeground(Color.white);
			}
		});
		
		listA = new JList<String>();
		listA.setForeground(new Color(255, 255, 255));
		listA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		listA.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent arg0) {
				System.out.println("INICIO COGER ELIMINADO");
				Character ch = new Character(arg0.getKeyChar());
				System.out.println(ch.getNumericValue(arg0.getKeyChar()));
				System.out.println("FIN COGER VALOR");
			}
			@Override
			public void keyPressed(KeyEvent arg0) {
				System.out.println(arg0.getKeyChar());
			}
		});
		listA.setBackground(PrimarioLight);
		listA.setBounds(0, 31, 240, 168);
		listA.setModel(modellist);
		panelGrupoA.add(listA);
		
		JPopupMenu menuA = new JPopupMenu();
		menuA.setBackground(Color.WHITE);
		menuA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuA.setForeground(Color.WHITE);
		menuA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		menuA.setPopupSize(new Dimension(180, 50));
		addPopup(listA, menuA);
		
		JMenuItem eliminarA = new JMenuItem("Eliminar equipo");
		eliminarA.setBorder(null);
		eliminarA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		eliminarA.setForeground(Color.WHITE);
		eliminarA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		eliminarA.setBackground(Primario);
		eliminarA.setHorizontalTextPosition(SwingConstants.CENTER);
		eliminarA.setHorizontalAlignment(SwingConstants.CENTER);
		eliminarA.addActionListener(Controlador.eliminarEquipoSeleccionado("A"));
		menuA.add(eliminarA);
		
		comboGrupoA = new RSComboMetro();
		comboGrupoA.setColorBorde(new Color(255, 255, 255));
		comboGrupoA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		comboGrupoA.setBounds(0, 201, 240, 32);
		panelGrupoA.add(comboGrupoA);
		
		RSPanelGradiente panelGrupoB = new RSPanelGradiente();
		panelGrupoB.setBounds(268, 10, 240, 235);
		Fase_1.add(panelGrupoB);
		panelGrupoB.setColorSecundario(PrimarioLight);
		panelGrupoB.setColorPrimario(PrimarioDark);
		panelGrupoB.setLayout(null);
		
		JLabel lblNombreGrupoB = new JLabel("GRUPO B");
		lblNombreGrupoB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNombreGrupoB.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNombreGrupoB.setForeground(new Color(255, 255, 255));
		lblNombreGrupoB.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNombreGrupoB.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreGrupoB.setBounds(0, 0, 240, 31);
		panelGrupoB.add(lblNombreGrupoB);
		lblNombreGrupoB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {				
				Controlador.abrirInfoGrupo(2);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNombreGrupoB.setForeground(PrimarioLight);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNombreGrupoB.setForeground(Color.white);
			}
		});
		
		
		listB = new JList<String>();
		listB.setForeground(new Color(255, 255, 255));
		listB.setFont(new Font("Tahoma", Font.PLAIN, 18));
		listB.setBackground(PrimarioLight);
		listB.setBounds(0, 31, 240, 168);
		panelGrupoB.add(listB);
		
		JPopupMenu menuBE = new JPopupMenu();
		menuBE.setBackground(Color.WHITE);
		menuBE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuBE.setPopupSize(new Dimension(180, 50));
		menuBE.setForeground(Color.WHITE);
		menuBE.setFont(new Font("Tahoma", Font.PLAIN, 18));
		addPopup(listB, menuBE);
		
		JMenuItem eliminarB = new JMenuItem("Eliminar equipo");
		eliminarB.setForeground(Color.WHITE);
		eliminarB.setFont(new Font("Tahoma", Font.PLAIN, 18));
		eliminarB.setBorder(null);
		eliminarB.setBackground(Primario);
		eliminarB.addActionListener(Controlador.eliminarEquipoSeleccionado("B"));
		menuBE.add(eliminarB);
		
		comboGrupoB = new RSComboMetro();
		comboGrupoB.setColorBorde(new Color(255, 255, 255));
		comboGrupoB.setFont(new Font("Tahoma", Font.PLAIN, 18));
		comboGrupoB.setBounds(0, 201, 240, 32);
		panelGrupoB.add(comboGrupoB);
		
		RSPanelGradiente panelGrupoC = new RSPanelGradiente();
		panelGrupoC.setBounds(525, 10, 240, 235);
		Fase_1.add(panelGrupoC);
		panelGrupoC.setColorSecundario(PrimarioLight);
		panelGrupoC.setColorPrimario(PrimarioDark);
		panelGrupoC.setLayout(null);
		
		JLabel lblNombreGrupoC = new JLabel("GRUPO C");
		lblNombreGrupoC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNombreGrupoC.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNombreGrupoC.setForeground(new Color(255, 255, 255));
		lblNombreGrupoC.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNombreGrupoC.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreGrupoC.setBounds(0, 0, 240, 31);
		panelGrupoC.add(lblNombreGrupoC);
		lblNombreGrupoC.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {				
				Controlador.abrirInfoGrupo(3);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNombreGrupoC.setForeground(PrimarioLight);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNombreGrupoC.setForeground(Color.white);
			}
		});
		
		listC = new JList<String>();
		listC.setForeground(new Color(255, 255, 255));
		listC.setFont(new Font("Tahoma", Font.PLAIN, 18));
		listC.setBackground(PrimarioLight);
		listC.setBounds(0, 31, 240, 168);
		panelGrupoC.add(listC);
		
		JPopupMenu menuC = new JPopupMenu();
		menuC.setBackground(Color.WHITE);
		menuC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuC.setPopupSize(new Dimension(180, 50));
		menuC.setForeground(Color.WHITE);
		menuC.setFont(new Font("Tahoma", Font.PLAIN, 18));
		addPopup(listC, menuC);
		
		JMenuItem eliminarC = new JMenuItem("Eliminar equipo");
		eliminarC.setForeground(Color.WHITE);
		eliminarC.setFont(new Font("Tahoma", Font.PLAIN, 18));
		eliminarC.setBorder(null);
		eliminarC.setBackground(Primario);
		eliminarC.addActionListener(Controlador.eliminarEquipoSeleccionado("C"));
		menuC.add(eliminarC);
		
		comboGrupoC = new RSComboMetro();
		comboGrupoC.setColorBorde(new Color(255, 255, 255));
		comboGrupoC.setFont(new Font("Tahoma", Font.PLAIN, 18));
		comboGrupoC.setBounds(0, 201, 240, 32);
		panelGrupoC.add(comboGrupoC);
		
		RSPanelGradiente panelGrupoD = new RSPanelGradiente();
		panelGrupoD.setBounds(10, 261, 240, 235);
		Fase_1.add(panelGrupoD);
		panelGrupoD.setColorSecundario(PrimarioLight);
		panelGrupoD.setColorPrimario(PrimarioDark);
		panelGrupoD.setLayout(null);
		
		JLabel lblNombreGrupoD = new JLabel("GRUPO D");
		lblNombreGrupoD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNombreGrupoD.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNombreGrupoD.setForeground(new Color(255, 255, 255));
		lblNombreGrupoD.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNombreGrupoD.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreGrupoD.setBounds(0, 0, 240, 31);
		panelGrupoD.add(lblNombreGrupoD);
		lblNombreGrupoD.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {				
				Controlador.abrirInfoGrupo(4);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNombreGrupoD.setForeground(PrimarioLight);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNombreGrupoD.setForeground(Color.white);
			}
		});
		
		listD = new JList<String>();
		listD.setForeground(new Color(255, 255, 255));
		listD.setFont(new Font("Tahoma", Font.PLAIN, 18));
		listD.setBackground(PrimarioLight);
		listD.setBounds(0, 31, 240, 168);
		panelGrupoD.add(listD);
		
		JPopupMenu menuD = new JPopupMenu();
		menuD.setBackground(Color.WHITE);
		menuD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuD.setPopupSize(new Dimension(180, 50));
		menuD.setForeground(Color.WHITE);
		menuD.setFont(new Font("Tahoma", Font.PLAIN, 18));
		addPopup(listD, menuD);
		
		JMenuItem eliminarD = new JMenuItem("Eliminar equipo");
		eliminarD.setForeground(Color.WHITE);
		eliminarD.setFont(new Font("Tahoma", Font.PLAIN, 18));
		eliminarD.setBorder(null);
		eliminarD.setBackground(new Color(0, 128, 0));
		eliminarD.addActionListener(Controlador.eliminarEquipoSeleccionado("D"));
		menuD.add(eliminarD);
		
		comboGrupoD = new RSComboMetro();
		comboGrupoD.setColorBorde(new Color(255, 255, 255));
		comboGrupoD.setFont(new Font("Tahoma", Font.PLAIN, 18));
		comboGrupoD.setBounds(0, 201, 240, 32);
		panelGrupoD.add(comboGrupoD);
		
		RSPanelGradiente panelGrupoE = new RSPanelGradiente();
		panelGrupoE.setBounds(268, 261, 240, 235);
		Fase_1.add(panelGrupoE);
		panelGrupoE.setColorSecundario(PrimarioLight);
		panelGrupoE.setColorPrimario(PrimarioDark);
		panelGrupoE.setLayout(null);
		
		JLabel lblNombreGrupoE = new JLabel("GRUPO E");
		lblNombreGrupoE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNombreGrupoE.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNombreGrupoE.setForeground(new Color(255, 255, 255));
		lblNombreGrupoE.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNombreGrupoE.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreGrupoE.setBounds(0, 0, 240, 31);
		panelGrupoE.add(lblNombreGrupoE);
		lblNombreGrupoE.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {				
				Controlador.abrirInfoGrupo(5);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNombreGrupoE.setForeground(PrimarioLight);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNombreGrupoE.setForeground(Color.white);
			}
		});
		
		listE = new JList<String>();
		listE.setForeground(new Color(255, 255, 255));
		listE.setFont(new Font("Tahoma", Font.PLAIN, 18));
		listE.setBackground(PrimarioLight);
		listE.setBounds(0, 31, 240, 168);
		panelGrupoE.add(listE);
		
		JPopupMenu menuE = new JPopupMenu();
		menuE.setBackground(Color.WHITE);
		menuE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuE.setPopupSize(new Dimension(180, 50));
		menuE.setForeground(Color.WHITE);
		menuE.setFont(new Font("Tahoma", Font.PLAIN, 18));
		addPopup(listE, menuE);
		
		JMenuItem eliminarE = new JMenuItem("Eliminar equipo");
		eliminarE.setForeground(Color.WHITE);
		eliminarE.setFont(new Font("Tahoma", Font.PLAIN, 18));
		eliminarE.setBorder(null);
		eliminarE.setBackground(new Color(0, 128, 0));
		eliminarE.addActionListener(Controlador.eliminarEquipoSeleccionado("E"));
		menuE.add(eliminarE);
		
		comboGrupoE = new RSComboMetro();
		comboGrupoE.setColorBorde(new Color(255, 255, 255));
		comboGrupoE.setFont(new Font("Tahoma", Font.PLAIN, 18));
		comboGrupoE.setBounds(0, 201, 240, 32);
		panelGrupoE.add(comboGrupoE);
		
		RSPanelGradiente panelGrupoF = new RSPanelGradiente();
		panelGrupoF.setBounds(525, 259, 240, 235);
		Fase_1.add(panelGrupoF);
		panelGrupoF.setColorSecundario(PrimarioLight);
		panelGrupoF.setColorPrimario(PrimarioDark);
		panelGrupoF.setLayout(null);
		
		JLabel lblNombreGrupoF = new JLabel("GRUPO F");
		lblNombreGrupoF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNombreGrupoF.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNombreGrupoF.setForeground(new Color(255, 255, 255));
		lblNombreGrupoF.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNombreGrupoF.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreGrupoF.setBounds(0, 0, 240, 31);
		panelGrupoF.add(lblNombreGrupoF);
		lblNombreGrupoF.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {				
				Controlador.abrirInfoGrupo(6);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNombreGrupoF.setForeground(PrimarioLight);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNombreGrupoF.setForeground(Color.white);
			}
		});
		
		listF = new JList<String>();
		listF.setForeground(new Color(255, 255, 255));
		listF.setFont(new Font("Tahoma", Font.PLAIN, 18));
		listF.setBackground(PrimarioLight);
		listF.setBounds(0, 31, 240, 168);
		panelGrupoF.add(listF);
		
		JPopupMenu menuF = new JPopupMenu();
		menuF.setBackground(Color.WHITE);
		menuF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuF.setPopupSize(new Dimension(180, 50));
		menuF.setForeground(Color.WHITE);
		menuF.setFont(new Font("Tahoma", Font.PLAIN, 18));
		addPopup(listF, menuF);
		
		JMenuItem eliminarF = new JMenuItem("Eliminar equipo");
		eliminarF.setForeground(Color.WHITE);
		eliminarF.setFont(new Font("Tahoma", Font.PLAIN, 18));
		eliminarF.setBorder(null);
		eliminarF.setBackground(new Color(0, 128, 0));
		eliminarF.addActionListener(Controlador.eliminarEquipoSeleccionado("F"));
		menuF.add(eliminarF);
		
		comboGrupoF = new RSComboMetro();
		comboGrupoF.setColorBorde(new Color(255, 255, 255));
		comboGrupoF.setFont(new Font("Tahoma", Font.PLAIN, 18));
		comboGrupoF.setBounds(0, 201, 240, 32);
		panelGrupoF.add(comboGrupoF);
		
		RSMaterialButtonRectangle btnGenerarPartidosDeFutbol = new RSMaterialButtonRectangle();
		btnGenerarPartidosDeFutbol.addActionListener(Controlador.generarPartidosGrupo());
		btnGenerarPartidosDeFutbol.setText("Generar partidos");
		btnGenerarPartidosDeFutbol.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnGenerarPartidosDeFutbol.setBackground(PrimarioLight);
		btnGenerarPartidosDeFutbol.setBounds(361, 498, 200, 60);
		Fase_1.add(btnGenerarPartidosDeFutbol);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setForeground(Color.BLACK);
		separator_2.setBackground(Color.BLACK);
		separator_2.setBounds(0, 530, 775, 13);
		Fase_1.add(separator_2);
		
		JPanel Fase_2 = new JPanel();
		Fase_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		Fase_2.setBackground(new Color(255, 255, 255));
		FasesFutbol.addTab("Cuartos de final", new ImageIcon("resources/iconsBotones/match.png"), Fase_2, "Cuartos");
		FasesFutbol.setForegroundAt(1, new Color(255, 255, 224));
		FasesFutbol.setBackgroundAt(1, Primario);
		FasesFutbol.setEnabledAt(1, true);
		Fase_2.setLayout(null);
		
		p1Local = new RSComboMetro();
		p1Local.setColorBorde(Color.WHITE);
		p1Local.setColorArrow(Primario);
		p1Local.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p1Local.setBounds(15, 13, 250, 32);
		Fase_2.add(p1Local);
		
		p1Visitante = new RSComboMetro();
		p1Visitante.setColorBorde(Color.WHITE);
		p1Visitante.setColorArrow(Primario);
		p1Visitante.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p1Visitante.setBounds(510, 13, 250, 32);
		Fase_2.add(p1Visitante);
		
		p2Local = new RSComboMetro();
		p2Local.setColorBorde(Color.WHITE);
		p2Local.setColorArrow(Primario);
		p2Local.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p2Local.setBounds(15, 128, 250, 32);
		Fase_2.add(p2Local);
		
		p2Visitante = new RSComboMetro();
		p2Visitante.setColorBorde(Color.WHITE);
		p2Visitante.setColorArrow(Primario);
		p2Visitante.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p2Visitante.setBounds(510, 128, 250, 32);
		Fase_2.add(p2Visitante);
		
		p3Local = new RSComboMetro();
		p3Local.setColorBorde(Color.WHITE);
		p3Local.setColorArrow(Primario);
		p3Local.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p3Local.setBounds(15, 316, 250, 32);
		Fase_2.add(p3Local);
		
		p3Visitante = new RSComboMetro();
		p3Visitante.setColorBorde(Color.WHITE);
		p3Visitante.setColorArrow(Primario);
		p3Visitante.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p3Visitante.setBounds(510, 316, 250, 32);
		Fase_2.add(p3Visitante);
		
		p4Local = new RSComboMetro();
		p4Local.setColorBorde(Color.WHITE);
		p4Local.setColorArrow(Primario);
		p4Local.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p4Local.setBounds(15, 422, 250, 32);
		Fase_2.add(p4Local);
		
		p4Visitante = new RSComboMetro();
		p4Visitante.setColorBorde(Color.WHITE);
		p4Visitante.setColorArrow(Primario);
		p4Visitante.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p4Visitante.setBounds(510, 422, 250, 32);
		Fase_2.add(p4Visitante);
		
		p1GolesLocal = new RSTextFieldShade();
		p1GolesLocal.setBgShadeHover(PrimarioLight);
		p1GolesLocal.setHorizontalAlignment(SwingConstants.CENTER);
		p1GolesLocal.setFont(new Font("Tahoma", Font.BOLD, 18));
		p1GolesLocal.setText("0");
		p1GolesLocal.setBounds(219, 49, 46, 45);
		Fase_2.add(p1GolesLocal);
		
		p1GolesVisitante = new RSTextFieldShade();
		p1GolesVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		p1GolesVisitante.setBgShadeHover(PrimarioLight);
		p1GolesVisitante.setText("0");
		p1GolesVisitante.setFont(new Font("Tahoma", Font.BOLD, 18));
		p1GolesVisitante.setBounds(510, 50, 46, 45);
		Fase_2.add(p1GolesVisitante);
		
		p2GolesLocal = new RSTextFieldShade();
		p2GolesLocal.setHorizontalAlignment(SwingConstants.CENTER);
		p2GolesLocal.setBgShadeHover(PrimarioLight);
		p2GolesLocal.setText("0");
		p2GolesLocal.setFont(new Font("Tahoma", Font.BOLD, 18));
		p2GolesLocal.setBounds(219, 166, 46, 45);
		Fase_2.add(p2GolesLocal);
		
		p2GolesVisitante = new RSTextFieldShade();
		p2GolesVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		p2GolesVisitante.setBgShadeHover(PrimarioLight);
		p2GolesVisitante.setText("0");
		p2GolesVisitante.setFont(new Font("Tahoma", Font.BOLD, 18));
		p2GolesVisitante.setBounds(510, 167, 46, 45);
		Fase_2.add(p2GolesVisitante);
		
		p3GolesLocal = new RSTextFieldShade();
		p3GolesLocal.setHorizontalAlignment(SwingConstants.CENTER);
		p3GolesLocal.setBgShadeHover(PrimarioLight);
		p3GolesLocal.setText("0");
		p3GolesLocal.setFont(new Font("Tahoma", Font.BOLD, 18));
		p3GolesLocal.setBounds(219, 361, 46, 45);
		Fase_2.add(p3GolesLocal);
		
		p3GolesVisitante = new RSTextFieldShade();
		p3GolesVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		p3GolesVisitante.setBgShadeHover(PrimarioLight);
		p3GolesVisitante.setText("0");
		p3GolesVisitante.setFont(new Font("Tahoma", Font.BOLD, 18));
		p3GolesVisitante.setBounds(510, 364, 46, 45);
		Fase_2.add(p3GolesVisitante);
		
		p4GolesLocal = new RSTextFieldShade();
		p4GolesLocal.setHorizontalAlignment(SwingConstants.CENTER);
		p4GolesLocal.setBgShadeHover(PrimarioLight);
		p4GolesLocal.setText("0");
		p4GolesLocal.setFont(new Font("Tahoma", Font.BOLD, 18));
		p4GolesLocal.setBounds(219, 470, 46, 45);
		Fase_2.add(p4GolesLocal);
		
		p4GolesVisitante = new RSTextFieldShade();
		p4GolesVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		p4GolesVisitante.setBgShadeHover(PrimarioLight);
		p4GolesVisitante.setText("0");
		p4GolesVisitante.setFont(new Font("Tahoma", Font.BOLD, 18));
		p4GolesVisitante.setBounds(510, 470, 46, 45);
		Fase_2.add(p4GolesVisitante);
		
		p1Fecha = new RSDateChooser();
		p1Fecha.setColorForeground(Primario);
		p1Fecha.setColorButtonHover(new Color(0, 255, 0));
		p1Fecha.setColorBackground(Primario);
		p1Fecha.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		p1Fecha.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p1Fecha.setFormatoFecha("dd//MM/yyyy");
		p1Fecha.setBounds(293, 13, 200, 40);
		Fase_2.add(p1Fecha);
		
		p2Fecha = new RSDateChooser();
		p2Fecha.setColorForeground(Primario);
		p2Fecha.setColorButtonHover(new Color(0, 255, 0));
		p2Fecha.setColorBackground(Primario);
		p2Fecha.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		p2Fecha.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p2Fecha.setFormatoFecha("dd//MM/yyyy");
		p2Fecha.setBounds(293, 131, 200, 40);
		Fase_2.add(p2Fecha);
		
		p3Fecha = new RSDateChooser();
		p3Fecha.setColorForeground(Primario);
		p3Fecha.setColorButtonHover(new Color(0, 255, 0));
		p3Fecha.setColorBackground(Primario);
		p3Fecha.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		p3Fecha.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p3Fecha.setFormatoFecha("dd//MM/yyyy");
		p3Fecha.setBounds(293, 316, 200, 40);
		Fase_2.add(p3Fecha);
		
		p4Fecha = new RSDateChooser();
		p4Fecha.setColorForeground(Primario);
		p4Fecha.setColorButtonHover(new Color(0, 255, 0));
		p4Fecha.setColorBackground(Primario);
		p4Fecha.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		p4Fecha.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p4Fecha.setFormatoFecha("dd//MM/yyyy");
		p4Fecha.setBounds(293, 422, 200, 40);
		Fase_2.add(p4Fecha);
		
		RSButtonMetro btnGuardarPartidosF2 = new RSButtonMetro();
		btnGuardarPartidosF2.addActionListener(Controlador.crearPartidosFase2());
		btnGuardarPartidosF2.setBackground(Primario);
		btnGuardarPartidosF2.setText("Guardar ");
		btnGuardarPartidosF2.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnGuardarPartidosF2.setBounds(180, 236, 200, 70);
		Fase_2.add(btnGuardarPartidosF2);
		
		RSButtonMetro btnSiguienteFaseF2 = new RSButtonMetro();
		btnSiguienteFaseF2.setBackground(Primario);
		btnSiguienteFaseF2.setText(">");
		btnSiguienteFaseF2.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnSiguienteFaseF2.setBounds(400, 236, 200, 70);
		Fase_2.add(btnSiguienteFaseF2);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(new Color(0, 0, 0));
		separator.setForeground(new Color(0, 0, 0));
		separator.setBounds(0, 266, 775, 13);
		Fase_2.add(separator);
		
		JPanel Fase_3 = new JPanel();
		Fase_3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		Fase_3.setBackground(new Color(255, 255, 255));
		FasesFutbol.addTab("Semifinal", new ImageIcon("resources/iconsBotones/football-field.png"), Fase_3, "Semifinales");
		FasesFutbol.setForegroundAt(2, new Color(255, 255, 224));
		FasesFutbol.setBackgroundAt(2, Primario);
		FasesFutbol.setEnabledAt(2, true);
		Fase_3.setLayout(null);
		
		RSComboMetro p1F3Local = new RSComboMetro();
		p1F3Local.setColorFondo(Primario);
		p1F3Local.setColorBorde(Color.WHITE);
		p1F3Local.setColorArrow(Primario);
		p1F3Local.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p1F3Local.setBounds(15, 45, 300, 50);
		Fase_3.add(p1F3Local);
		
		RSComboMetro p1F3Visitante = new RSComboMetro();
		p1F3Visitante.setColorFondo(Primario);
		p1F3Visitante.setColorBorde(Color.WHITE);
		p1F3Visitante.setColorArrow(Primario);
		p1F3Visitante.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p1F3Visitante.setBounds(460, 45, 300, 50);
		Fase_3.add(p1F3Visitante);
		
		RSComboMetro p2F3Local = new RSComboMetro();
		p2F3Local.setColorFondo(Primario);
		p2F3Local.setColorBorde(Color.WHITE);
		p2F3Local.setColorArrow(Primario);
		p2F3Local.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p2F3Local.setBounds(15, 329, 300, 50);
		Fase_3.add(p2F3Local);
		
		RSComboMetro p2F3Visitante = new RSComboMetro();
		p2F3Visitante.setColorFondo(Primario);
		p2F3Visitante.setColorBorde(Color.WHITE);
		p2F3Visitante.setColorArrow(Primario);
		p2F3Visitante.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p2F3Visitante.setBounds(460, 329, 300, 50);
		Fase_3.add(p2F3Visitante);
		
		RSButtonMetro btnGuardar = new RSButtonMetro();
		btnGuardar.setBackground(Primario);
		btnGuardar.setText("Guardar");
		btnGuardar.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnGuardar.setBounds(181, 253, 200, 70);
		Fase_3.add(btnGuardar);
		
		RSButtonMetro btnALaFinal = new RSButtonMetro();
		btnALaFinal.setBackground(Primario);
		btnALaFinal.setText(">");
		btnALaFinal.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnALaFinal.setBounds(396, 253, 200, 70);
		Fase_3.add(btnALaFinal);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(Color.BLACK);
		separator_1.setBackground(Color.BLACK);
		separator_1.setBounds(0, 285, 775, 13);
		Fase_3.add(separator_1);
		
		RSTextFieldShade p1F3GolesLocal = new RSTextFieldShade();
		p1F3GolesLocal.setBgShadeHover(Primario);
		p1F3GolesLocal.setHorizontalAlignment(SwingConstants.CENTER);
		p1F3GolesLocal.setText("0");
		p1F3GolesLocal.setFont(new Font("Tahoma", Font.BOLD, 22));
		p1F3GolesLocal.setBounds(255, 111, 60, 60);
		Fase_3.add(p1F3GolesLocal);
		
		RSTextFieldShade p1F3GolesVisitante = new RSTextFieldShade();
		p1F3GolesVisitante.setBgShadeHover(Primario);
		p1F3GolesVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		p1F3GolesVisitante.setText("0");
		p1F3GolesVisitante.setFont(new Font("Tahoma", Font.BOLD, 22));
		p1F3GolesVisitante.setBounds(460, 111, 60, 60);
		Fase_3.add(p1F3GolesVisitante);
		
		RSTextFieldShade p2F3GolesVisitante = new RSTextFieldShade();
		p2F3GolesVisitante.setBgShadeHover(Primario);
		p2F3GolesVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		p2F3GolesVisitante.setText("0");
		p2F3GolesVisitante.setFont(new Font("Tahoma", Font.BOLD, 22));
		p2F3GolesVisitante.setBounds(460, 395, 60, 60);
		Fase_3.add(p2F3GolesVisitante);
		
		RSTextFieldShade p2F3GolesLocal = new RSTextFieldShade();
		p2F3GolesLocal.setBgShadeHover(Primario);
		p2F3GolesLocal.setHorizontalAlignment(SwingConstants.CENTER);
		p2F3GolesLocal.setText("0");
		p2F3GolesLocal.setFont(new Font("Tahoma", Font.BOLD, 22));
		p2F3GolesLocal.setBounds(255, 395, 60, 60);
		Fase_3.add(p2F3GolesLocal);
		
		RSDateChooser p2F3Fecha = new RSDateChooser();
		p2F3Fecha.setColorForeground(Primario);
		p2F3Fecha.setColorBackground(Primario);
		p2F3Fecha.setBounds(255, 471, 265, 50);
		Fase_3.add(p2F3Fecha);
		
		RSDateChooser p1F3Fecha = new RSDateChooser();
		p1F3Fecha.setColorForeground(Primario);
		p1F3Fecha.setColorButtonHover(new Color(0, 128, 128));
		p1F3Fecha.setColorBackground(Primario);
		p1F3Fecha.setFont(new Font("Tahoma", Font.PLAIN, 18));
		p1F3Fecha.setBounds(255, 187, 265, 50);
		Fase_3.add(p1F3Fecha);
		
		JPanel Fase_4 = new JPanel();
		Fase_4.setBackground(Color.WHITE);
		Fase_4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		FasesFutbol.addTab("Final TORNEO", new ImageIcon("resources/iconsBotones/success.png"), Fase_4, "Gran final");
		Fase_4.setLayout(null);
		
		equipo1 = new JTextField();
		equipo1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		equipo1.setText("EQUIPO 1");
		equipo1.setBounds(0, 205, 300, 40);
		Fase_4.add(equipo1);
		equipo1.setColumns(10);
		
		equipo2 = new JTextField();
		equipo2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		equipo2.setText("EQUIPO 2");
		equipo2.setColumns(10);
		equipo2.setBounds(475, 205, 300, 40);
		Fase_4.add(equipo2);
		
		RSTextFieldShade golesLocal = new RSTextFieldShade();
		golesLocal.setPlaceholder("");
		golesLocal.setHorizontalAlignment(SwingConstants.CENTER);
		golesLocal.setText("0");
		golesLocal.setFont(new Font("Tahoma", Font.BOLD, 30));
		golesLocal.setBounds(200, 261, 100, 100);
		Fase_4.add(golesLocal);
		
		RSTextFieldShade golesVisitante = new RSTextFieldShade();
		golesVisitante.setPlaceholder("");
		golesVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		golesVisitante.setText("0");
		golesVisitante.setFont(new Font("Tahoma", Font.BOLD, 30));
		golesVisitante.setBounds(475, 261, 100, 100);
		Fase_4.add(golesVisitante);
		
		JLabel lblLaGranFinal = new JLabel("LA GRAN FINAL");
		lblLaGranFinal.setForeground(new Color(0, 128, 128));
		lblLaGranFinal.setHorizontalAlignment(SwingConstants.CENTER);
		lblLaGranFinal.setHorizontalTextPosition(SwingConstants.CENTER);
		lblLaGranFinal.setFont(new Font("Tahoma", Font.BOLD, 50));
		lblLaGranFinal.setBounds(0, 16, 775, 147);
		Fase_4.add(lblLaGranFinal);
		
		JLabel label_3 = new JLabel("");
		label_3.setToolTipText("\u00BFQui\u00E9n ser\u00E1 el ganador?");
		label_3.setIcon(new ImageIcon("resources/Copa-Mundo-88237.gif"));
		label_3.setBounds(315, 205, 145, 240);
		Fase_4.add(label_3);
		
		RSButtonMetro buttonMetro_5 = new RSButtonMetro();
		buttonMetro_5.setBackground(Primario);
		buttonMetro_5.setText("Guardar");
		buttonMetro_5.setFont(new Font("Tahoma", Font.BOLD, 18));
		buttonMetro_5.setBounds(625, 495, 150, 64);
		Fase_4.add(buttonMetro_5);
		FasesFutbol.setForegroundAt(3, new Color(255, 255, 224));
		FasesFutbol.setBackgroundAt(3, Primario);
		FasesFutbol.setEnabledAt(3, true);
		
		
		btnVerPartidos.setText("Ver partidos");
		btnVerPartidos.setBounds(0, 0, 170, 100);
		futbolPanel.add(btnVerPartidos);
		
		RSButtonMetro btnVerJugadores = new RSButtonMetro();
		btnVerJugadores.setGrosorLinea(10);
		btnVerJugadores.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnVerJugadores.setBackground(Primario);
		btnVerJugadores.addActionListener(Controlador.abrirFrameJugador(1));
		btnVerJugadores.setText("Ver jugadores");
		btnVerJugadores.setBounds(170, 0, 170, 100);
		futbolPanel.add(btnVerJugadores);
		
		RSButtonMetro btnVerEquipos = new RSButtonMetro();
		btnVerEquipos.setGrosorLinea(10);
		btnVerEquipos.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnVerEquipos.setBackground(Primario);
		btnVerEquipos.addActionListener(Controlador.abrirVentanasFutbol(1));
		btnVerEquipos.setText("Ver equipos");
		btnVerEquipos.setBounds(340, 0, 170, 100);
		futbolPanel.add(btnVerEquipos);
		
		JLabel logoPanelFutbol = new JLabel("");
		logoPanelFutbol.setBounds(655, 1, 125, 125);
		Icon banderaEquipo=new ImageIcon(iconoBanderaEquipo.getImage().getScaledInstance(logoPanelFutbol.getWidth(), logoPanelFutbol.getHeight(), Image.SCALE_DEFAULT));
		logoPanelFutbol.setIcon(banderaEquipo);
		futbolPanel.add(logoPanelFutbol);
		
		JLabel lblPdfLogo = new JLabel("PDF LOGO");
		lblPdfLogo.setForeground(new Color(255, 255, 255));
		lblPdfLogo.setBounds(10, 714, 78, 40);
		futbolPanel.add(lblPdfLogo);
		
		tenisPanel = new JPanel();
		tenisPanel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tenisPanel.setName("tenisPanel");
		tenisPanel.setBackground(PrimarioDark);
		panelSliderPrincipal.add(tenisPanel, "name_393962276656864");
		tenisPanel.setLayout(null);
		
		JLabel tenisTitulo = new JLabel("Clasificaciones tenis de mesa");
		tenisTitulo.setForeground(new Color(255, 255, 255));
		tenisTitulo.setFont(new Font("Tahoma", Font.BOLD, 18));
		tenisTitulo.setBounds(15, 16, 267, 20);
		tenisPanel.add(tenisTitulo);
		
		JScrollPane TablaTenis = new JScrollPane();
		TablaTenis.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		TablaTenis.setBounds(15, 164, 750, 323);
		tenisPanel.add(TablaTenis);
		
		tablaParatidasTenis = new RSTableMetro();
		
		tablaParatidasTenis.setAltoHead(30);
		tablaParatidasTenis.setColorFilasForeground1(Color.DARK_GRAY);
		tablaParatidasTenis.setColorFilasBackgound2(Color.WHITE);
		tablaParatidasTenis.setRowMargin(2);
		tablaParatidasTenis.setRowHeight(25);
		tablaParatidasTenis.setEditingRow(0);
		tablaParatidasTenis.setEditingColumn(0);
		tablaParatidasTenis.setFont(new Font("Tahoma", Font.PLAIN, 30));
		tablaParatidasTenis.setColorSelForeground(Color.WHITE);
		tablaParatidasTenis.setColorSelBackgound(Color.DARK_GRAY);
		tablaParatidasTenis.setColorFilasForeground2(Color.DARK_GRAY);
		tablaParatidasTenis.setColorFilasBackgound1(Color.WHITE);
		tablaParatidasTenis.setColorBackgoundHead(Primario);
		tablaParatidasTenis.setSelectionForeground(new Color(0, 0, 0));
		tablaParatidasTenis.setSelectionBackground(new Color(0, 255, 0));
		tablaParatidasTenis.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tablaParatidasTenis.setMultipleSeleccion(false);
		tablaParatidasTenis.setFuenteFilas(new Font("Tahoma", Font.PLAIN, 24));
		tablaParatidasTenis.setFuenteFilasSelect(new Font("Tahoma", Font.PLAIN, 24));
		tablaParatidasTenis.setFuenteHead(new Font("Tahoma", Font.PLAIN, 16));
		
		tablaParatidasTenis.addMouseListener(Controlador.mostrarPartidaTenis());
		TablaTenis.setViewportView(tablaParatidasTenis);
		
		faseXTenis = new JLabel("FASE X");
		faseXTenis.setFont(new Font("Tahoma", Font.PLAIN, 18));
		faseXTenis.setForeground(new Color(255, 255, 255));
		faseXTenis.setBounds(326, 95, 69, 20);
		tenisPanel.add(faseXTenis);
		
		RSMaterialButtonCircle siguienteFaseTenis = new RSMaterialButtonCircle();
		siguienteFaseTenis.addActionListener(Controlador.siguienteTablaTenis());
		siguienteFaseTenis.setFont(new Font("Dialog", Font.PLAIN, 18));
		siguienteFaseTenis.setText(">");
		siguienteFaseTenis.setBackground(PrimarioLight);
		siguienteFaseTenis.setBounds(453, 48, 100, 100);
		tenisPanel.add(siguienteFaseTenis);
		
		RSMaterialButtonCircle anteriorFaseTenis = new RSMaterialButtonCircle();
		anteriorFaseTenis.addActionListener(Controlador.anteriorTablaTenis());
		anteriorFaseTenis.setFont(new Font("Dialog", Font.PLAIN, 18));
		anteriorFaseTenis.setBackground(PrimarioLight);
		anteriorFaseTenis.setText("<");
		anteriorFaseTenis.setBounds(182, 48, 100, 100);
		tenisPanel.add(anteriorFaseTenis);
		
		fechaTenis = new RSDateChooser();
		fechaTenis.setFormatoFecha("dd/MM/yyyy");
		fechaTenis.setPlaceholder("Seleccione fecha del partido");
		fechaTenis.setFuente(new Font("Tahoma", Font.BOLD, 18));
		fechaTenis.setColorForeground(Primario);
		fechaTenis.setColorDiaActual(new Color(0, 255, 255));
		fechaTenis.setColorButtonHover(new Color(50, 205, 50));
		fechaTenis.setColorBackground(Primario);
		fechaTenis.setForeground(Primario);
		fechaTenis.setFont(new Font("Tahoma", Font.BOLD, 18));
		fechaTenis.setBounds(275, 587, 240, 42);
		tenisPanel.add(fechaTenis);
		
		barraFasesTenis = new JProgressBar();
		barraFasesTenis.setForeground(Primario);
		barraFasesTenis.setFont(new Font("Tahoma", Font.PLAIN, 18));
		barraFasesTenis.setStringPainted(true);
		barraFasesTenis.setMaximum(5);
		barraFasesTenis.setBackground(Primario);
		barraFasesTenis.setBounds(298, 140, 146, 24);
		tenisPanel.add(barraFasesTenis);
		
		RSMaterialButtonRectangle btnGuardarTenis = new RSMaterialButtonRectangle();
		btnGuardarTenis.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnGuardarTenis.addActionListener(Controlador.actualizarPartidaTenis());
		btnGuardarTenis.setText("Guardar");
		btnGuardarTenis.setBackground(PrimarioLight);
		btnGuardarTenis.setBounds(15, 693, 200, 60);
		tenisPanel.add(btnGuardarTenis);
		
		ganadorJ1 = new JRadioButton("Ganador");
		ganadorJ1.addMouseListener(Controlador.seleccionarGanadorTenis("local"));
		ganadorJ1.setHorizontalTextPosition(SwingConstants.LEFT);
		ganadorJ1.setForeground(Color.WHITE);
		ganadorJ1.setOpaque(false);
		ganadorJ1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		ganadorJ1.setBounds(275, 555, 100, 29);
		tenisPanel.add(ganadorJ1);
		
		ganadorJ2 = new JRadioButton("Ganador");
		ganadorJ2.addMouseListener(Controlador.seleccionarGanadorTenis("visitante"));
		ganadorJ2.setOpaque(false);
		ganadorJ2.setHorizontalTextPosition(SwingConstants.RIGHT);
		ganadorJ2.setForeground(Color.WHITE);
		ganadorJ2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		ganadorJ2.setBounds(405, 555, 100, 29);
		tenisPanel.add(ganadorJ2);
		
		RSButtonMetro btnVerJugadorsTenis = new RSButtonMetro();
		btnVerJugadorsTenis.addActionListener(Controlador.abrirFrameJugador(3));
		btnVerJugadorsTenis.setText("Ver jugadores");
		btnVerJugadorsTenis.setGrosorLinea(10);
		btnVerJugadorsTenis.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnVerJugadorsTenis.setBackground(Primario);
		btnVerJugadorsTenis.setBounds(565, 593, 200, 80);
		tenisPanel.add(btnVerJugadorsTenis);
		
		RSButtonMetro generarPartidasTenis = new RSButtonMetro();
		generarPartidasTenis.addActionListener(Controlador.generarPartidasTenis());
		generarPartidasTenis.setBackground(Primario);
		generarPartidasTenis.setText("GENERAR PARTIDAS");
		generarPartidasTenis.setFont(new Font("Tahoma", Font.BOLD, 16));
		generarPartidasTenis.setBounds(565, 673, 200, 80);
		
		tenisPanel.add(generarPartidasTenis);
		
		jugadorTenisLocal = new RSMTextFull();
		jugadorTenisLocal.setHorizontalAlignment(SwingConstants.CENTER);
		jugadorTenisLocal.setEditable(false);
		jugadorTenisLocal.setxDarkIcon(true);
		jugadorTenisLocal.setSoloLetras(true);
		jugadorTenisLocal.setSelectionColor(Color.WHITE);
		jugadorTenisLocal.setPlaceholder("Jugador Local");
		jugadorTenisLocal.setModoMaterial(false);
		jugadorTenisLocal.setForeground(Color.WHITE);
		jugadorTenisLocal.setFont(new Font("Dialog", Font.BOLD, 18));
		jugadorTenisLocal.setColorTransparente(true);
		jugadorTenisLocal.setBotonColor(new Color(55, 99, 53));
		jugadorTenisLocal.setBordeColorFocus(new Color(9, 56, 14));
		jugadorTenisLocal.setBounds(15, 502, 360, 40);
		tenisPanel.add(jugadorTenisLocal);
		
		jugadorTenisVisitante = new RSMTextFull();
		jugadorTenisVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		jugadorTenisVisitante.setEditable(false);
		jugadorTenisVisitante.setxDarkIcon(true);
		jugadorTenisVisitante.setSoloLetras(true);
		jugadorTenisVisitante.setSelectionColor(Color.WHITE);
		jugadorTenisVisitante.setPlaceholder("Jugador Visitante");
		jugadorTenisVisitante.setModoMaterial(false);
		jugadorTenisVisitante.setForeground(Color.WHITE);
		jugadorTenisVisitante.setFont(new Font("Dialog", Font.BOLD, 18));
		jugadorTenisVisitante.setColorTransparente(true);
		jugadorTenisVisitante.setBotonColor(new Color(55, 99, 53));
		jugadorTenisVisitante.setBordeColorFocus(new Color(9, 56, 14));
		jugadorTenisVisitante.setBounds(405, 503, 360, 40);
		tenisPanel.add(jugadorTenisVisitante);
		
		JLabel logoPanelTenis = new JLabel("");
		logoPanelTenis.setBounds(640, 16, 125, 125);
		Icon iconPanelTenis=new ImageIcon(iconoTenis.getImage().getScaledInstance(logoPanelTenis.getWidth(), logoPanelTenis.getHeight(), Image.SCALE_DEFAULT));
		logoPanelTenis.setIcon(iconPanelTenis);
		tenisPanel.add(logoPanelTenis);
		
		RSMaterialButtonRectangle btnPDF = new RSMaterialButtonRectangle();
		btnPDF.addActionListener(Controlador.generarPDF(1));
		btnPDF.setText("PDF");
		btnPDF.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnPDF.setBackground(new Color(100, 145, 95));
		btnPDF.setBounds(15, 617, 200, 60);
		tenisPanel.add(btnPDF);
		
		ajedrezPanel = new JPanel();
		ajedrezPanel.setName("ajedrezPanel");
		ajedrezPanel.setBackground(PrimarioDark);
		panelSliderPrincipal.add(ajedrezPanel, "name_393967728409899");
		ajedrezPanel.setLayout(null);
		
		RSMaterialButtonCircle anteriorRondaAjedrez = new RSMaterialButtonCircle();
		anteriorRondaAjedrez.addActionListener(Controlador.anteriorTablaAjedrez());
		anteriorRondaAjedrez.setText("<");
		anteriorRondaAjedrez.setFont(new Font("Dialog", Font.PLAIN, 18));
		anteriorRondaAjedrez.setBackground(Primario);
		anteriorRondaAjedrez.setBounds(182, 55, 100, 100);
		ajedrezPanel.add(anteriorRondaAjedrez);
		
		RSMaterialButtonCircle siguienteRondaAjedrez = new RSMaterialButtonCircle();
		siguienteRondaAjedrez.addActionListener(Controlador.siguienteTablaAjedrez());
		siguienteRondaAjedrez.setText(">");
		siguienteRondaAjedrez.setFont(new Font("Dialog", Font.PLAIN, 18));
		siguienteRondaAjedrez.setBackground(Primario);
		siguienteRondaAjedrez.setBounds(474, 55, 100, 100);
		ajedrezPanel.add(siguienteRondaAjedrez);
		
		faseX = new JLabel("FASE X");
		faseX.setForeground(Color.WHITE);
		faseX.setFont(new Font("Tahoma", Font.PLAIN, 18));
		faseX.setBounds(326, 94, 69, 20);
		ajedrezPanel.add(faseX);
		
		deY = new JLabel("DE Y");
		deY.setForeground(Color.WHITE);
		deY.setFont(new Font("Tahoma", Font.PLAIN, 18));
		deY.setBounds(326, 118, 69, 20);
		ajedrezPanel.add(deY);
		
		barraAjedrez = new JProgressBar();
		barraAjedrez.setValue(4);
		barraAjedrez.setStringPainted(true);
		barraAjedrez.setMaximum(5);
		barraAjedrez.setFont(new Font("Tahoma", Font.PLAIN, 18));
		barraAjedrez.setBackground(Primario);
		barraAjedrez.setBounds(298, 139, 146, 24);
		ajedrezPanel.add(barraAjedrez);
		
		RSMaterialButtonRectangle btnGuardarAjedrez = new RSMaterialButtonRectangle();
		btnGuardarAjedrez.addActionListener(Controlador.actualizarPartidaDeAjedrez());
		btnGuardarAjedrez.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnGuardarAjedrez.setText("Guardar");
		btnGuardarAjedrez.setBackground(PrimarioLight);
		btnGuardarAjedrez.setBounds(15, 693, 200, 60);
		ajedrezPanel.add(btnGuardarAjedrez);
		
		RSButtonMetro btnGenerarPartidas = new RSButtonMetro();
		btnGenerarPartidas.addActionListener(Controlador.generarPartidasAjedrez());
		btnGenerarPartidas.setBackground(Primario);
		btnGenerarPartidas.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnGenerarPartidas.setText("GENERAR PARTIDAS");
		btnGenerarPartidas.setBounds(576, 593, 189, 75);
		ajedrezPanel.add(btnGenerarPartidas);
		
		JLabel lblClasificacionesAjedrez = new JLabel("Clasificaciones ajedrez");
		lblClasificacionesAjedrez.setForeground(Color.WHITE);
		lblClasificacionesAjedrez.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblClasificacionesAjedrez.setBounds(15, 16, 267, 20);
		ajedrezPanel.add(lblClasificacionesAjedrez);
		
		RSButtonMetro btnVerJugadoresAjedrez = new RSButtonMetro();
		btnVerJugadoresAjedrez.setText("Ver jugadores");
		btnVerJugadoresAjedrez.setGrosorLinea(10);
		btnVerJugadoresAjedrez.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnVerJugadoresAjedrez.setBackground(Primario);
		btnVerJugadoresAjedrez.setBounds(576, 668, 189, 85);
		btnVerJugadoresAjedrez.addActionListener(Controlador.abrirFrameJugador(2));
		ajedrezPanel.add(btnVerJugadoresAjedrez);
		
		JLabel lblPartida = new JLabel("Partida 1");
		lblPartida.setForeground(Color.WHITE);
		lblPartida.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPartida.setBounds(15, 520, 100, 27);
		ajedrezPanel.add(lblPartida);
		
		JLabel lblPartida_1 = new JLabel("Partida 2");
		lblPartida_1.setForeground(Color.WHITE);
		lblPartida_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPartida_1.setBounds(15, 553, 100, 27);
		ajedrezPanel.add(lblPartida_1);
		
		JLabel lblPartida_2 = new JLabel("Partida 3");
		lblPartida_2.setForeground(Color.WHITE);
		lblPartida_2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPartida_2.setBounds(15, 592, 100, 27);
		ajedrezPanel.add(lblPartida_2);
		
		partida1Ajedrez = new RSComboMetro();
		partida1Ajedrez.setFont(new Font("Tahoma", Font.PLAIN, 16));
		partida1Ajedrez.setColorFondo(Primario);
		partida1Ajedrez.setColorBorde(new Color(255, 255, 255));
		partida1Ajedrez.setColorArrow(Primario);
		partida1Ajedrez.setBackground(Primario);
		partida1Ajedrez.setBounds(112, 515, 234, 32);
		ajedrezPanel.add(partida1Ajedrez);
		
		partida2Ajedrez = new RSComboMetro();
		partida2Ajedrez.setFont(new Font("Tahoma", Font.PLAIN, 16));
		partida2Ajedrez.setColorFondo(Primario);
		partida2Ajedrez.setColorBorde(new Color(255, 255, 255));
		partida2Ajedrez.setColorArrow(Primario);
		partida2Ajedrez.setBackground(Primario);
		partida2Ajedrez.setBounds(112, 551, 234, 32);
		ajedrezPanel.add(partida2Ajedrez);
		
		partida3Ajedrez = new RSComboMetro();
		partida3Ajedrez.setFont(new Font("Tahoma", Font.PLAIN, 16));
		partida3Ajedrez.setColorFondo(Primario);
		partida3Ajedrez.setColorBorde(new Color(255, 255, 255));
		partida3Ajedrez.setColorArrow(Primario);
		partida3Ajedrez.setBackground(Primario);
		partida3Ajedrez.setBounds(112, 587, 234, 32);
		ajedrezPanel.add(partida3Ajedrez);
		
		fechaPartida1 = new RSDateChooser();
		fechaPartida1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		fechaPartida1.setColorForeground(Primario);
		fechaPartida1.setColorButtonHover(PrimarioLight);
		fechaPartida1.setColorBackground(Primario);
		fechaPartida1.setPlaceholder("Fecha");
		fechaPartida1.setFuente(new Font("Tahoma", Font.BOLD, 18));
		fechaPartida1.setFormatoFecha("dd/MM/yyyy");
		fechaPartida1.setBounds(361, 515, 200, 32);
		ajedrezPanel.add(fechaPartida1);
		
		fechaPartida2 = new RSDateChooser();
		fechaPartida2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		fechaPartida2.setColorForeground(Primario);
		fechaPartida2.setColorButtonHover(PrimarioLight);
		fechaPartida2.setColorBackground(Primario);
		fechaPartida2.setPlaceholder("Fecha");
		fechaPartida2.setFuente(new Font("Tahoma", Font.BOLD, 18));
		fechaPartida2.setFormatoFecha("dd/MM/yyyy");
		fechaPartida2.setBounds(361, 553, 200, 32);
		ajedrezPanel.add(fechaPartida2);
		
		fechaPartida3 = new RSDateChooser();
		fechaPartida3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		fechaPartida3.setColorForeground(Primario);
		fechaPartida3.setColorButtonHover(PrimarioLight);
		fechaPartida3.setColorBackground(Primario);
		fechaPartida3.setPlaceholder("Fecha");
		fechaPartida3.setFuente(new Font("Tahoma", Font.BOLD, 18));
		fechaPartida3.setFormatoFecha("dd/MM/yyyy");
		fechaPartida3.setBounds(361, 587, 200, 32);
		ajedrezPanel.add(fechaPartida3);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 190, 750, 295);
		ajedrezPanel.add(scrollPane);
		
		tablaAjedrez = new RSTableMetro();
		tablaAjedrez.setAltoHead(30);
		tablaAjedrez.setColorFilasForeground1(Color.DARK_GRAY);
		tablaAjedrez.setColorFilasBackgound2(Color.WHITE);
		tablaAjedrez.setRowMargin(2);
		tablaAjedrez.setRowHeight(25);
		tablaAjedrez.setEditingRow(0);
		tablaAjedrez.setEditingColumn(0);
		tablaAjedrez.setFont(new Font("Tahoma", Font.PLAIN, 30));
		tablaAjedrez.setColorSelForeground(Color.WHITE);
		tablaAjedrez.setColorSelBackgound(Color.DARK_GRAY);
		tablaAjedrez.setColorFilasForeground2(Color.DARK_GRAY);
		tablaAjedrez.setColorFilasBackgound1(Color.WHITE);
		tablaAjedrez.setColorBackgoundHead(Primario);
		tablaAjedrez.setSelectionForeground(new Color(0, 0, 0));
		tablaAjedrez.setSelectionBackground(new Color(0, 255, 0));
		tablaAjedrez.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tablaAjedrez.setMultipleSeleccion(false);
		tablaAjedrez.setFuenteFilas(new Font("Tahoma", Font.PLAIN, 24));
		tablaAjedrez.setFuenteFilasSelect(new Font("Tahoma", Font.PLAIN, 24));
		tablaAjedrez.setFuenteHead(new Font("Tahoma", Font.PLAIN, 16));
		tablaAjedrez.addMouseListener(Controlador.mostrarPartidaSeleccionadaAjedrez());
		scrollPane.setViewportView(tablaAjedrez);
		
		JLabel logoPanelAjedrez = new JLabel("");
		logoPanelAjedrez.setBounds(640, 16, 125, 125);
		Icon iconPanelAjedrez=new ImageIcon(iconoPanelAjedrez.getImage().getScaledInstance(logoPanelAjedrez.getWidth(), logoPanelAjedrez.getHeight(), Image.SCALE_DEFAULT));
		logoPanelAjedrez.setIcon(iconPanelAjedrez);
		ajedrezPanel.add(logoPanelAjedrez);
		
		JPanel FechaHora = new JPanel();
		FechaHora.setBounds(880, 71, 300, 48);
		FechaHora.setBackground(PrimarioLight);
		contentPane.add(FechaHora);
		FechaHora.setLayout(null);
		
		RSLabelHora labelHora = new RSLabelHora();
		labelHora.setBounds(145, 0, 153, 48);
		FechaHora.add(labelHora);
		labelHora.setForeground(PrimarioDark);
		labelHora.setFont(new Font("Dialog", Font.BOLD, 23));
		
		RSLabelFecha labelFecha = new RSLabelFecha();
		labelFecha.setBounds(0, 0, 130, 48);
		FechaHora.add(labelFecha);
		labelFecha.setForeground(PrimarioDark);
		labelFecha.setFont(new Font("Dialog", Font.BOLD, 23));
		
		RSButtonIconD btncndJugadores = new RSButtonIconD();
		btncndJugadores.setIcon(new ImageIcon("resources/iconsBotones/business-card-of-a-man-with-contact-info_b.png"));
		btncndJugadores.setFont(new Font("Tahoma", Font.BOLD, 18));
		btncndJugadores.setBackground(Primario);
		btncndJugadores.setColorHover(PrimarioDark);
		btncndJugadores.addActionListener(Controlador.abrirFrameJugador(4));
		btncndJugadores.setText("Jugadores");
		btncndJugadores.setBounds(400, 71, 270, 48);
		contentPane.add(btncndJugadores);
		
		
		//Movimiento del frame a partir de un JLabel
		JLabel frameFake = new JLabel("");
		frameFake.setForeground(Color.WHITE);
		frameFake.setBackground(Color.WHITE);
		frameFake.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				setLocation(getLocation().x + arg0.getX() - x, getLocation().y + arg0.getY() - y );
			}
		});
		frameFake.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				x=arg0.getX();
				y=arg0.getY();
			}
		});
		frameFake.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		frameFake.setBounds(0, 0, 1200, 68);
		contentPane.add(frameFake);
		
		consola = new JLabel("");
		consola.setFont(new Font("Tahoma", Font.PLAIN, 14));
		consola.setBounds(0, 900, 1200, 20);
		contentPane.add(consola);
	
		Controlador.actualizarFramePrincipal();
	}

	public static RSToggleButtonShade getFutbolSelect() {
		return futbolSelect;
	}

	public static RSToggleButtonShade getAjedrezSelect() {
		return ajedrezSelect;
	}

	public static RSToggleButtonShade getTenisSelect() {
		return tenisSelect;
	}

	public static RSTextFieldShade getNombreAlumno() {
		return nombreAlumno;
	}

	public static RSComboMetro getCursoAlumno() {
		return cursoAlumno;
	}

	public static JEditorPane getObservaciones() {
		return observaciones;
	}

	public static RSToggleButtonShade getSelectLunes() {
		return selectLunes;
	}

	public static RSToggleButtonShade getSelectMartes() {
		return selectMartes;
	}

	public static RSToggleButtonShade getSelectMiercoles() {
		return selectMiercoles;
	}

	public static RSToggleButtonShade getSelectJueves() {
		return selectJueves;
	}

	public static RSToggleButtonShade getSelectViernes() {
		return selectViernes;
	}

	public static RSButtonIconD getLunes() {
		return lunes;
	}

	public static RSButtonIconD getMartes() {
		return martes;
	}

	public static RSButtonIconD getMiercoles() {
		return miercoles;
	}

	public static RSButtonIconD getJueves() {
		return jueves;
	}

	public static RSButtonIconD getViernes() {
		return viernes;
	}

	public static RSFormatFieldShade getUsuario() {
		return usuario;
	}

	public static RSPassFieldShade getContrasenna() {
		return contrasenna;
	}

	public static RSFormatFieldShade getPaginaFav() {
		return paginaFav;
	}

	public static JList<String> getListA() {
		return listA;
	}

	public static JList<String> getListB() {
		return listB;
	}

	public static JList<String> getListC() {
		return listC;
	}

	public static JList<String> getListD() {
		return listD;
	}

	public static JList<String> getListE() {
		return listE;
	}

	public static JList<String> getListF() {
		return listF;
	}

	public static RSComboMetro getComboGrupoA() {
		return comboGrupoA;
	}

	public static RSComboMetro getComboGrupoB() {
		return comboGrupoB;
	}

	public static RSComboMetro getComboGrupoC() {
		return comboGrupoC;
	}

	public static RSComboMetro getComboGrupoD() {
		return comboGrupoD;
	}

	public static RSComboMetro getComboGrupoE() {
		return comboGrupoE;
	}

	public static RSComboMetro getComboGrupoF() {
		return comboGrupoF;
	}

	public static Color getComplemento() {
		return Complemento;
	}

	public static void setComplemento(Color complemento) {
		Complemento = complemento;
	}

	public static Color getComplementoOscuro() {
		return ComplementoDark;
	}

	public static void setComplementoOscuro(Color complementoOscuro) {
		ComplementoDark = complementoOscuro;
	}

	public static Color getPrimarioLight() {
		return PrimarioLight;
	}

	public static void setPrimarioLight(Color primarioLight) {
		PrimarioLight = primarioLight;
	}

	public static Color getPrimario() {
		return Primario;
	}

	public static void setPrimario(Color primario) {
		Primario = primario;
	}

	public static Color getPrimarioDark() {
		return PrimarioDark;
	}

	public static void setPrimarioDark(Color primarioDark) {
		PrimarioDark = primarioDark;
	}

	public static RSTableMetro getTablaParatidasTenis() {
		return tablaParatidasTenis;
	}

	public static JLabel getLblFaseX() {
		return faseXTenis;
	}

	public static RSDateChooser getFechaTenis() {
		return fechaTenis;
	}

	public static RSMTextFull getJugadorTenisVisitante() {
		return jugadorTenisVisitante;
	}

	public static RSMTextFull getJugadorTenisLocal() {
		return jugadorTenisLocal;
	}

	public static JTextField getEquipo1() {
		return equipo1;
	}

	public static JTextField getEquipo2() {
		return equipo2;
	}

	public static RSComboMetro getPartida1Ajedrez() {
		return partida1Ajedrez;
	}

	public static RSComboMetro getPartida2Ajedrez() {
		return partida2Ajedrez;
	}

	public static RSComboMetro getPartida3Ajedrez() {
		return partida3Ajedrez;
	}

	public static RSDateChooser getFechaPartida1() {
		return fechaPartida1;
	}

	public static RSDateChooser getFechaPartida2() {
		return fechaPartida2;
	}

	public static RSDateChooser getFechaPartida3() {
		return fechaPartida3;
	}

	public static JProgressBar getBarraAjedrez() {
		return barraAjedrez;
	}

	public static JLabel getFaseX() {
		return faseX;
	}

	public static JLabel getDeY() {
		return deY;
	}

	public static RSComboMetro getP1Local() {
		return p1Local;
	}

	public static RSComboMetro getP2Local() {
		return p2Local;
	}

	public static RSComboMetro getP1Visitante() {
		return p1Visitante;
	}

	public static RSTextFieldShade getP1GolesLocal() {
		return p1GolesLocal;
	}

	public static RSTextFieldShade getP1GolesVisitante() {
		return p1GolesVisitante;
	}

	public static RSTextFieldShade getP2GolesLocal() {
		return p2GolesLocal;
	}

	public static RSTextFieldShade getP2GolesVisitante() {
		return p2GolesVisitante;
	}

	public static RSDateChooser getP2Fecha() {
		return p2Fecha;
	}

	public static RSDateChooser getP1Fecha() {
		return p1Fecha;
	}

	public static RSComboMetro getP2Visitante() {
		return p2Visitante;
	}

	public static RSComboMetro getP3Local() {
		return p3Local;
	}

	public static RSDateChooser getP3Fecha() {
		return p3Fecha;
	}

	public static RSComboMetro getP3Visitante() {
		return p3Visitante;
	}

	public static RSComboMetro getP4Local() {
		return p4Local;
	}

	public static RSTextFieldShade getP3GolesLocal() {
		return p3GolesLocal;
	}

	public static RSTextFieldShade getP3GolesVisitante() {
		return p3GolesVisitante;
	}

	public static RSComboMetro getP4Visitante() {
		return p4Visitante;
	}

	public static RSTextFieldShade getP4GolesLocal() {
		return p4GolesLocal;
	}

	public static RSTextFieldShade getP4GolesVisitante() {
		return p4GolesVisitante;
	}

	public static JTable getTablaAjedrez() {
		return tablaAjedrez;
	}


	public static JProgressBar getBarraFasesTenis() {
		return barraFasesTenis;
	}

	public static JRadioButton getGanadorJ1() {
		return ganadorJ1;
	}

	public static RSPassFieldShade getContrasennaNueva() {
		return contrasennaNueva;
	}

	public static JRadioButton getGanadorJ2() {
		return ganadorJ2;
	}
	
	
	public static Color getComplementoDark() {
		return ComplementoDark;
	}

	public static RSDateChooser getP4Fecha() {
		return p4Fecha;
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}

package Vista;

import java.awt.Color;

import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import Controlador.Controlador;
import necesario.RSAnimation;

import javax.swing.JList;
import rojeru_san.RSButtonRiple;
import rscomponentshade.RSFormatFieldShade;
import rojerusan.RSComboMetro;
import rojeru_san.RSMTextFull;
import javax.swing.JRadioButton;
import rojerusan.RSButtonIconD;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import rojerusan.RSMaterialButtonRectangle;


public class Equipos extends JFrame {

	int x, y;
	private JPanel contentPane;
	private static Color PrimarioLight=Principal.getPrimarioLight();
	private static Color Primario=Principal.getPrimario();
	private static Color PrimarioDark=Principal.getPrimarioDark();
	private static Color Complemento=Principal.getComplemento();
	private static Color ComplementoOscuro=Principal.getComplementoOscuro();

	private static JList listaEquipo;
	private static RSFormatFieldShade nombreEquipo;
	private static RSComboMetro jugador3;
	private static RSComboMetro jugador4;
	private static RSComboMetro jugador5;
	private static RSComboMetro jugador6;
	private static RSComboMetro jugador7;
	private static JEditorPane observacionesEquipo;
	private JLabel label;
	private static RSComboMetro jugador2;
	private static RSComboMetro jugador1;
	private static RSComboMetro tagCurso;
	private static RSMaterialButtonRectangle btnAgregar;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Equipos frame = new Equipos();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Equipos() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("resources/iconsApp/footballApp.png"));
		setTitle("Gestionar equipos");
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1100, 600);
		contentPane = new JPanel();
		contentPane.setBackground(PrimarioDark);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//Boton X salir
		JButton btnX = new JButton("X");
		btnX.setBackground(Primario);
		btnX.setBorderPainted(false);
		btnX.setBorder(null);
		btnX.setForeground(PrimarioDark);
		btnX.setFont(new Font("Tahoma", Font.BOLD, 50));
		btnX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
			}
		});
		btnX.setBounds(1050, 0, 50, 50);
		contentPane.add(btnX);
		
		listaEquipo = new JList<String>();
		listaEquipo.addMouseListener(Controlador.mostrarEquipo());
		listaEquipo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		listaEquipo.setBackground(Primario);
		listaEquipo.setForeground(new Color(255, 255, 255));
		listaEquipo.setBounds(15, 49, 413, 486);
		contentPane.add(listaEquipo);
		
		nombreEquipo = new RSFormatFieldShade();
		nombreEquipo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		nombreEquipo.setPlaceholder("Nombre de equipo");
		nombreEquipo.setBounds(443, 47, 382, 45);
		contentPane.add(nombreEquipo);
		
		jugador1 = new RSComboMetro();
		jugador1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jugador1.setColorBorde(Color.WHITE);
		jugador1.setColorFondo(Primario);
		jugador1.setColorArrow(Primario);
		jugador1.setBounds(443, 93, 382, 40);
		contentPane.add(jugador1);
		
		jugador2 = new RSComboMetro();
		jugador2.setColorBorde(new Color(255, 255, 255));
		jugador2.setColorFondo(Primario);
		jugador2.setColorArrow(Primario);
		jugador2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jugador2.setBounds(443, 138, 382, 40);
		contentPane.add(jugador2);
		
		jugador3 = new RSComboMetro();
		jugador3.setColorBorde(new Color(255, 255, 255));
		jugador3.setColorFondo(Primario);
		jugador3.setColorArrow(Primario);
		jugador3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jugador3.setBounds(443, 182, 382, 40);
		contentPane.add(jugador3);
		
		jugador4 = new RSComboMetro();
		jugador4.setColorBorde(new Color(255, 255, 255));
		jugador4.setColorFondo(Primario);
		jugador4.setColorArrow(Primario);
		jugador4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jugador4.setBounds(443, 224, 382, 40);
		contentPane.add(jugador4);
		
		jugador5 = new RSComboMetro();
		jugador5.setColorBorde(new Color(255, 255, 255));
		jugador5.setColorFondo(Primario);
		jugador5.setColorArrow(Primario);
		jugador5.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jugador5.setBounds(443, 267, 382, 40);
		contentPane.add(jugador5);
		
		jugador6 = new RSComboMetro();
		jugador6.setColorBorde(new Color(255, 255, 255));
		jugador6.setColorFondo(Primario);
		jugador6.setColorArrow(Primario);
		jugador6.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jugador6.setBounds(443, 310, 382, 40);
		contentPane.add(jugador6);
		
		jugador7 = new RSComboMetro();
		jugador7.setColorBorde(new Color(255, 255, 255));
		jugador7.setColorFondo(Primario);
		jugador7.setColorArrow(Primario);
		jugador7.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jugador7.setBounds(443, 354, 382, 40);
		contentPane.add(jugador7);
		
		observacionesEquipo = new JEditorPane();
		observacionesEquipo.setText("Observaciones");
		observacionesEquipo.setBounds(443, 444, 382, 140);
		observacionesEquipo.setSelectedTextColor(new Color(255, 255, 240));
		observacionesEquipo.setSelectionColor(new Color(50, 205, 50));
		observacionesEquipo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		observacionesEquipo.setForeground(new Color(255, 255, 255));
		observacionesEquipo.setBackground(Primario);
		observacionesEquipo.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		observacionesEquipo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (observacionesEquipo.getText().toString().equals("Observaciones")) {
					observacionesEquipo.setText("");
				}
			}
		});
		observacionesEquipo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				int numeroCaracteres=observacionesEquipo.getText().toString().length()+1;
				label.setText(""+numeroCaracteres);
			}
		});	
		
		tagCurso = new RSComboMetro();
		tagCurso.setModel(new DefaultComboBoxModel(new String[] {"Seleccione tag de curso", "ESO", "FP"}));
		tagCurso.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tagCurso.setColorFondo(Primario);
		tagCurso.setColorBorde(Color.WHITE);
		tagCurso.setColorArrow(Primario);
		tagCurso.setBounds(443, 398, 382, 40);
		contentPane.add(tagCurso);
		contentPane.add(observacionesEquipo);
		
		label = new JLabel("0");
		label.setBounds(840, 564, 30, 20);
		contentPane.add(label);
		
		JLabel iconoEquipo = new JLabel("");
		iconoEquipo.setBounds(885, 64, 200, 200);
		contentPane.add(iconoEquipo);
		
		
		//Movimiento del frame a partir de un JLabel
		JLabel frameFake = new JLabel("");
		frameFake.setForeground(Color.WHITE);
		frameFake.setBackground(Color.WHITE);
		frameFake.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				setLocation(getLocation().x + arg0.getX() - x, getLocation().y + arg0.getY() - y );
			}
		});
		frameFake.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				x=arg0.getX();
				y=arg0.getY();
			}
		});
		
		frameFake.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		frameFake.setBounds(0, 0, 1053, 50);
		contentPane.add(frameFake);
		
		btnAgregar = new RSMaterialButtonRectangle();
		btnAgregar.addActionListener(Controlador.annadirJugadorAlEquipo());
		btnAgregar.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnAgregar.setText("Insertar");
		btnAgregar.setBounds(885, 405, 200, 60);
		btnAgregar.setBackground(PrimarioLight);
		contentPane.add(btnAgregar);
		
		RSMaterialButtonRectangle btnGuardar = new RSMaterialButtonRectangle();
		btnGuardar.addActionListener(Controlador.actualizarEquipoJugadores());
		btnGuardar.setText("GUARDAR");
		btnGuardar.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnGuardar.setBounds(885, 524, 200, 60);
		btnGuardar.setBackground(PrimarioLight);
		contentPane.add(btnGuardar);
		
		RSMaterialButtonRectangle btnEliminar = new RSMaterialButtonRectangle();
		btnEliminar.addActionListener(Controlador.eliminarEquipo());
		btnEliminar.setText("ELIMINAR");
		btnEliminar.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnEliminar.setBounds(885, 465, 200, 60);
		btnEliminar.setBackground(PrimarioLight);
		contentPane.add(btnEliminar);
		
		Controlador.actualizarFrameEquipo();
	}

	public static JList getListEquipo() {
		return listaEquipo;
	}

	public static RSFormatFieldShade getNombreEquipo() {
		return nombreEquipo;
	}

	public static RSComboMetro getJ1() {
		return jugador1;
	}

	public static RSComboMetro getJ2() {
		return jugador2;
	}

	public static RSComboMetro getJ3() {
		return jugador3;
	}

	public static RSComboMetro getJ4() {
		return jugador4;
	}

	public static RSComboMetro getJ5() {
		return jugador5;
	}

	public static RSComboMetro getJ6() {
		return jugador6;
	}

	public static RSComboMetro getJ7() {
		return jugador7;
	}

	public static JEditorPane getObservacionesEquipo() {
		return observacionesEquipo;
	}

	public static RSComboMetro getTagCurso() {
		return tagCurso;
	}

	public static RSMaterialButtonRectangle getBtnAgregar() {
		return btnAgregar;
	}
}

package Vista;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.JList;
import rscomponentshade.RSFormatFieldShade;
import rojerusan.RSComboMetro;

import javax.swing.JEditorPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import rojeru_san.RSMTextFull;

import rscomponentshade.RSToggleButtonShade;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.ComponentOrientation;

import rojerusan.RSTableMetro;
import javax.swing.table.DefaultTableModel;

import Controlador.Controlador;

import javax.swing.ListSelectionModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import rojerusan.RSMaterialButtonRectangle;

public class Jugadores extends JFrame {

	int x, y;
	private JPanel contentPane;
	private static Color PrimarioLight=Principal.getPrimarioLight();
	private static Color Primario=Principal.getPrimario();
	private static Color PrimarioDark=Principal.getPrimarioDark();
	private static Color Complemento=Principal.getComplemento();
	private static Color ComplementoOscuro=Principal.getComplementoOscuro();
	private static RSComboMetro comboCurso;
	private static RSToggleButtonShade futbolSelect;
	private static JEditorPane Observaciones;

	private static RSFormatFieldShade nombreJugador;
	private static RSTableMetro tableMetro;
	private static JList<String> listJugadores;
	private static RSMTextFull buscadorJugadores;
	private static JSpinner rondaTenis;
	private static JSpinner rondaAjedrez;
	private static RSToggleButtonShade tenisSelect;
	private static RSToggleButtonShade ajedrezSelect;
	private static RSMaterialButtonRectangle btnInsertar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Jugadores frame = new Jugadores();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Jugadores(int i) {
		setIconImage(Toolkit.getDefaultToolkit().getImage("resources/iconsApp/football-jerseyApp.png"));
		setTitle("Gesti�n de jugadores");
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1000, 600);
		contentPane = new JPanel();
		contentPane.setBackground(PrimarioDark);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//Boton X salir
		JButton btnX = new JButton("X");
		btnX.setBackground(Primario);
		btnX.setBorderPainted(false);
		btnX.setBorder(null);
		btnX.setForeground(PrimarioDark);
		btnX.setFont(new Font("Tahoma", Font.BOLD, 50));
		btnX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();			}
		});
		btnX.setBounds(950, 0, 50, 50);
		contentPane.add(btnX);
		
		nombreJugador = new RSFormatFieldShade();
		nombreJugador.setBackground(Color.WHITE);
		nombreJugador.setFont(new Font("Tahoma", Font.PLAIN, 18));
		nombreJugador.setPlaceholder("Nombre del jugador");
		nombreJugador.setBounds(494, 49, 281, 45);
		contentPane.add(nombreJugador);
		
		comboCurso = new RSComboMetro();
		comboCurso.setModel(new DefaultComboBoxModel(new String[] {"Seleccione curso", "ESO 1\u00BA", "ESO 2\u00BA", "ESO 3\u00BA", "ESO 4\u00BA", "FPB\u00E1sico 1\u00BA", "FPB\u00E1sico 2\u00BA", "GMedio 1\u00BA", "GMedio 2\u00BA", "GSuperior 1\u00BA", "GSuperior 2\u00BA"}));
		comboCurso.setFont(new Font("Tahoma", Font.BOLD, 18));
		comboCurso.setColorFondo(Primario);
		comboCurso.setColorBorde(Color.WHITE);
		comboCurso.setColorArrow(Primario);
		comboCurso.setBounds(494, 110, 281, 42);
		contentPane.add(comboCurso);
		
		futbolSelect = new RSToggleButtonShade();
		futbolSelect.setBgShadeHover(Color.WHITE);
		futbolSelect.bgHover = new Color(100, 145, 95);
		futbolSelect.setBgHover(PrimarioLight);
		futbolSelect.setBackground(Primario);
		futbolSelect.setBounds(494, 226, 80, 80);
		ImageIcon selectFut=new ImageIcon("resources/iconsBotones/soccer-player-motion.png");
		Icon iselectFut=new ImageIcon(selectFut.getImage().getScaledInstance(futbolSelect.getWidth(), futbolSelect.getHeight(), Image.SCALE_DEFAULT));
		futbolSelect.setIcon(iselectFut);
		contentPane.add(futbolSelect);
		
		tenisSelect = new RSToggleButtonShade();
		tenisSelect.setBgShadeHover(Color.WHITE);
		tenisSelect.bgHover = new Color(100, 145, 95);
		tenisSelect.setBgHover(PrimarioLight);
		tenisSelect.setBackground(Primario);
		tenisSelect.setBounds(595, 226, 80, 80);
		ImageIcon selectTenis=new ImageIcon("resources/iconsBotones/man-playing-ping-pong.png");
		Icon iselectTenis=new ImageIcon(selectTenis.getImage().getScaledInstance(tenisSelect.getWidth(), tenisSelect.getHeight(), Image.SCALE_DEFAULT));
		tenisSelect.setIcon(iselectTenis);
		contentPane.add(tenisSelect);
		
		ajedrezSelect = new RSToggleButtonShade();
		ajedrezSelect.setBgShadeHover(Color.WHITE);
		ajedrezSelect.bgHover = new Color(100, 145, 95);
		ajedrezSelect.setBgHover(PrimarioLight);
		ajedrezSelect.setBackground(Primario);
		ajedrezSelect.setBounds(695, 226, 80, 80);
		ImageIcon selectAje=new ImageIcon("resources/iconsBotones/strategy.png");
		Icon iselectAje=new ImageIcon(selectAje.getImage().getScaledInstance(ajedrezSelect.getWidth(), ajedrezSelect.getHeight(), Image.SCALE_DEFAULT));
		ajedrezSelect.setIcon(iselectAje);
		contentPane.add(ajedrezSelect);
		
		rondaTenis = new JSpinner();
		rondaTenis.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		rondaTenis.setBackground(new Color(255, 255, 255));
		rondaTenis.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		rondaTenis.setFont(new Font("Tahoma", Font.BOLD, 18));
		rondaTenis.setBounds(577, 340, 40, 40);
		contentPane.add(rondaTenis);
		
		rondaAjedrez = new JSpinner();
		rondaAjedrez.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		rondaAjedrez.setForeground(new Color(0, 128, 0));
		rondaAjedrez.setBackground(new Color(255, 255, 255));
		rondaAjedrez.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		rondaAjedrez.setFont(new Font("Tahoma", Font.BOLD, 18));
		rondaAjedrez.setBounds(735, 340, 40, 40);
		contentPane.add(rondaAjedrez);
		
		JEditorPane dtrpnRondaTenis = new JEditorPane();
		dtrpnRondaTenis.setForeground(Color.WHITE);
		dtrpnRondaTenis.setOpaque(false);
		dtrpnRondaTenis.setFont(new Font("Tahoma", Font.PLAIN, 18));
		dtrpnRondaTenis.setEditable(false);
		dtrpnRondaTenis.setText("Ronda \r\ntenis");
		dtrpnRondaTenis.setBounds(494, 330, 68, 50);
		contentPane.add(dtrpnRondaTenis);
		
		JEditorPane dtrpnRondaAjedrez = new JEditorPane();
		dtrpnRondaAjedrez.setForeground(Color.WHITE);
		dtrpnRondaAjedrez.setOpaque(false);
		dtrpnRondaAjedrez.setFont(new Font("Tahoma", Font.PLAIN, 18));
		dtrpnRondaAjedrez.setText("Ronda\r\najedrez");
		dtrpnRondaAjedrez.setEditable(false);
		dtrpnRondaAjedrez.setBounds(652, 330, 68, 50);
		contentPane.add(dtrpnRondaAjedrez);
		
		JLabel label = new JLabel("0");
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label.setBounds(745, 562, 30, 20);
		contentPane.add(label);
		
		Observaciones = new JEditorPane();
		Observaciones.setText("Observaciones");
		Observaciones.setBounds(494, 396, 250, 186);
		Observaciones.setSelectedTextColor(new Color(255, 255, 240));
		Observaciones.setSelectionColor(new Color(50, 205, 50));
		Observaciones.setFont(new Font("Tahoma", Font.PLAIN, 18));
		Observaciones.setForeground(new Color(255, 255, 255));
		Observaciones.setBackground(Primario);
		Observaciones.setBorder(new LineBorder(new Color(255, 255, 255), 2));
		Observaciones.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (Observaciones.getText().toString().equals("Observaciones")) {
					Observaciones.setText("");
				}
			}
		});
		Observaciones.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				int numeroCaracteres=Observaciones.getText().toString().length()+1;
				label.setText(""+numeroCaracteres);
			}
		});
		contentPane.add(Observaciones);
		
		buscadorJugadores = new RSMTextFull();
		buscadorJugadores.addKeyListener(Controlador.buscadorJugadores());
		buscadorJugadores.setSoloLetras(true);
		buscadorJugadores.setSelectionColor(new Color(0, 128, 128));
		buscadorJugadores.setPlaceholder("Buscador");
		buscadorJugadores.setFont(new Font("Dialog", Font.BOLD, 18));
		buscadorJugadores.setForeground(Primario);
		buscadorJugadores.setBotonColor(new Color(0, 100, 0));
		buscadorJugadores.setBordeColorFocus(new Color(0, 128, 0));
		buscadorJugadores.setBounds(15, 542, 463, 40);
		contentPane.add(buscadorJugadores);
		
		listJugadores = new JList<String>();
		listJugadores.addMouseListener(Controlador.mostrarJugadorSeleccionado(i));
		listJugadores.setFont(new Font("Tahoma", Font.PLAIN, 18));
		listJugadores.setForeground(new Color(255, 255, 255));
		listJugadores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listJugadores.setBackground(Primario);
		listJugadores.setBounds(15, 49, 463, 486);
		contentPane.add(listJugadores);
		
		//Movimiento de la ventana a partir de un JLabel
		JLabel frameFake = new JLabel("");
		frameFake.setForeground(Color.WHITE);
		frameFake.setBackground(Color.WHITE);
		frameFake.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				setLocation(getLocation().x + arg0.getX() - x, getLocation().y + arg0.getY() - y );
			}
		});
		frameFake.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				x=arg0.getX();
				y=arg0.getY();
			}
		});
		frameFake.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		frameFake.setBounds(0, 0, 968, 50);
		contentPane.add(frameFake);
		
		tableMetro = new RSTableMetro();
		tableMetro.setBounds(15, 49, 463, 486);
		contentPane.add(tableMetro);
		
		RSMaterialButtonRectangle btnGuardar = new RSMaterialButtonRectangle();
		btnGuardar.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnGuardar.addActionListener(Controlador.actualizarJugador(i));
		btnGuardar.setText("Guardar");
		btnGuardar.setBounds(778, 522, 200, 60);
		btnGuardar.setBackground(PrimarioLight);
		contentPane.add(btnGuardar);
		
		RSMaterialButtonRectangle btnEliminar = new RSMaterialButtonRectangle();
		btnEliminar.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnEliminar.addActionListener(Controlador.eliminarJugador(i));
		btnEliminar.setText("ELiminar");
		btnEliminar.setBounds(778, 463, 200, 60);
		btnEliminar.setBackground(PrimarioLight);
		contentPane.add(btnEliminar);
		
		btnInsertar = new RSMaterialButtonRectangle();
		btnInsertar.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnInsertar.addActionListener(Controlador.annadirJugador(i));
		btnInsertar.setText("Insertar");
		btnInsertar.setBounds(778, 403, 200, 60);
		btnInsertar.setBackground(PrimarioLight);
		contentPane.add(btnInsertar);
		
		//ACTUALIZAMOS EL FRAME SEGUN LA OPCION ENTRANTE
		switch (i) {
		case 1:
			Controlador.actualizarFrameJugadores(i);
			break;
		case 2:
			Controlador.actualizarFrameJugadores(i);
			break;
		case 3:
			Controlador.actualizarFrameJugadores(i);
			break;
		default:
			Controlador.actualizarFrameJugadores(i);
			break;
		}
	}

	//CONSTRUCTOR VACIO
	public Jugadores() {
		// TODO Auto-generated constructor stub
	}

	public Color getComplemento() {
		return Complemento;
	}

	public static RSToggleButtonShade getFutbolSelect() {
		return futbolSelect;
	}

	public static JEditorPane getObservaciones() {
		return Observaciones;
	}

	public static RSFormatFieldShade getNombreJugador() {
		return nombreJugador;
	}

	public static RSTableMetro getTableMetro() {
		return tableMetro;
	}

	public static JList getListJugadores() {
		return listJugadores;
	}

	public static RSMTextFull getBuscadorJugadores() {
		return buscadorJugadores;
	}

	public static JSpinner getSpinner() {
		return rondaTenis;
	}

	public static JSpinner getSpinner_1() {
		return rondaAjedrez;
	}

	public static RSToggleButtonShade getTenisSelect() {
		return tenisSelect;
	}

	public static RSToggleButtonShade getAjedrezSelect() {
		return ajedrezSelect;
	}

	public static RSComboMetro getComboCurso() {
		return comboCurso;
	}

	public static JSpinner getRondaTenis() {
		return rondaTenis;
	}

	public static JSpinner getRondaAjedrez() {
		return rondaAjedrez;
	}

	public static RSMaterialButtonRectangle getBtnInsertar() {
		return btnInsertar;
	}
	
	
	
}

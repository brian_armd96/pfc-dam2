package Vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import rojerusan.RSTableMetro;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import rojerusan.RSComboMetro;
import rojeru_san.RSMTextFull;
import rsbuttongradiente.RSButtonGradiente;
import rscomponentshade.RSButtonShade;
import org.jdesktop.swing.animation.rendering.JRendererPanel;
import rojerusan.RSPanelImage;
import rojeru_san.componentes.RSDateChooser;
import rscomponentshade.RSTextFieldShade;
import javax.swing.JEditorPane;
import javax.swing.SwingConstants;

public class Partidos extends JFrame {

	int x, y;
	private JPanel contentPane;
	private static Color PrimarioLight=Principal.getPrimarioLight();
	private static Color Primario=Principal.getPrimario();
	private static Color PrimarioDark=Principal.getPrimarioDark();
	private static Color Complemento=Principal.getComplemento();
	private static Color ComplementoOscuro=Principal.getComplementoOscuro();
	private RSMTextFull buscadorPorEquipo;
	private RSComboMetro equipoLocal;
	private JEditorPane observaciones;
	private RSTextFieldShade golesLocal;
	private RSDateChooser fechaPartido;
	private RSTextFieldShade golesVisitante;
	private RSComboMetro equipoVisitante;
	private RSTableMetro tableMetro;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Partidos frame = new Partidos();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Partidos() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("resources/iconsApp/goal.png"));
		setTitle("Partidos de f�tbol");
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1000, 600);
		contentPane = new JPanel();
		contentPane.setBackground(PrimarioDark);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//Boton X salir
		JButton btnX = new JButton("X");
		btnX.setBackground(Primario);
		btnX.setBorderPainted(false);
		btnX.setBorder(null);
		btnX.setForeground(PrimarioDark);
		btnX.setFont(new Font("Tahoma", Font.BOLD, 50));
		btnX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();			
				}
		});
		btnX.setBounds(950, 0, 50, 50);
		contentPane.add(btnX);
		//Movimiento del frame a partir de un JLabel
		JLabel frameFake = new JLabel("");
		frameFake.setForeground(Color.WHITE);
		frameFake.setBackground(Color.WHITE);
		frameFake.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				setLocation(getLocation().x + arg0.getX() - x, getLocation().y + arg0.getY() - y );
			}
		});
		frameFake.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				x=arg0.getX();
				y=arg0.getY();
			}
		});
		
		frameFake.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		frameFake.setBounds(0, 0, 800, 50);
		contentPane.add(frameFake);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 62, 970, 286);
		contentPane.add(scrollPane);
		
		tableMetro = new RSTableMetro();
		scrollPane.setViewportView(tableMetro);
		
		equipoLocal = new RSComboMetro();
		equipoLocal.setBounds(15, 364, 300, 32);
		contentPane.add(equipoLocal);
		
		equipoVisitante = new RSComboMetro();
		equipoVisitante.setBounds(685, 364, 300, 32);
		contentPane.add(equipoVisitante);
		
		buscadorPorEquipo = new RSMTextFull();
		buscadorPorEquipo.setOpaque(false);
		buscadorPorEquipo.setSoloLetras(true);
		buscadorPorEquipo.setFont(new Font("Dialog", Font.BOLD, 16));
		buscadorPorEquipo.setPlaceholder("Buscardor");
		buscadorPorEquipo.setBounds(15, 542, 284, 42);
		contentPane.add(buscadorPorEquipo);
		
		fechaPartido = new RSDateChooser();
		fechaPartido.setFuente(new Font("Tahoma", Font.BOLD, 16));
		fechaPartido.setBounds(377, 360, 240, 40);
		contentPane.add(fechaPartido);
		
		golesLocal = new RSTextFieldShade();
		golesLocal.setFont(new Font("Tahoma", Font.BOLD, 18));
		golesLocal.setPlaceholder("");
		golesLocal.setHorizontalAlignment(SwingConstants.CENTER);
		golesLocal.setText("0");
		golesLocal.setBounds(318, 354, 50, 50);
		contentPane.add(golesLocal);
		
		golesVisitante = new RSTextFieldShade();
		golesVisitante.setFont(new Font("Tahoma", Font.BOLD, 18));
		golesVisitante.setPlaceholder("");
		golesVisitante.setHorizontalAlignment(SwingConstants.CENTER);
		golesVisitante.setText("0");
		golesVisitante.setBounds(632, 354, 50, 50);
		contentPane.add(golesVisitante);
		
		observaciones = new JEditorPane();
		observaciones.setText("Observaciones");
		observaciones.setBounds(318, 412, 364, 172);
		contentPane.add(observaciones);
		
	}
}

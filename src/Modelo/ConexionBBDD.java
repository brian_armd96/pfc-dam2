package Modelo;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class ConexionBBDD {	
	private static String barra=File.separator;
	private static String rutaBD=System.getProperty("user.dir")+barra+"GestorDeTorneosBBDD";
	
	private static void crearBBDD() {
		Connection conn = null;
		File direccionBD=new File(rutaBD);
		
		if (direccionBD.exists()) {
			//JOptionPane.showMessageDialog(null, "Base de datos ya existente");
		}else {
			insertarTablas();
		}
	}
	
	public static void insertarTablas() {
		Connection conn=conectarBD();
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			conn=DriverManager.getConnection("jdbc:derby:"+rutaBD+";create=true");
			//Se crean las tablas
			String[] querys= {"CREATE TABLE grupo_futbol (\r\n" + 
					"  id_grupo_futbol INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),\r\n" + 
					"  tag_curso VARCHAR(45),\r\n" + 
					"  ronda INT,\r\n" + 
					"  nombre_grupo VARCHAR(3),\r\n" + 
					"  CONSTRAINT pk_grupo_futbol PRIMARY KEY (id_grupo_futbol)\r\n" + 
					"  )", "CREATE TABLE equipo (\r\n" + 
							"  id_equipo INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),\r\n" + 
							"  nombre_equipo VARCHAR(50) NOT NULL,\r\n" + 
							"  tag_curso VARCHAR(45),\r\n" + 
							"  ronda INT,\r\n" + 
							"  observaciones VARCHAR(128),\r\n" + 
							"  puntos INT,\r\n" + 
							"  id_grupo INT REFERENCES grupo_futbol (id_grupo_futbol) ON DELETE CASCADE,\r\n" + 
							"  CONSTRAINT pk_equipo PRIMARY KEY (id_equipo)\r\n" + 
							"  )", "CREATE TABLE jugador (\r\n" + 
									"  id_jugador INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),\r\n" + 
									"  nombre VARCHAR(45) NOT NULL,\r\n" + 
									"  curso VARCHAR(45) NOT NULL,\r\n" + 
									"  competicion VARCHAR(3),\r\n" + 
									"  ronda_ajedrez INT,\r\n" + 
									"  ronda_tenis INT,\r\n" + 
									"  observaciones VARCHAR(128),\r\n" + 
									"  id_equipo INT REFERENCES equipo (id_equipo) ON DELETE CASCADE,\r\n" + 
									"  CONSTRAINT pk_jugador PRIMARY KEY (id_jugador)\r\n" + 
									"  )", "CREATE TABLE partido (\r\n" + 
											"  id_partido INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),\r\n" + 
											"  id_equipo_local INT REFERENCES equipo (id_equipo) ON DELETE CASCADE,\r\n" + 
											"  id_equipo_visitante INT REFERENCES equipo (id_equipo) ON DELETE CASCADE,\r\n" + 
											"  goles_local INT,\r\n" + 
											"  goles_visitante INT,\r\n" + 
											"  fecha DATE,\r\n" + 
											"  ronda INT,\r\n" + 
											"  vencedor VARCHAR (50),\r\n" + 
											"  observaciones VARCHAR(128),\r\n" + 
											"  CONSTRAINT pk_partido PRIMARY KEY (id_partido)\r\n" + 
											"  )", "CREATE TABLE partida (\r\n" + 
													"  id_partida INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),\r\n" + 
													"  id_local INT REFERENCES jugador (id_jugador) ON DELETE CASCADE,\r\n" + 
													"  id_visitante INT REFERENCES jugador (id_jugador) ON DELETE CASCADE,\r\n" + 
													"  partidas_ganadas_local INT,\r\n" + 
													"  partidas_ganadas_visitante INT,\r\n" + 
													"  ronda INT,\r\n" + 
													"  CONSTRAINT pk_partida PRIMARY KEY (id_partida)\r\n" + 
													"  ) ", "CREATE TABLE partida_tenis (\r\n" + 
															"  id_tenis INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),\r\n" + 
															"  id_local_tenis INT REFERENCES jugador (id_jugador) ON DELETE CASCADE,\r\n" + 
															"  id_visitante_tenis INT REFERENCES jugador (id_jugador) ON DELETE CASCADE,\r\n" + 
															"  puntos_local INT,\r\n" + 
															"  puntos_visitante INT,\r\n" + 
															"  fecha DATE,\r\n" + 
															"  vencedor VARCHAR(45),\r\n" + 
															"  ronda INT,\r\n" + 
															"  CONSTRAINT pk_partida_temis PRIMARY KEY (id_tenis)\r\n" + 
															"  )", "CREATE TABLE partida_ajedrez (\r\n" + 
																	"  id_ajedrez INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),\r\n" + 
																	"  id_local INT REFERENCES jugador (id_jugador) ON DELETE CASCADE,\r\n" + 
																	"  id_visitante INT REFERENCES jugador (id_jugador) ON DELETE CASCADE,\r\n" + 
																	"  blancas VARCHAR(45) ,\r\n" + 
																	"  negras VARCHAR(45),\r\n" + 
																	"  fecha DATE,\r\n" + 
																	"  vencedor VARCHAR(45),\r\n" + 
																	"  ronda INT,\r\n" + 
																	"  id_partida INT REFERENCES partida (id_partida) ON DELETE CASCADE,\r\n" + 
																	"  CONSTRAINT pk_partida_ajedrez PRIMARY KEY (id_ajedrez)\r\n" + 
																	"  )", "CREATE TABLE configuracion (\r\n" + 
																			"  usuario VARCHAR(25) NOT NULL,\r\n" + 
																			"  contrasenna VARCHAR(8),\r\n" + 
																			"  colores INT,\r\n" + 
																			"  fav VARCHAR(250),\r\n" + 
																			"  observaciones_futbol VARCHAR(400),\r\n" + 
																			"  observaciones_ajedrez VARCHAR(400),\r\n" + 
																			"  observaciones_tenis VARCHAR(400),\r\n" + 
																			"  otro VARCHAR(250),\r\n" + 
																			"  CONSTRAINT pk_configuracion PRIMARY KEY (usuario)\r\n" + 
																			"  )"};
			for (int i = 0; i < 8; i++) {
				PreparedStatement ps=conn.prepareStatement(querys[i]);
				ps.execute();
				ps.close();
			}

			String[] letras= {"A", "B", "C", "D", "E", "F"};
	        try{
	            Statement es =conn.createStatement();
	            for (int i = 0; i < 76; i++) {
	            	es.executeUpdate("insert into GRUPO_FUTBOL (nombre_grupo) values ('"+letras[i]+"')");
				}
	            System.out.println("INSERTADO CORRECTAMENTE");
	            es.close();
	        }catch(Exception ex){
	        	//ALERT DIALOG CON ANIMACIÓN DE ERROR + ERRORES COMUNES
	            System.err.println("Error "+ex);
	        }
			//JOptionPane.showMessageDialog(null, "Tablas insertadas");
			conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public static Connection conectarBD() {
		Connection conn = null;
		String barra=File.separator;
		String rutaBD=System.getProperty("user.dir")+barra+"GestorDeTorneosBBDD";
		
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			conn=DriverManager.getConnection("jdbc:derby:"+rutaBD);
			System.out.println("En linea");
			return conn;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static void main(String[] args) {
		crearBBDD();
		//configuracion();
	}

	
	public static void configuracion() {
		Connection con = conectarBD();
        //QUERYS INSERT
        String queryJugadorRapido="insert into configuracion (usuario, contrasenna, colores, fav, observaciones_futbol, observaciones_ajedrez, observaciones_tenis, otro) values('admin', 'admin', 1, '', '', '', '', '')";
        try{
            Statement es =con.createStatement();
			 es.executeUpdate(queryJugadorRapido);
            System.out.println("INSERTADO CORRECTAMENTE");
            es.close();
            con.close();
        }catch(Exception ex){
        	//ALERT DIALOG CON ANIMACIÓN DE ERROR + ERRORES COMUNES
            System.err.println("Error "+ex);
        }   
	}
	
	public static ResultSet cofig() {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from configuracion");
            rs.close();
            con.close();
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;		
	}
	
	public static void actualizarBBDD(String usuario, int color, String fav, String futbol, String tenis, String ajedrez, String otros ) {
		Connection con = conectarBD();
		String queryModificarAjustes="update jugador set color="+color+", fav='"+fav+"', observaciones_futbol='"+futbol+"', observaciones_ajedrez='"+ajedrez+"', observaciones_tenis='"+tenis+"', otro='"+otros+"' where usuario='"+usuario+"'";    
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryModificarAjustes);
			System.out.println("actualizado CORRECTAMENTE");
			es.close();
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static void borrarTablas() {
		Connection conn=conectarBD();
		String[]querys= {"drop table partido", "drop table partida_tenis", "drop table partida_ajedrez", "drop table partida", "drop table jugador", "drop table equipo", "drop table grupo_futbol"};
		for (int i = 0; i < 7; i++) {
			PreparedStatement ps;
			try {
				ps = conn.prepareStatement(querys[i]);
				ps.execute();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

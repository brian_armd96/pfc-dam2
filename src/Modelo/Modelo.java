package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.DEFAULT;

import Vista.Principal;

public class Modelo {
	//QUERYS PREDEFINIDAS
	private static String listaJugadores="select * from jugador";
	private static String listaJugadoresNoEquipo="select * from jugador where competicion like '%F%' and id_equipo is null";
	private static String listaJugadoresFutbol="select * from jugador where competicion like '%F%'";
	private static String listaJugadoresAjedrez="select * from jugador where competicion like '%A%' order by curso";
	private static String listaJugadoresTenis="select * from jugador where competicion like '%T%'";
	private static String listaEquipos="select * from equipo where ronda=";
	private static String ajustes="select * from configuracion";
	private static String queryListarTodosLosEquipos="select * from equipo";

	public static ResultSet listarJugadores(){
        Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery(listaJugadores);
            
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
       
        return rs;
    }
	
	public static ResultSet listaPartidasFase(int nFase) {
		Connection con = ConexionBBDD.conectarBD();
		ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from partida where ronda="+nFase);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
		return rs;
	}
	
	public static void annadirJugador(String nombre, String curso, String seleccion, String observacion, int ronda_ajedrez, int ronda_tenis, String id_equipo, int valor){
        Connection con = ConexionBBDD.conectarBD();
        //QUERYS INSERT
        String queryInsertarJugador = null;
        switch (valor) {
        	case 1:
        		queryInsertarJugador="insert into jugador (nombre, curso, competicion, observaciones) values('"+nombre+"','"+curso+"','"+seleccion+"','"+observacion+"')";
        		break;
        	case 2:
        		queryInsertarJugador="insert into jugador (nombre, curso, competicion, ronda_ajedrez, ronda_tenis, observaciones) values('"+nombre+"', '"+curso+"', '"+seleccion+"', "+ronda_ajedrez+", "+ronda_tenis+",'"+observacion+"')";
        		break;
        	case 3:
        		queryInsertarJugador="insert into jugador (nombre, curso, competicion, ronda_ajedrez, ronda_tenis, observaciones, id_equipo) values('"+nombre+"', '"+curso+"', '"+seleccion+"', "+ronda_ajedrez+", "+ronda_tenis+", '"+observacion+"', "+id_equipo+")";
        		break;
		}
        try{
            Statement es =con.createStatement();
            es.executeUpdate(queryInsertarJugador);
			System.out.println("INSERTADO CORREcTAMENTE");
        }catch(Exception ex){
        	//ALERT DIALOG CON ANIMACIÓN DE ERROR + ERRORES COMUNES
            System.err.println("Error "+ex);
        }      
    }
	
	public static void crearPartidas(int jugador1, int jugador2, int i){
		Connection con = ConexionBBDD.conectarBD();
        //QUERYS INSERT
        String queryCrearPartida="insert into partida (id_local, id_visitante, ronda, partidas_ganadas_local, partidas_ganadas_visitante) values("+jugador1+", "+jugador2+", "+i+", 0, 0)";
        try{
            Statement es =con.createStatement();
            es.executeUpdate(queryCrearPartida);
            System.out.println("INSERTADO CORRECTAMENTE");
        }catch(Exception ex){
        	//ALERT DIALOG CON ANIMACIÓN DE ERROR + ERRORES COMUNES
            System.err.println("Error "+ex);
        }   
	}

	public static void annadirEquipo(String nombre, String tag_curso, int ronda, String observaciones, int puntos ){
		Connection con = ConexionBBDD.conectarBD();
        //QUERYS INSERT
        String queryJugadorRapido="insert into equipo (nombre_equipo, tag_curso, ronda, observaciones, puntos) values('"+nombre+"','"+tag_curso+"',"+ronda+",'"+observaciones+"', "+puntos+")";
        try{
            Statement es =con.createStatement();
            es.executeUpdate(queryJugadorRapido);
            System.out.println("INSERTADO CORRECTAMENTE");
        }catch(Exception ex){
        	//ALERT DIALOG CON ANIMACIÓN DE ERROR + ERRORES COMUNES
            System.err.println("Error "+ex);
        }   
	}
	
	public static void actualizarJugador(int id_equipo, int id_jugador) {
		Connection con = ConexionBBDD.conectarBD();
		String queryModificarJugador="update jugador set id_equipo="+id_equipo+" where id_jugador="+id_jugador;    
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryModificarJugador);
			System.out.println("actualizado CORRECTAMENTE");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("erro en poner equipo al jugador");
		}		
	}
	
	public static void eliminarJugadordeEquipo(int id_jugador) {
		Connection con = ConexionBBDD.conectarBD();
		String queryModificarJugador="update jugador set id_equipo=null where id_jugador="+id_jugador;    
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryModificarJugador);
			System.out.println("actualizado CORRECTAMENTE");
		} catch (Exception e) {
			System.out.println("erro poniendo a null los jugadores");
		}		
	}
	

	public static ResultSet listarEquiposPorFaseSinGrupo(int ronda) {
        Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery(listaEquipos+ronda+" and id_grupo is null");
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
       
        return rs;
    }
	
	public static ResultSet ajustes() {
        Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery(ajustes);
            
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
       
        return rs;
    }

	public static ResultSet listarJugadoresFutbol() {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery(listaJugadoresFutbol);
            
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
       
        return rs;
	}
	
	public static ResultSet listarJugadoresAjedrez() {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery(listaJugadoresAjedrez);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
       
        return rs;
	}
	
	public static ResultSet listarJugadoresTenis() {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery(listaJugadoresTenis);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
       
        return rs;
	}

	public static ResultSet listarJugadoresFutbolNoEquipo() {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery(listaJugadoresNoEquipo);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
       
        return rs;
	}

	public static ResultSet listarJugadoresEquipo(int idEquipo) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from JUGADOR where id_equipo="+idEquipo+"");
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static void eliminarEquipo(int id_equipo) {
		Connection con = ConexionBBDD.conectarBD();
		
		try {
			Statement es=con.createStatement();
			String queryEliminar="delete from equipo where id_equipo ="+id_equipo;
			es.executeUpdate(queryEliminar);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	public static void actualizarEquipo(int id_equipo, String nombre_equipo, String tagCurso, String observaciones) {
		Connection con = ConexionBBDD.conectarBD();
		String queryActualizarEquipo="update equipo set nombre_equipo='"+nombre_equipo+"' where id_equipo="+id_equipo;    
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarEquipo);
			System.out.println("Equipo actualizado CORRECTAMENTE");
		} catch (Exception e) {
			//erro al actualizar equipo
		}		
	
		
	}

	public static int obtenerIdEquipo(String nombre_equipo) {
		int idEquipo = 0;
		ResultSet rs=null;
		Connection con = ConexionBBDD.conectarBD();
		String queryObtenerEquipo="select id_equipo from equipo where nombre_equipo='"+nombre_equipo+"'";    
		try {
			Statement es =con.createStatement();		
			rs=es.executeQuery(queryObtenerEquipo);
			while (rs.next()) {
				idEquipo=rs.getInt("id_equipo");
			}
			
		} catch (Exception e) {
			System.out.println("error obteniendo el id de equipo");
		}		
		return idEquipo;
	}
	
	public static ArrayList<String> obtenerJugador(String jugadorListados){
		ArrayList<String>jugador=new ArrayList<>();
		ResultSet rs=null;
		Connection con = ConexionBBDD.conectarBD();
		String queryObtenerEquipo="select * from jugador where id_jugador="+jugadorListados;    
		try {
			Statement es =con.createStatement();		
			rs=es.executeQuery(queryObtenerEquipo);
			while (rs.next()) {
				jugador.clear();
				jugador.add(rs.getString("id_jugador"));
				jugador.add(rs.getString("nombre"));
				jugador.add(rs.getString("curso"));
				jugador.add(rs.getString("competicion"));
				jugador.add(rs.getString("ronda_ajedrez"));
				jugador.add(rs.getString("ronda_tenis"));
				jugador.add(rs.getString("observaciones"));
				jugador.add(rs.getString("id_equipo"));
			}
			
		} catch (Exception e) {
			System.out.println("error obteniendo el jugador");
		}		
		return jugador;
	}

	public static void eliminarJugador(String id_jugador) {
		Connection con = ConexionBBDD.conectarBD();
		try {
			Statement es=con.createStatement();
			String queryEliminar="delete from jugador where id_jugador ="+id_jugador;
			es.executeUpdate(queryEliminar);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void actualizarJugador(String id_jugador, String nombre, String curso, String seleccion, String observacion,
			String ronda_ajedrez, String ronda_tenis, String id_equipo, int i) {
		Connection con = ConexionBBDD.conectarBD();
		String queryActualizarJugador;
		if (i==1) {
			queryActualizarJugador="update jugador set nombre='"+nombre+"', curso='"+curso+"', competicion='"+seleccion+"', observaciones='"+observacion+"', ronda_ajedrez="+ronda_ajedrez+", ronda_tenis="+ronda_tenis+" , id_equipo="+id_equipo+" where id_jugador="+id_jugador;    
		}else {
			queryActualizarJugador="update jugador set nombre='"+nombre+"', curso='"+curso+"', competicion='"+seleccion+"', observaciones='"+observacion+"', ronda_ajedrez="+ronda_ajedrez+", ronda_tenis="+ronda_tenis+" where id_jugador="+id_jugador;    
		}
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Jugador actualizado CORRECTAMENTE");
		} catch (Exception e) {
			//erro al actualizar jugador
			System.out.println("ERROR ACTUALIZANDO JUGADOR");
		}		
		
	}

	public static void cambiarColor(int i, String id_usuario) {
		Connection con = ConexionBBDD.conectarBD();
		String queryActualizarColor;
		if (i==1) {
			queryActualizarColor="update configuracion set colores="+i+" where usuario= '"+id_usuario+"'";    
		}else {
			queryActualizarColor="update configuracion set colores=2 where usuario= '"+id_usuario+"'";  
		}
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarColor);
			System.out.println("color actualizado CORRECTAMENTE");
		} catch (Exception e) {
			//erro al actualizar jugador
			System.out.println(e);
		}	
		
	}

	public static void actualizarFaseJugador(int torneo, int ronda, int id_jugador) {
		Connection con = ConexionBBDD.conectarBD();
		String queryActualizarJugador;
		if (torneo==1) {
			queryActualizarJugador="update jugador set ronda_tenis="+ronda+" where id_jugador="+id_jugador;    
		}else {
			queryActualizarJugador="update jugador set ronda_ajedrez="+ronda+" where id_jugador="+id_jugador;
		}
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Jugador actualizado CORRECTAMENTE");
		} catch (Exception e) {
			//erro al actualizar jugador
			System.out.println("ERROR ACTUALIZANDO JUGADOR");
		}		
	}

	public static ResultSet listarJugadoresAjedrezPorFase(int nFase) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from JUGADOR where ronda_ajedrez="+nFase+" order by curso");
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static int countJugadoresAjedrez() {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        int count=0;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select count (*) from jugador where competicion like '%A%'");
            while (rs.next()) {
				count=rs.getInt(1);
			}
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        
		return count;
	}

	public static ResultSet obtenerPartidaAjedrezPorID(int idPartida) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from partida_ajedrez where id_partida="+idPartida);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static String obtenerIDPartidaPorJugador(int id_jugador) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        String id_Partida = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select id_partida from partida where id_local="+id_jugador);
            while (rs.next()) {
				id_Partida=rs.getString(1);
			}
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        
		return id_Partida;
	}

	public static void crearPartidasDeAjedrez(Integer jugador1, Integer jugador2, int nFase, String idPartida,
			String fechaPartida) {
		Connection con = ConexionBBDD.conectarBD();
        //QUERYS INSERT
        String queryJugadorRapido="insert into partida_ajedrez (id_local, id_visitante, ronda, id_partida, fecha, vencedor) values("+jugador1+", "+jugador2+", "+nFase+", "+idPartida+", '"+fechaPartida+"', 'none')";
        try{
        	for (int i = 0; i < 3; i++) {
        		 Statement es =con.createStatement();
                 es.executeUpdate(queryJugadorRapido);
			}
            System.out.println("INSERTADO CORRECTAMENTE");
        }catch(Exception ex){
        	//ALERT DIALOG CON ANIMACIÓN DE ERROR + ERRORES COMUNES
            System.err.println("Error "+ex);
        }  
		
	}

	public static String obtenerNombreJugadorPorID(String idJugador) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        String jugadorPorID = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select nombre, curso from jugador where JUGADOR.ID_JUGADOR="+idJugador);
            while (rs.next()) {
            	jugadorPorID=rs.getString(1)+"-"+rs.getString(2);
			}
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
		return jugadorPorID;
	}

	public static void actualizarPartida(int valor, String tipoJugador, int idPartida) {
		Connection con = ConexionBBDD.conectarBD();
		String queryActualizarJugador="update partida set partidas_ganadas_"+tipoJugador+"="+valor+" where id_partida="+idPartida;
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Se ha actualizado partida");
		} catch (Exception e) {
			//erro al actualizar jugador
			System.out.println(e);
		}	
		
	}

	public static void actualizarPartidaDeAjedrez(String idP1, String fecha, String vencedor) {
		Connection con = ConexionBBDD.conectarBD();
		String queryActualizarJugador="update partida_ajedrez set fecha= '"+fecha+"', vencedor='"+vencedor+"' where id_ajedrez="+idP1;
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Se ha actualizado partida");
		} catch (Exception e) {
			//erro al actualizar jugador
			System.out.println(e);
		}	
	}

	public static int countJugadoresTenis() {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        int count=0;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select count (*) from jugador where competicion like '%T%'");
            while (rs.next()) {
				count=rs.getInt(1);
			}
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        
		return count;
	}

	public static ResultSet listaPartidaAjedrezFase(int nFaseTenis) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from partida_tenis where ronda="+nFaseTenis);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static ResultSet listaPartidasTenisFase(int nFaseTenis) {
		Connection con = ConexionBBDD.conectarBD();
		ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from partida_tenis where ronda="+nFaseTenis);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
		return rs;
	}

	public static ResultSet listarJugadoresTenisPorFase(int nFaseTenis) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from JUGADOR where ronda_tenis="+nFaseTenis+" order by curso");
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static void crearPartidasDeTenis(Integer jugador1, Integer jugador2, int nFase, String fechaPartida) {
		Connection con = ConexionBBDD.conectarBD();
        //QUERYS INSERT
        String queryJugadorRapido="insert into partida_tenis (id_local_tenis, id_visitante_tenis, ronda, fecha, vencedor) values("+jugador1+", "+jugador2+", "+nFase+", '"+fechaPartida+"', 'none')";
        try{
        	Statement es =con.createStatement();
            es.executeUpdate(queryJugadorRapido);
            System.out.println("Partida de tenis insertado correctamente");
        }catch(Exception ex){
        	//ALERT DIALOG CON ANIMACIÓN DE ERROR + ERRORES COMUNES
            System.err.println("Error "+ex);
        }  
	}

	public static void actalizarPartidaDeTenis(String idPartidaTenis, String vencedor, String fecha) {
		Connection con=ConexionBBDD.conectarBD();
		String queryActualizarJugador="update partida_tenis set fecha= '"+fecha+"', vencedor='"+vencedor+"' where id_tenis="+idPartidaTenis;
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Se ha actualizado partida");
		} catch (Exception e) {
			//erro al actualizar jugador
			System.out.println(e);
		}
		
	}

	public static void actualizarConfiguracion(String usuario, String contrasenna, String paginaFav) {
		Connection con=ConexionBBDD.conectarBD();
		String queryActualizarJugador="update configuracion set contrasenna= '"+contrasenna+"', fav='"+paginaFav+"' where usuario='admin'";
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Se ha actualizado configuraciones");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static ResultSet listarTodosLosEquipos() {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery(queryListarTodosLosEquipos);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static ResultSet listaEquipoPorGrupo(int i) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from equipo where id_grupo="+i);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static void actualizarGrupoDeEquipo(int idEquipo, int grupo, String none) {
		Connection con=ConexionBBDD.conectarBD();
		String queryActualizarJugador="update equipo set id_grupo="+grupo+" where id_equipo="+idEquipo;
		if (none.equals("null")) {
			queryActualizarJugador="update equipo set id_grupo="+none+" where id_equipo="+idEquipo;
		}
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Se ha actualizado el grupo");
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}

	public static ResultSet listaEquipoPorGrupoPuntos(int i) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from equipo where id_grupo="+i+" order by puntos");
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static ResultSet listaPartidosPorGrupo(int i) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select PARTIDO.ID_PARTIDO, e.nombre_equipo, e1.nombre_equipo, PARTIDO.GOLES_LOCAL, PARTIDO.GOLES_VISITANTE, PARTIDO.FECHA from PARTIDO, EQUIPO as e, EQUIPO as e1 where PARTIDO.ID_EQUIPO_LOCAL=e.id_equipo and PARTIDO.ID_EQUIPO_VISITANTE=e1.id_equipo and e.id_grupo="+i);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static void actualizarPuntosEquipo(int i, int idEquipoVencedor) {
		Connection con=ConexionBBDD.conectarBD();
		String queryActualizarJugador="update equipo set puntos="+i+" where id_equipo="+idEquipoVencedor;
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Se ha actualizado el grupo");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static int obtenerPuntosDeEquipo(String nombreEquipo) {
		int puntos=-1;
		ResultSet rs=null;
		Connection con = ConexionBBDD.conectarBD();
		String queryObtenerEquipo="select puntos from equipo where nombre_equipo='"+nombreEquipo+"'";    
		try {
			Statement es =con.createStatement();		
			rs=es.executeQuery(queryObtenerEquipo);
			while (rs.next()) {
				puntos=rs.getInt(1);
			}
			
		} catch (Exception e) {
			System.out.println("error obteniendo los puntos");
		}		
		return puntos;
	}

	public static void actualizarPartidoFutbol(String idPartido, int idEquipoLocal, int idEquipoVisitante, int golesLocal,
			int golesVisitante, String fechaPartido, String observaciones) {
		Connection con=ConexionBBDD.conectarBD();
		String queryActualizarPartido="update partido set id_equipo_local="+idEquipoLocal+
				", id_equipo_visitante="+idEquipoVisitante+", goles_local="+golesLocal+
				", goles_visitante="+golesVisitante+", fecha='"+fechaPartido+
				"', observaciones='"+observaciones+"' where id_partido="+idPartido, 
				queryActualizarPartidoSinObservaciones="update partido set id_equipo_local="+idEquipoLocal+
						", id_equipo_visitante="+idEquipoVisitante+", goles_local="+golesLocal+
						", goles_visitante="+golesVisitante+", fecha='"+fechaPartido+
						" where id_partido="+idPartido;
		try {
			Statement es =con.createStatement();	
			if (observaciones.equals("")) {
				es.executeUpdate(queryActualizarPartidoSinObservaciones);
			}else {
				es.executeUpdate(queryActualizarPartido);
			}
			System.out.println("Se ha actualizado el grupo");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void crearPartidoDeFutbol(int local, int visitante) {
		Connection con = ConexionBBDD.conectarBD();
        //QUERYS INSERT
        String queryJugadorRapido="insert into partido (id_equipo_local, id_equipo_visitante, goles_local, goles_visitante, fecha, ronda, observaciones) "
        		+ "values("+local+", "+visitante+", 0, 0, '08/08/2018', 1, 'Ninguno')";
        try{
        	Statement es =con.createStatement();
            es.executeUpdate(queryJugadorRapido);
            System.out.println("Partida de tenis insertado correctamente");
        }catch(Exception ex){
        	//ALERT DIALOG CON ANIMACIÓN DE ERROR + ERRORES COMUNES
            System.err.println("Error "+ex);
        }  
	}

	public static String obtenerNombreEquipoPorID(String idEquipo) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        String nombre=null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select nombre_equipo from equipo where id_equipo="+idEquipo);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        try {
			while (rs.next()) {
				nombre=rs.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return nombre;
	}

	public static ResultSet listaJugadoresBusqueda(String buscar, int i) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        String query=null;
        switch (i) {
		case 1:
			query="select * from jugador where competicion like '%F%' and nombre like '"+buscar+"%'";
			break;
		case 2:
			query="select * from jugador where competicion like '%A%' and nombre like '"+buscar+"%'";
			break;
		case 3:
			query="select * from jugador where competicion like '%T%' and nombre like'"+buscar+"%'";
			break;
		default:
			query="select * from jugador where nombre like '"+buscar+"%'";
			break;
		}
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery(query);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static ResultSet listarEquiposPorFase(int i) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select * from equipo where ronda="+i);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}

	public static String obtenerGanadorTenis(int nFase) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        String nombre="";
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select nombre from jugador where ronda_tenis="+nFase);
            while (rs.next()) {
				nombre=rs.getString(1);
			}
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return nombre;
	}
	public static String obtenerGanadorAjedrez(int nFase) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        String nombre="";
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select nombre from jugador where ronda_ajedrez="+nFase);
            while (rs.next()) {
				nombre=rs.getString(1);
			}
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return nombre;
	}

	public static void actualizarVencedorPartido(String idPartido, int idVencedor) {
		Connection con=ConexionBBDD.conectarBD();
		String queryActualizarJugador="update partido set vencedor='"+idVencedor+"' where id_partido="+idPartido;
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Se ha actualizado el vencedor");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static boolean listarPartidosSinJugar(int ronda) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        String nombre="none";
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select id_partido from partido where ronda= "+ronda+" and vencedor is null");
            while (rs.next()) {
            	nombre=rs.getString(1);
			}
			if (nombre.equals("none")) {
				return false;
			}else {
				return true;
			}
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
		return false;
	}

	public static void pasarDeRondaEquipo(int idEquipo, int i) {
		Connection con=ConexionBBDD.conectarBD();
		String queryActualizarJugador="update equipo set ronda="+i+" where id_equipo="+idEquipo;
		try {
			Statement es =con.createStatement();		
			es.executeUpdate(queryActualizarJugador);
			System.out.println("Se ha actualizado la ronda");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static ResultSet partidosPorFase(int i) {
		Connection con = ConexionBBDD.conectarBD();
        ResultSet rs = null;
        try{
            Statement es =con.createStatement();
            rs = es.executeQuery("select PARTIDO.ID_PARTIDO, e.nombre_equipo, e1.nombre_equipo, PARTIDO.GOLES_LOCAL, PARTIDO.GOLES_VISITANTE, PARTIDO.FECHA from PARTIDO, EQUIPO as e, EQUIPO as e1 where PARTIDO.ID_EQUIPO_LOCAL=e.id_equipo and PARTIDO.ID_EQUIPO_VISITANTE=e1.id_equipo and e.ronda="+i);
        }catch(Exception ex){
            System.err.println("Error "+ex);
            //MENSAJE ERROR ALELRT DIALOG
        }
        return rs;
	}
}

package Modelo;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;

import javax.swing.JOptionPane;

import Vista.Principal;



public class AjustesApp {

	public static ActionListener abrirPaginaEfa(){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
		            Desktop.getDesktop().browse(new URI("https://efamoratalaz.com/"));
		        } catch (Exception e1) {
		        }
			}		
		};
	}
	
	public static ActionListener abrirPaginaFav(){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String url=Principal.getPaginaFav().getText().toString();
				if (!url.isEmpty()) {
					String inicio=url.charAt(0)+""+url.charAt(1)+url.charAt(2)+url.charAt(3)+url.charAt(4)+url.charAt(5)+url.charAt(6)+url.charAt(7);
					System.out.println(inicio);
					if (url.length()<140) {
						if (inicio.equals("https://")) {
							try {
					            Desktop.getDesktop().browse(new URI(url));
					        } catch (Exception e1) {
					        	JOptionPane.showMessageDialog(null, "Fallo al abrir el enlace, compruebe que est� correctamente escrito", "URL no v�lida", 2);
					        }
						}else {
				        	JOptionPane.showMessageDialog(null, "En enlacee de la p�gina web debe de empezar por \n http://", "URL no v�lida", 2);
						}
						
					}else {
						System.out.println("Numero de caracteres sobrepasa el l�mite");
						JOptionPane.showMessageDialog(null, "Sobre pasa el n�mero m�ximo de caracteres", "URL no v�lida", 1);
					}
				}else {
					JOptionPane.showMessageDialog(null, "Primero a�ada un enlace a una p�gina web, \n para a�adir dir�jase al bot�n de ajustes", "ERROR", 0);
				}
				
			}		
		};
	}

	public static void iniciarTodo() {
		File bbdd=new File("GestorDeTorneosBBDD/");
		System.out.println(bbdd.getAbsolutePath());
		if (!bbdd.exists()) {
			ConexionBBDD.main(null);
			ConexionBBDD.configuracion();
		}else {
			System.out.println("Preparado para iniciar sesion");
		}
		
	}
	
	
}
